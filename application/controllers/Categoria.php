<?php
class Categoria extends CI_controller{
	function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $this->load->model('Producto_model');
        $data['products'] = $this->Producto_model->get_all_producto();
        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();
        $this->load->view('front/layouts/main',$data);
    }
    
    public function menuFront(){
        $this->load->model('Categorium_model');
        $this->load->model('Familium_model');
        $data['nav_cats'] = $this->Categorium_model->get_all_categoria_join();
        $data['nav_family'] = $this->Familium_model->get_all_familia();
        return $data;
    }

    public function buscar($fam='', $cat=''){
        $this->load->model('Producto_model');
        if ($cat == '') {
            $data['products'] = $this->Producto_model->get_all_producto($fam);
        }else{
            $data['products'] = $this->Producto_model->get_producto_by_cat($fam, $cat);
        }
        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();
        $this->load->view('front/layouts/main',$data);
    }

}