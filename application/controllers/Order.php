<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Order extends BaseController{
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->helper('price');
            $this->load->model('Order_hdr_model');
            $this->user = $this->dataUser();

        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of order
     */
    function index(){
        $data['_view'] = 'order/index';
        $this->load->view('layouts/main',$data);

    }

    public function get_list_dt(){

        //Paginado
        $start = $this->input->post('start');

        //Ver
        $length = $this->input->post("length");
        
        //Busqueda
        $search = $this->input->post("search")['value'];

        $result = $this->Order_hdr_model->getAllOrder($start, $length, $search);
        
        $resultado = $result["datos"];

        $numrows   = $result["numRows"];
        $numrowstotal = $this->Order_hdr_model->getNumRowAll();

        $data = array();
        
        foreach ($resultado->result_array() as $result_row) {
            
            $row_of_data = array();

            $row_of_data[] = $result_row["idOrderHdr"];
            $row_of_data[] = $result_row["idUser"];
            $row_of_data[] = show_price($result_row["total"]);
            $row_of_data[] = $result_row["idOrderStatus"];
            $row_of_data[] = $result_row["nota"];
            $row_of_data[] = $result_row["createdBy"];
            $row_of_data[] = $result_row["created"];
            $row_of_data[] = $result_row["updatedBy"];
            $row_of_data[] = $result_row["updated"];
            $row_of_data[] = "<a href='" . base_url() . 'order/orderDetail/' . $result_row["idOrderHdr"] . "' class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> EDITAR</a> 
                              <a href='' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span> BORRAR</a>";
            
            $data[] = $row_of_data;

        }
        
        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrows),
            "data"            => $data
        );


        echo json_encode($json_data);  

    }

    public function orderDetail($idOrderHdr){
        
        $data['_view'] = 'order/detail';
        $data['order'] = $idOrderHdr;
        
        $this->load->view('layouts/main',$data);   
    }

    public function getOrderDetail(){


        
    }
    
}
