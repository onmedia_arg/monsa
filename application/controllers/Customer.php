<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Customer extends BaseController{
    
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Order_hdr_model');
            $this->load->model('Order_itm_model');
            $this->load->helper("globalFunctions");
            $this->load->helper("price");            

        }else{
            redirect('/', 'refresh');
        }
    }

    function index(){
        $data['nav']   = menuFront();
        $data['title'] = "Mi Cuenta";
        $data['_view'] = 'front/customer/orders_list';
        $data['user']         = $this->dataUser();
        
        $this->load->view('front/customer/index', $data);
    }


    function listOrders(){

        $result = $this->Order_hdr_model->getAllByCustomer($this->session->userdata('id'));
        
        $data   = array();
       
        foreach ($result as $result_row){
            $row = array();
            
            $row[] = $result_row->idOrderHdr;
            if (trim($result_row->created) && $result_row->created != null) {
                 $result_row->created = DateTime::createFromFormat("Y-m-d", $result_row->created);
                 $result_row->created = $result_row->created->format("d-m-Y");
            }
            $row[] = $result_row->created;

            $row[] = show_price($result_row->total);
            $row[] = $result_row->idOrderStatus;

            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }

    function listItems(){
        $idOrderHdr = $this->input->post("idOrderHdr");
        $result = $this->Order_itm_model->getItemsByOrder($idOrderHdr);
        $data   = array();
        foreach ($result as $result_row){
            $row = array();
            $row[] = $result_row->posnr;
            $row[] = $result_row->idProducto;
            $row[] = show_price($result_row->precio);
            $row[] = $result_row->qty;
            $row[] = show_price($result_row->total);

            $data[] = $row;            
        }
        
        $output = array(
            "data" => $data,
        );

        echo json_encode($output);

    }

    function getOrder(){
        $idOrderHdr = $this->input->post("idOrderHdr");
        $result = $this->Order_hdr_model->getById($idOrderHdr);

        $data = array();
        $data[] = $result["created"];
        $data[] = $result["nota"];
        $data[] = show_price($result["total"]);
        $data[] = $result["idOrderStatus"];
        
        echo json_encode($data);
    }

    function viewProfile(){

        
    }

    function viewAccount(){

        
    }

}