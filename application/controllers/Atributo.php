<?php
//test branch
include_once(FCPATH."/application/controllers/BaseController.php");
class Atributo extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Atributo_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of atributo
     */
    function index()
    {
        $data['atributo'] = $this->Atributo_model->get_all_atributo_join();
        $data['user'] = $this->user;
        $data['_view'] = 'atributo/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new atributo
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'idfamilia' => $this->input->post('idfamilia'),
                'nombre' => $this->input->post('nombre'),
				'slug' => $this->input->post('slug'),
				'valor' => json_encode($this->input->post('valor')),
            );
            $atributo_id = $this->Atributo_model->add_atributo($params);
            redirect('atributo/index');
        }
        else
        {      
            $this->load->model('Familium_model');
            $data['all_familia'] = $this->Familium_model->get_all_familia();
            $data['user'] = $this->user;      
            $data['_view'] = 'atributo/add';

            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a atributo
     */
    function edit($idAtributo)
    {   
        // check if the atributo exists before trying to edit it
        $data['atributo'] = $this->Atributo_model->get_atributo($idAtributo);
        if(isset($data['atributo']['idAtributo']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $filtered_values = array_filter($this->input->post('valor'), "strlen");
                $params = array(
                    'idfamilia' => $this->input->post('idfamilia'),
                    'nombre' => $this->input->post('nombre'),
                    'valor' => json_encode($filtered_values),
                    'slug' => $this->input->post('slug'),
                );

                $this->Atributo_model->update_atributo($idAtributo,$params);            
                redirect('atributo/index');
            }
            else
            {
                $this->load->model('Familium_model');
                $data['all_familia'] = $this->Familium_model->get_all_familia();
                $data['user'] = $this->user; 
                $data['_view'] = 'atributo/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The atributo you are trying to edit does not exist.');
    } 

    /*
     * Deleting atributo
     */
    function remove($idAtributo)
    {
        $atributo = $this->Atributo_model->get_atributo($idAtributo);

        // check if the atributo exists before trying to delete it
        if(isset($atributo['idAtributo']))
        {
            $this->Atributo_model->delete_atributo($idAtributo);
            redirect('atributo/index');
        }
        else
            show_error('The atributo you are trying to delete does not exist.');
    }

    function get_attr_by_family($idfamilia = ''){

        if ($idfamilia != '') {
            $data['atributo'] = $this->Atributo_model->get_all_atributo_by_family($idfamilia);
        }else{
            $data['atributo'] = $this->Atributo_model->get_all_atributo_join();
        }
        if (count($data['atributo']) > 0) {
            for ($i=0; $i < count($data['atributo']); $i++) { 
                $data['atributo'][$i]['valor'] = json_decode($data['atributo'][$i]['valor']);
            }
        }

        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));

        // $this->json_ouput($data);
    }

    function get_attr_for_family_all($str = NULL){

        $data['atributo'] = $this->Atributo_model->get_all_atributo_join($str);

        if (count($data['atributo']) > 0) {
            for ($i=0; $i < count($data['atributo']); $i++) { 
                $data['atributo'][$i]['valor'] = json_decode($data['atributo'][$i]['valor']);
            }
        }
        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
        
        // return $this->json_ouput($data);
    }

    function get_all_attr(){

        $this->load->model('ProductoAtributo_model');
        $params = [ 'idAtributo' => $_REQUEST['idAtributo'], 'idProducto' => $_REQUEST['idProducto'] ];
        $atributos = $this->ProductoAtributo_model->get_all_producto_atributo( $params );
        $return = array(); 
 
        foreach ($atributos as $atributo) {
            $return[] = $atributo;
        }
 
        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        
        // return $this->json_ouput($data);
    }

    function nuevo_valor_atributo( $idAtributo = null, $idfamilia = null, $valor = null ){

        $idAtributo = ( isset( $_REQUEST['idAtributo'] ) ) ? $_REQUEST['idAtributo'] : $idAtributo ;
        $valor = ( isset( $_REQUEST['valor'] ) ) ? $_REQUEST['valor'] : $valor ;

        $json = $this->db->where( 'idAtributo', $idAtributo )
                         ->get( 'atributo' )
                         ->result_array();

        $json = $json[0]['valor'];

        if ( $json !== '' && $json !== null && $json !== 0 && $json !== '0' ) {
            $array = json_decode($json); 
        }else{
            $array = array();
        }

        array_push( $array, $valor );

        $update = $this->db->where( 'idAtributo', $idAtributo )
                           ->update( 'atributo', [ 'valor' => json_encode( $array ) ] );

        if ( $this->db->affected_rows() >= 1 ) {
            $return = [ 'code' => 1, 'message' => null ];
        }else{
            $return = [ 'code' => 0, 'message' => 'No se pudo actualizar la base de datos' ];
        }

        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));

    }

}
