<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Shop extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Producto_model');
            $this->load->model('Familium_model');
            $this->load->model('Marca_model');
            $this->load->helper("globalFunctions");
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 


    public function fetch_data()
    {
        sleep(1);

        // if ( isset( $_POST['marca'] ) ) {
        //     if ( is_array( $_POST['marca'] ) ) {
        //         $marca = ( strpos($_POST['marca'], ',') !== false ) ? explode(',', $_POST['marca']) : $_POST['marca'] ;
        //     }else{
        //         $marca = $this->input->post('marca');
        //     }

        // }

        // if ( isset( $_POST['familia'] ) ) {

        //     if ( is_array( $_POST['familia'] ) ) {
        //         $familia = $this->input->post('familia');
        //     }else{
        //         $familia = ( strpos($_POST['familia'], ',') !== false ) ? explode(',', $_POST['familia']) : $_POST['familia'] ;

        //     }

        // } 

        $marca         = $this->input->post('marca');
        $familia       = $this->input->post('familia');
        
        $this->load->library('pagination');

        $config = array();
        $config['base_url']         = '#';
        $config['total_rows']       = $this->Producto_model->countAll($marca, $familia);
        $config['per_page']         = 12;
        $config['uri_segment']      = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['next_link']        = '&gt;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['prev_link']        = '&lt;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['cur_tag_open']     = "<li class='active'><a href='#'>";
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['num_links']        = 3;

        $this->pagination->initialize($config);
        $page   = $this->uri->segment(3);
        $start  = ($page - 1) * $config['per_page'];
        

        $data = $this->Producto_model->fetch_data($config["per_page"], $start, $marca, $familia);

        $output = array(
                    'pagination_link' => $this->pagination->create_links(),
                    'product_list'    => $data
                  );
        
        echo json_encode($output);

    } 

    public function build_src(){


        $images  = $this->input->post('images');
        
        $max     = 1;
        $img_src = build_img_url($images, $max); 

        echo json_encode(array('img_src' => $img_src));

    }
  
}
?>