<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class FrontController extends BaseController{

    // private $cart;
    
	function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Familium_model');
            $this->load->model('Marca_model');
            $this->load->model('Producto_model');
            $this->load->model('Atributo_model');
            $this->load->helper("globalFunctions");
            $this->load->model('ProductoCategoria_model');
            $this->load->model('ProductoAtributo_model');
            $this->load->model('ProductoAplicacion_model');
            $this->load->model('moto_model');            

        }else{
            redirect('/', 'refresh');
        }

    }

    public function index(){
    	$this->data['_view']  = 'front/home';
        
        $this->data['user']  = $this->dataUser();
        $this->data['nav']   = menuFront();
        
        // $fam   = null;
        // $marca = null;
        // $base  = base_url() . 'frontController/inicio';
        // $config['total_rows'] = count($this->Producto_model->get_all_producto_con_img_count($fam)); 
        // $config['per_page']   = 12;
        // include('partials/pagination.php');
        // $this->data["products"] = $this->Producto_model->get_all_producto_con_img($page,$config["per_page"]);
        // $this->pagination->initialize($config);
        // $this->data["pagination"] = $this->pagination->create_links();

        $this->load->view('front/front',$this->data);
    }


    public function detalle($strProduct){

    	$data['products'] = $this->Producto_model->get_product_by_str($strProduct);
    	$data['product'] = (is_array($data['products']))?$data['products'][0]: false;
        if (!(isset($data['product']) && $data['product'] != '')) {
            redirect($this->inicio, 'refresh');
        }else{
            if(!isset($data['product']['idProducto'])){
                redirect($this->inicio, 'refresh');
            }else{
                $idProducto = $data['product']['idProducto'];
                
                $data['product']['cats'] = $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
                
                // if($data['product']['idTipo'] == 0){
                //     $data['product']['atributos'] = $this->ProductoAtributo_model->get_all_atributo_by_product($idProducto);
                // }else if($data['product']['idTipo'] == 1){
                //      $data['product']['atributos']       = $this->Atributo_model->get_all_atributo_by_family($data['product']['idFamilia']);
                // }

                $data['product']['atributos'] = $this->ProductoAtributo_model->get_all_atributo_by_product($idProducto);
                
                if($data['product']['idTipo'] == 1){
                    $data['product']['atr_value_list']  = $this->Atributo_model->get_atributo_for_prod_var($idProducto);
                }

                // var_dump('<pre>atributos:', $data['product']['atr_value_list'],$data['product']['atributos'],'</pre>');
                // die;

                $data['product']['aplicacion'] = $this->ProductoAplicacion_model->get_prodApp_by_idProducto($idProducto);

                $motos = json_decode($data['product']['aplicacion']['arrMoto']);

                $data['product']['aplicacion']['motos'] = [];
                
                if (is_array($motos)){
                    if(count($motos)>0){
                        foreach ($motos as $key => $moto) {
                            array_push($data['product']['aplicacion']['motos'], $this->moto_model->get_moto($moto));
                        }
                    }
                }
                
            	$data['_view'] = 'front/detail';
                $data['user'] = $this->dataUser();
                $data['nav'] = $this->menuFront();

                $this->mybreadcrumb->add('Productos', base_url('frontController/productos'));
                $this->mybreadcrumb->add($data['product']['familia'], base_url('frontController/productos/'.$data['product']['familiaSlug']));
                $this->mybreadcrumb->add($data['product']['nombre'], base_url('frontController/detalle/'.$data['product']['slug']));
                $data['breadcrumbs'] = $this->mybreadcrumb->render();                        

                $this->load->view('front/productos/index', $data);
                
            }
        }
    }

    public function menuFront(){
        $this->load->model('Categorium_model');
        $this->load->model('Familium_model');
        $this->load->model('Marca_model');
        $data['nav_cats'] = $this->Categorium_model->get_all_categoria_join();
        $data['nav_family'] = $this->Familium_model->get_all_familia();
        $data['nav_marcas'] = $this->Marca_model->get_all_marca();
        return $data;
    }

    function set_product_filter(){
        $this->session->userdata();
        foreach ($_POST as $key => $value) {
            if($key == "mostrar18")
                $this->session->set_userdata('mostrar', '18');
            if($key == "mostrar9")
                $this->session->set_userdata('mostrar', '9');
            if($key == "mostrar6")
                $this->session->set_userdata('mostrar', '6');
            if($key == "ordasc")
                $this->session->set_userdata('ord', 'asc');
            if($key == "orddesc")
                $this->session->set_userdata('ord', 'desc');
            if($key == "preciomin")
                $this->session->set_userdata('preciomin', $_POST['preciomin']);
            if($key == "preciomax")
                $this->session->set_userdata('preciomax', $_POST['preciomax']);
        }
    }

    public function productos($fam=null,$marca=null){

        $this->load->model('Producto_model');

        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();

        $data['title'] = 'Productos según categoría';
        $data['fam'] = $fam;
        if($marca && !is_numeric($marca))
        $data['marca'] = $marca;

        // $base = 'http://monsa-dev.onmedia.com.ar/frontController/productos/';
        $base = base_url() . 'frontController/productos';
    
        if ($fam  && !is_numeric($fam)){ 
            $base .= "/".$fam;
        }
        if ($marca && !is_numeric($marca)){
            $base .= "/".$marca;
        }
        $config['total_rows'] = count($this->Producto_model->get_all_producto_count($fam,$marca)); 
        if($this->session->userdata('mostrar'))
           $config['per_page'] = $this->session->userdata('mostrar');
        else
            $config['per_page'] = 18;
        include('partials/pagination.php');
        
        $data['products']      = $this->Producto_model->get_all_producto($page,$config["per_page"],$fam,$marca);
        $data['marcas']        = $this->Producto_model->get_all_producto(null,null,$fam,$marca);
        $data["featured_prod"] = $this->Producto_model->get_featured_prod();
        $data['total_rows']    = $config['total_rows'];

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $this->mybreadcrumb->add('Productos', base_url('frontController/productos'));
        if($fam)
            $this->mybreadcrumb->add($fam, base_url('frontController/productos'.'/'.$fam));
        if($marca && !is_numeric($marca))
            $this->mybreadcrumb->add($marca, base_url('frontController/productos'.'/'.$fam.'/'.$marca));

            $data['breadcrumbs'] = $this->mybreadcrumb->render();

            $this->load->view('front/productos',$data);
    }

    public function productoslista($fam=null,$marca=null){

        $this->load->model('Producto_model');

        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();

        $data['title'] = 'Productos según categoría';
        $data['fam'] = $fam;
        if($marca && !is_numeric($marca))
            $data['marca'] = $marca;
        
        $base = base_url() . 'frontController/productoslista/';        
        // $base = 'http://monsa-dev.onmedia.com.ar/frontController/productoslista/';
        
        if ($fam  && !is_numeric($fam)){ 
            $base .= "/".$fam;
        }
        if ($marca && !is_numeric($marca)){
            $base .= "/".$marca;
        }
        $config['total_rows'] = count($this->Producto_model->get_all_producto_count($fam,$marca)); 
        if($this->session->userdata('mostrar'))
           $config['per_page'] = $this->session->userdata('mostrar');
        else
            $config['per_page'] = 4;
        include('partials/pagination.php');
        
        $data['products'] = $this->Producto_model->get_all_producto($page,$config["per_page"],$fam,$marca);
        $data['marcas'] = $this->Producto_model->get_all_producto(null,null,$fam,$marca);
        $data['total_rows'] = $config['total_rows'];

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

            $this->mybreadcrumb->add('Productos', base_url('frontController/productos'));
            if($fam)
                $this->mybreadcrumb->add($fam, base_url('frontController/productos'.'/'.$fam));
            if($marca && !is_numeric($marca))
                $this->mybreadcrumb->add($marca, base_url('frontController/productos'.'/'.$fam.'/'.$marca));

                $data['breadcrumbs'] = $this->mybreadcrumb->render();

                $this->load->view('front/productoslista',$data);
    }

    public function producto(){
        $this->load->model('Producto_model');
        $data['products'] = $this->Producto_model->get_all_producto();
        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();
        $data['title'] = 'Detalle del producto';

        $this->load->view('front/producto',$data);
    }

    public function marcas($marca=null,$fam=null){

        $this->load->model('Producto_model');

        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();
        $data['title'] = 'Productos según marcas';
        $data['fam'] = $fam;
        if($marca && !is_numeric($marca))
            $data['marca'] = $marca;
        $base = base_url() . 'frontController/marcas/';
        // $base = 'http://monsa-dev.onmedia.com.ar/frontController/marcas/';
        if ($fam  && !is_numeric($fam)){ 
            $base .= "/".$fam;
        }
        if ($marca && !is_numeric($marca)){
            $base .= "/".$marca;
        }
        $config['total_rows'] = count($this->Producto_model->get_all_producto_count($fam,$marca)); 
        if($this->session->userdata('mostrar'))
           $config['per_page'] = $this->session->userdata('mostrar');
        else
            $config['per_page'] = 18;
        include('partials/pagination.php');
        $data['products'] = $this->Producto_model->get_all_producto($page,$config["per_page"],$fam,$marca);
        $data['marcas'] = $this->Producto_model->get_all_producto(null,null,$fam,$marca);
        $data['total_rows'] = $config['total_rows'];

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $this->mybreadcrumb->add('Productos', base_url('frontController/productos'));
        if($fam)
            $this->mybreadcrumb->add($fam, base_url('frontController/productos'.'/'.$fam));
        if($marca && !is_numeric($marca))
            $this->mybreadcrumb->add($marca, base_url('frontController/productos'.'/'.$fam.'/'.$marca));

            $data['breadcrumbs'] = $this->mybreadcrumb->render();

            $this->load->view('front/productos',$data);
    }    

    public function shop()
    {
        $data['user']         = $this->dataUser();
        $data['marca_data']   = $this->Marca_model->get_all_marca(); 
        $data['family_data']  = $this->Familium_model->get_all_familia();
        $data['nav']          = menuFront();
        $data['title']        = "Tienda";

        $this->load->view('front/shop/index', $data);
    }
 
//  Fn para mostrar la vista Carrito
    
    function carrito()
    {
        // $data['marca_data']   = $this->Marca_model->get_all_marca(); 
        // $data['family_data']  = $this->Familium_model->get_all_familia();
        $data['user']         = $this->dataUser();
        $data['nav']          = menuFront();
        $data['title']        = "Carrito";

        $this->load->view('front/cart/index', $data);
    }

    function customer(){
        $data['nav']          = menuFront();
        $data['user']         = $this->dataUser();
        $data['title']        = "Mi Cuenta";
        $data['_view']        = 'front/customer/orders_list';

        $this->load->view('front/customer/index', $data);   
    }

    function getProdDestacados(){
        $data = $this->Producto_model->get_products_destacado();
        echo json_encode($data);
    }


}