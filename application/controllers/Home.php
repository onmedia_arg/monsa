<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Home extends BaseController{

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Producto_model');

        }else{
            redirect('/', 'refresh');
        }
    }

    function getProductosHome(){

        $data = $this->Producto_model->get_products_home();
        echo json_encode($data);

    }
}

