<?php
// 13525449
include_once(FCPATH."/application/controllers/BaseController.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Producto extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Producto_model');
            $this->user = $this->dataUser();
            $this->load->helper("price");
        }else{
            redirect('/', 'refresh');
        }
    }

    /*
     * Listing of producto
     */
    function index()
    {
        //$data['producto'] = $this->Producto_model->get_all_producto();
        $data['_view']    = 'producto/index';
        $data['user']     = $this->user;
        $this->load->view('layouts/main',$data);
    }

    public function get_list_dt(){

        //Paginado
        $start = $this->input->post('start');

        //Ver
        $length = $this->input->post("length");

        //Busqueda
        $search = $this->input->post("search")['value'];

        $result = $this->Producto_model->getAllProducto($start, $length, $search);

        $resultado = $result["datos"];

        $numrows   = $result["numRows"];
        $numrowstotal = $this->Producto_model->getNumRowAll();

        $data = array();

        foreach ($resultado->result_array() as $result_row) {

            $row_of_data = array();

            $row_of_data[] = $result_row["sku"];
            $row_of_data[] = $result_row["familia"];
            $row_of_data[] = $result_row["marca"];
            $row_of_data[] = $result_row["modelo"];
            $row_of_data[] = $result_row["precio"];
            $row_of_data[] = $result_row["stock"];
            $row_of_data[] = $result_row["visibilidad"];
            $row_of_data[] = "<a href='". base_url() . 'producto/edit/'  . $result_row["idProducto"] . "' class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> EDITAR</a> 
                              <a href='". base_url() . 'producto/remove/'. $result_row["idProducto"] . "' class='btn btn-danger btn-xs'><span class='fa fa-trash'></span> BORRAR</a>";

            $data[] = $row_of_data;

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrows),
            "data"            => $data
        );


        echo json_encode($json_data);

    }

    /*
     * Adding a new producto
     */

    function get_product_by_id($idProducto){
        $producto = $this->Producto_model->get_producto($idProducto);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($producto));
    }

    function add()
    {
        if(isset($_POST) && count($_POST) > 0)
        {
            // Si hay imagenes subir
            if ( isset($_FILES) ) {
                $uploadsImages = $this->uploadsImages($_FILES, $this->input->post('idFamilia')); 

                if (count($uploadsImages['error']) != 0) {
                    // TODO: manejo de error al subir imagenes
                        $alert = '<ul class ="error error-upload">';
                            foreach ($uploadsImages['error'] as $key => $error) {
                                $alert .= '<li>'. $key .': '. $error .'</li>';
                            }
                        $alert .= '</ul>';
                    
                        $this->session->set_flashdata('alert_message', 'Hubo un error subiendo las imagenes del producto: ' . $alert); 

                }

            }
 
                // add
                $dimensiones = array(
                    'alto'        => $this->input->post('alto'),
                    'largo'       => $this->input->post('largo'),
                    'ancho'       => $this->input->post('ancho')
                );
                $params = array(
                    'nombre'      => $this->input->post('nombre'),
                    'slug'        => $this->input->post('slug'),
                    'idFamilia'   => $this->input->post('idFamilia'),
                    'idMarca'     => $this->input->post('marcaProducto'),
                    'modelo'      => $this->input->post('modeloProducto'),
                    'descripcion' => $this->input->post('descripcion'),
                    'visibilidad' => $this->input->post('visibilidad'),
                    'sku'         => $this->input->post('sku'),
                    'precio'      => $this->input->post('precio'),
                    'peso'        => $this->input->post('peso'),
                    'stock'       => $this->input->post('stock'),
                    'dimensiones' => json_encode($dimensiones),
                    'imagen'      => json_encode($uploadsImages['img_url_arr'])
                    // 'state' => $this->input->post('state'),
                );
                $producto_id = $this->Producto_model->add_producto($params);

                // categorias
                if ($this->input->post('cats')) {
                    if (is_array($this->input->post('cats')) && count($this->input->post('cats') > 0)) {
                        $this->load->model('ProductoCategoria_model');
                        foreach ($this->input->post('cats') as $index => $cat) {
                            $params = [
                                'idProducto'  => $producto_id,
                                'idCategoria' => $cat
                            ];
                            $this->ProductoCategoria_model->add_productCat($params);
                        }
                    }
                }

                // atributos
                $this->load->model('ProductoAtributo_model');
                foreach ($this->input->post('atributo') as $familia => $atributo) {
                    foreach ($atributo as $idAtributo => $atributoValue) {
                        $valores = [];
                        foreach ($atributoValue as $key => $value) {
                            array_push($valores, $value);
                        }
                        $params = [
                            'idProducto' => $producto_id,
                            'idAtributo' => $idAtributo,
                            'valores'    => json_encode($valores)
                        ];
                        $this->ProductoAtributo_model->add_producto_atributo($params);
                    }
                }

                // variaciones
                if ( isset( $_POST['variacion_sku'] ) ) {
                    $varSkuCount = count( $_POST['variacion_sku'] );
                    $varJSON = array();
                    for ( $i=0; $i < $varSkuCount; $i++ ) { 
                        
                        $varJSON[] = [  
                                        'sku' => $_POST['variacion_sku'][$i],
                                        'precio' => $_POST['variacion_precio'][$i],
                                        'talle' => $_POST['variacion_talle'][$i],
                                     ];

                    }
                    // idAtributo = 3 - Talle
                    $insertVar = [ 'idProducto' => $producto_id, 'idAtributo' => 3, 'valores' => json_encode($varJSON), 'is_variacion' => true ];
                    $result = $this->Producto_model->add_variacion( $insertVar );
                }

                // aplicacion
                $aplicacion = $_POST['aplicacion'];
                unset($aplicacion[0]);
                $aplicacion = array_unique($aplicacion);
                sort($aplicacion);
                $this->load->model('ProductoAplicacion_model');
                $params = [
                    'idProducto' => $producto_id,
                    'arrMoto'    => []
                ];
                foreach ($aplicacion as $key => $idMoto) {
                    array_push($params['arrMoto'], $idMoto);
                    // aplicacion_producto
                    $appProd = $this->ProductoAplicacion_model->get_prodApp_by_idMoto($idMoto);
                    if (is_array($appProd)) {
                        $arrAppProd = json_decode($appProd['arrProd']);
                        $producto_id = (string)$producto_id;
                        array_push($arrAppProd, $producto_id);
                        $params2 = [
                            'idMoto'  => $idMoto,
                            'arrProd' => json_encode($arrAppProd)
                        ];
                        $this->ProductoAplicacion_model->update_aplicacion_producto($appProd['idAppProd'], $params2);
                    }else{
                        $params2 = [
                            'idMoto'  => $idMoto,
                            'arrProd' => [$producto_id]
                        ];
                        $params2['arrProd'] = json_encode($params2['arrProd']);
                        $this->ProductoAplicacion_model->add_aplicacion_producto($params2);
                    }
                }
                // producto_aplicacion
                $params['arrMoto'] = json_encode($params['arrMoto']);
                $this->ProductoAplicacion_model->add_producto_aplicacion($params);

                $this->session->set_flashdata('success_message', 'Producto guardado exitosamente!');
                redirect( 'producto/edit/' . $producto_id, 'refresh' );

        }
        else
        {
            $this->load->model('Familium_model');
            $data['all_familia'] = $this->Familium_model->get_all_familia();
            $this->load->model('Marca_model');
            $data['all_marca'] = $this->Marca_model->get_all_marca();
            $this->load->model('Atributo_model');
            $data['all_atribito'] = $this->Atributo_model->get_all_atributo_join();

            $data['_view'] = 'producto/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }

    /*
     * Editing a producto
     */
    function edit($idProducto)
    {
        // check if the producto exists before trying to edit it
        $producto =  $this->Producto_model->get_producto($idProducto);
        $data['producto'] = $producto;
        $data['idProducto'] = $idProducto;
        
        if(isset($data['producto']['idProducto']))
        {

            if(isset($_POST) && count($_POST) > 0)
            {
                // echo '<pre>';
                // var_dump($this->input->post());
                // die('here');
                // producto
                $this->updateProductTable($idProducto);

                // categorias
                $this->updateProductCat($idProducto);

                // atributos
                // $this->updateProductAttr($idProducto);

                // variciones
                // $this->updateVariacion($idProducto);

                // aplicacion
                $this->updateAplicacion($idProducto);

                $this->session->set_flashdata('success_message', 'Producto editado exitosamente!');
                redirect( 'producto/edit/' . $idProducto, 'refresh' );
            }
            else
            {
                $this->load->model('ProductoAplicacion_model');
                $data['aplicacion'] = $this->ProductoAplicacion_model->get_prodApp_by_idProducto($idProducto);
                $this->load->model('Familium_model');
                $data['all_familia'] = $this->Familium_model->get_all_familia(); 
                $this->load->model('Marca_model');
                $data['all_marca'] = $this->Marca_model->get_all_marca();
                $this->load->model('ProductoCategoria_model');
                $data['cats']= $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
                $this->load->model('ProductoAtributo_model');
                // Atributos por Familia
                $data['atributos_familia'] = $this->ProductoAtributo_model->atributos_por_familia($producto['idFamilia']);

                $data['atributos'] = $this->ProductoAtributo_model->get_all_atributo_by_product($idProducto);

                // Variacion
                $data['variaciones'] = ( $this->ProductoAtributo_model->is_variable($idProducto) ) ? $this->ProductoAtributo_model->get_all_variaciones_by_product($idProducto) : null ; 

                $this->load->model('Moto_model');
                $data['aplicacion']['motos'] = [];
                if (isset($data['aplicacion']['arrMoto']) && $data['aplicacion']['arrMoto'] != '') {
                    $motos  = json_decode($data['aplicacion']['arrMoto']);
                    foreach ($motos as $key => $moto) {
                        array_push($data['aplicacion']['motos'], $this->Moto_model->get_moto($moto));
                    }
                }
                $data['_view'] = 'producto/edit';
                $data['user'] = $this->user;
                // echo '<pre>';
                // var_dump($data);
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The producto you are trying to edit does not exist.');
    }

    public function updateProductTable($idProducto){
        if ($_FILES['imagen']['name'][0] != '') {
            $uploadsImages = $this->uploadsImages($_FILES, $this->input->post('idFamilia'));
            if (count($uploadsImages['error']) != 0) {
                // TODO: manejo de error al subir imagenes
                echo '<ul class ="error error-upload">';
                foreach ($uploadsImages['error'] as $key => $error) {
                    printf('<li>%s -- %s</li>', $key, $error);
                }
                echo '</ul>';
            }
        }else{
            $uploadsImages['img_url_arr'] = $this->input->post('upload');
        }

        // add
        $dimensiones = array(
            'alto'        => $this->input->post('alto'),
            'largo'       => $this->input->post('largo'),
            'ancho'       => $this->input->post('ancho')
        );
        $params = array(
            'nombre'      => $this->input->post('nombre'),
            'slug'        => $this->input->post('slug'),
            'idFamilia'   => $this->input->post('idFamilia'),
            'idMarca'     => $this->input->post('marcaProducto'),
            'modelo'      => $this->input->post('modeloProducto'),
            'descripcion' => $this->input->post('descripcion'),
            'visibilidad' => $this->input->post('visibilidad'),
            'sku'         => $this->input->post('sku'),
            'precio'      => $this->input->post('precio'),
            'peso'        => $this->input->post('peso'),
            'stock'       => $this->input->post('stock'),
            'dimensiones' => json_encode($dimensiones),
            'imagen'      => json_encode($uploadsImages['img_url_arr'])
            // 'state' => $this->input->post('state'),
        );
        $this->Producto_model->update_producto($idProducto, $params);
    }

    public function updateProductCat($idProducto){
        // $this->load->model('ProductoCategoria_model');
        // $old_cats = $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
        // $new_cats = $this->input->post('cats');
        // var_dump($new_cats!=null);
        // if($new_cats!=null){
        //     if (!$this->arrayCompare($old_cats, $new_cats)) {
        //         foreach ($old_cats as $key => $cat_saved) {
        //             $this->ProductoCategoria_model->delete_productCat($cat_saved['idProdCat']);
        //         }
        //         foreach ($this->input->post('cats') as $index => $cat) {
        //             $params = [
        //                 'idProducto'  => $idProducto,
        //                 'idCategoria' => $cat
        //             ];
        //             $this->ProductoCategoria_model->add_productCat($params);
        //         }
        //     }
        // }
    }

    public function updateProductAttr( $idProducto ){
        // $this->load->model('ProductoAtributo_model');
        if ( isset( $_POST['atributo_nombre'] ) && isset( $_POST['atributo_valores'] ) ) {
            $attrSkuCount = count( $_POST['attriacion_sku'] );
            $attrJSON = array();
            for ( $i=0; $i < $attrSkuCount; $i++ ) { 
                
                $attrJSON[] = [  
                                'idAtributo' => $_POST['attriacion_sku'][$i],
                                'idProducto' => $_POST['attriacion_precio'][$i],
                                'valores' => $_POST['attriacion_precio'][$i],
                                'is_variacion' => $_POST['attriacion_talle'][$i],
                             ];

            }
            // idAtributo = 3 - Talle
            $insertattr = [ 'idProducto' => $idProducto, 'idAtributo' => 3, 'valores' => json_encode($attrJSON), 'is_attriacion' => true ];
            $result = $this->Producto_model->update_atributo( $idProducto, 3, $insertattr );

            if( $result == false ){
                $this->session->set_flashdata('alert_message', 'Hubo un problema y no se pudo actualizar las attriaciones!');
            }

        }
    }

    public function updateVariacion($idProducto){

        // variaciones
        if ( isset( $_POST['variacion_sku'] ) ) {
            $varSkuCount = count( $_POST['variacion_sku'] );
            $varJSON = array();
            for ( $i=0; $i < $varSkuCount; $i++ ) { 
                
                $varJSON[] = [  
                                'sku' => $_POST['variacion_sku'][$i],
                                'precio' => $_POST['variacion_precio'][$i],
                                'talle' => $_POST['variacion_talle'][$i],
                             ];

            }
            // idAtributo = 3 - Talle
            $insertVar = [ 'idProducto' => $idProducto, 'idAtributo' => 3, 'valores' => json_encode($varJSON), 'is_variacion' => true ];
            $result = $this->Producto_model->update_variacion( $idProducto, 3, $insertVar );

            if( $result == false ){
                $this->session->set_flashdata('alert_message', 'Hubo un problema y no se pudo actualizar las variaciones!');
            }

        }

    }

    public function updateAplicacion($idProducto){
        if (isset($_POST['aplicacion']) && $_POST['aplicacion'] != '') {
            $this->load->model('ProductoAplicacion_model');
            $old_aplicacion = $this->ProductoAplicacion_model->get_prodApp_by_idProducto($idProducto);
            $idProdApp = $old_aplicacion['idProdApp'];
            $old_aplicacion = (is_array(json_decode($old_aplicacion['arrMoto']))) ? json_decode($old_aplicacion['arrMoto']) : [];
            $new_aplicacion = $_POST['aplicacion'];
            unset($new_aplicacion[0]);
            $new_aplicacion = array_unique($new_aplicacion);
            sort($new_aplicacion);

            if (!$this->arrayCompare($old_aplicacion, $new_aplicacion)) {
                $this->updateProductoAplicacionTable($idProdApp, $idProducto, $new_aplicacion);
                $this->updateAplicacionProductoTable($idProducto, $old_aplicacion, $new_aplicacion);
            }
        }
    }

    public function updateProductoAplicacionTable($idProdApp, $idProducto, $arrMoto){
        $params = [
            'idProducto' => $idProducto,
            'arrMoto'    => json_encode($arrMoto)
        ];
        $this->ProductoAplicacion_model->update_producto_aplicacion($idProdApp, $params);
    }

    public function updateAplicacionProductoTable($idProducto, $old_aplicacion, $new_aplicacion){
        echo '<pre>';
        $removeApp = array_diff($old_aplicacion, $new_aplicacion);
        $addApp    = array_diff($new_aplicacion, $old_aplicacion);

        if (count($removeApp) > 0) {
            foreach ($removeApp as $key => $oldMoto) {
               $strProdOld = $this->ProductoAplicacion_model->get_prodApp_by_idMoto($oldMoto);
                echo 'quitar in <br>';
                var_dump($strProdOld);
               if ($strProdOld) {
                   $arrProdOld = json_decode($strProdOld['arrProd']);
                    echo 'json decode arrProdOld <br>';
                    var_dump($arrProdOld);
                   $arrProdNew = $this->quitar_item($idProducto, $arrProdOld);
                   $paramsForRemove = [
                        'idMoto'  => $oldMoto,
                        'arrProd' => json_encode($arrProdNew)
                   ];
                    echo 'quitar out <br>';
                    var_dump($paramsForRemov);
                   $this->ProductoAplicacion_model->update_aplicacion_producto($strProdOld['idAppProd'],$paramsForRemove);
               }
            }
            echo '<br>Entro en quitar<br>';
        }

        if (count($addApp) > 0) {
            foreach ($new_aplicacion as $key => $newVsOld) {
                $strProdOld = $this->ProductoAplicacion_model->get_prodApp_by_idMoto($newVsOld);
                var_dump($strProdOld);
                if (!is_array($strProdOld)) {
                    $strProdOld['arrProd'] = [];
                    echo '<br>query vacio ----------';
                }else{
                    if (!is_array($strProdOld['arrProd']) || $strProdOld['arrProd'] == null  || $strProdOld['arrProd'] == false  || $strProdOld['arrProd'] == '') {
                        $strProdOld['arrProd'] = [];
                        echo '<br>no hay data ----------';
                    }else{
                        echo '<br>bien!, hay data ----------';
                    }
                }
                $strProdOld['arrProd'] = json_encode($strProdOld['arrProd']);

                var_dump($strProdOld);
                echo '<hr>';


               //  echo 'from model------';
               //  var_dump($strProdOld);
               // if ($strProdOld) {
               //      $arrProdOld = json_decode($strProdOld['arrProd']);
               //      if (!is_array($arrProdOld || $arrProdOld == 'null')) {
               //          $arrProdOld = [];
               //      }
               //      array_push($arrProdOld, $idProducto);
               //      $paramsForAdd = [
               //          'idMoto'  => $newVsOld,
               //          'arrProd' => $arrProdNew
               //      ];
               //      echo 'params------';
               //      var_dump($paramsForAdd);
               //     $this->ProductoAplicacion_model->update_aplicacion_producto($strProdOld['idAppProd'],$paramsForAdd);
               // }else{
               //      $paramsNew = [
               //          'idMoto'  => $idMoto,
               //          'arrProd' => array($idProducto)
               //      ];
               //      $paramsNew['arrProd'] = json_encode($paramsNew['arrProd']);
               //      $this->ProductoAplicacion_model->add_aplicacion_producto($paramsNew);
               // }
            }
            echo '<br>Entro en agregar<br>';
        }
        echo $this->db->last_query().'<br>';
        var_dump($old_aplicacion);
        var_dump($new_aplicacion);
        var_dump($removeApp);
        var_dump($addApp);
    }

    public function arrayCompare($a, $b){
        $equal = false;
        $d = array_diff($a, $b);
        $e = array_diff($b, $a);

        if (count($d) == 0 && count($e) == 0) {
            $equal = true;
        }
        return $equal;
    }

    /*
     * Deleting producto
     */
    function remove($idProducto)
    {
        $producto = $this->Producto_model->get_producto($idProducto);

        // check if the producto exists before trying to delete it
        if(isset($producto['idProducto']))
        {
            $this->Producto_model->delete_producto($idProducto);
            redirect('producto/index');
        }
        else
            show_error('The producto you are trying to delete does not exist.');
    }

    function uploadsImages($files, $idFamilia){
        $extensiones_permitidas = ["png", "bmp", "jpg", "jpeg", "gif"];

        $uploadsImages['img_url_arr'] = [];
        $uploadsImages['error'] = [];

        foreach ($files['imagen']['name'] as $index => $name) {
            $imgName     = $files['imagen']['name'][$index];
            $imgTempPath = $files['imagen']['tmp_name'][$index];
            $imgNameArr  = explode('.', $imgName);
            $last        = count($imgNameArr);
            $ext         = $imgNameArr[$last -1];

            if (in_array($ext, $extensiones_permitidas)) {

                if ($_FILES['imagen']['error'][$index] != 1) {

                    // $idFamilia = $this->input->post('idFamilia');
                    if ($idFamilia != '' && $idFamilia != 0) {

                        $this->load->model('Familium_model');
                        $familiaRow  = $this->Familium_model->get_familium($idFamilia);
                        $familiaSlug = $familiaRow['slug'];
                        $path        = FCPATH.'resources/img/uploads/'.$familiaSlug.'/';

                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                            chmod($path, 0777);
                        }

                        $destino = $path.$this->input->post('slug').$index.'.'.$ext;
                        $img_url = 'resources/img/uploads/'.$familiaSlug.'/'.$this->input->post('slug').$index.'.'.$ext;
                        copy($imgTempPath, $destino);

                        array_push($uploadsImages['img_url_arr'], $img_url);
                    }else{
                        // error se debe seleccionar una familia
                        $error = [
                            'index' => $index,
                            'mesage' => 'Error, se debe seleccionar una familia para gusrdado de las imagenes '.$index
                        ];
                        array_push($uploadsImages['error'], $error);
                    }
                }else{
                    // error en upload de al menos un archivo
                    $error = [
                        'index' => $index,
                        'mesage' => 'Error al subir el archivo  '.$index
                    ];
                    array_push($uploadsImages['error'], $error);
                }
            }else{
                // extensiones no permitidas
                $error = [
                    'index' => $index,
                    'mesage' => 'Error de extension '.$ext
                ];
                array_push($uploadsImages['error'], $error);
            }
        }
        return $uploadsImages;
    }

    public function getProductCats($idProducto = '', $print = false){{
        if ($idProducto == '') {
            $idProducto = $this->uri->segment(3);
        }

        if ($idProducto != '') {

            $this->load->model('ProductoCategoria_model');
            $out = $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);

            if ($print) {
                $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($out));
            }else{
                return $out;
            }
        }
    }}

    public function quitar_item($valor ,$arr){
        if (is_array($arr)) {
            foreach (array_keys($arr, $valor) as $key) {
                unset($arr[$key]);
            }
        }
        return $arr;
    }

    public function renombrarFotosSkuXFam($fam){
        $cambios = 0;
        $sueltas = array();
        $productos = $this->Producto_model->get_all_producto($fam);
        foreach ($productos as $producto) {
            //$newfiles = "[";
            for ($i=1; $i < 6; $i++) {
                echo "<pre>";
                $file = getcwd().'/resources/img/uploads/'.$fam.'/'.$producto['sku'].'-'.$i.'.jpg';
                var_dump(file_exists( $file ));
                if(file_exists( $file ) ){
                    $newfile = str_replace($producto['sku'].'-',$producto['slug'], $file);
                    rename($file, $newfile);
                    var_dump($file,$newfile); $cambios++;
                }
                else {
                    $sueltas[] = $producto['sku'];
                }
                echo "</pre>";
            }
        }
                echo "<pre>";
                    var_dump('sin cambios, prods sin fotos: '.$fam.': ',$sueltas);
                    var_dump('cambios: '.$cambios);
                echo "</pre>";
    }

    public function guardarImgXProd($fam){
        $cambios = 0;
        $sueltas = array();
        $productos = $this->Producto_model->get_all_producto($fam);
        foreach ($productos as $producto) {
            $images = "[";
            for ($i=1; $i < 6; $i++) {
                echo "<pre>";
                $file = getcwd().'/resources/img/uploads/'.$fam.'/'.$producto['slug'].$i.'.jpg';
                echo $file;
                if(file_exists( $file ) ){
                    $cambiados[] = $producto['slug'];
                    $cambios++;
                    $images .= '"'.str_replace(getcwd().'/','', $file).'",';
                }
                else {
                    $sueltas[] = $producto['slug'];
                }
                $params = array(
                    'imagen'      => rtrim(str_replace('/','\/', $images),',')."]"
                );
                echo "</pre>";
                $this->Producto_model->update_producto($producto['idProducto'], $params);

            }

        }
        echo "<pre>";
            var_dump('sin cambios, sin fotos de '.$fam.': ',$sueltas);
            var_dump('cambios: '.$cambios);
         //   var_dump('cambiados: ',array_unique($cambiados) );
        echo "</pre>";
    }

    public function updateBulkPrice()
	{
		$data['_view'] = 'producto/bulk/update';
		$data['user'] = $this->user;
		$this->load->view('layouts/main', $data);
	}

    public function do_updateBulkPrice()
	{
		$config['allowed_types'] = 'xls';
		$config['upload_path'] = './uploads/';

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('excel')) {
			$data = $this->upload->data();
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			$spreadsheet = $reader->load($data['full_path']);

			$worksheet = $spreadsheet->getActiveSheet();
			$highestRow = $worksheet->getHighestRow();

			$errors = array();
			$updated = 0;

			for ($i = 2; $i <= $highestRow; $i++) {
				$sku = $worksheet->getCellByColumnAndRow(1, $i)->getValue();
				$precio = $worksheet->getCellByColumnAndRow(2, $i)->getValue();

				$producto = $this->Producto_model->get_producto_by_sku($sku);

				if ($producto) {
					$params = array(
						'precio' => $precio,
					);

					$this->Producto_model->update_producto($producto['idProducto'], $params);
					$updated++;
				} else {
					$errors[] = array('sku' => $sku, 'precio' => $precio);
				}
			}

			$params = array(
				'filename' => $data['orig_name'],
				'updated' => $updated,
				'errors'=> json_encode($errors),

			);

			$this->load->model('BulkUpdateLogs_model');
			$idBulkUpdateLogs = $this->BulkUpdateLogs_model->add_bulk_update_logs($params);

			redirect('producto/bulk_show/'.$idBulkUpdateLogs);
		} else {
			var_dump($this->upload->display_errors());
		}
	}

	public function bulk_show($idBulkUpdateLogs)
	{
		$this->load->model('BulkUpdateLogs_model');
		$bulkUpdateLogs = $idBulkUpdateLogs = $this->BulkUpdateLogs_model->get_bulk_update_logs($idBulkUpdateLogs);

		$data['_view'] = 'producto/bulk/show';
		$data['user'] = $this->user;
		$data['bulkUpdateLogs'] = $bulkUpdateLogs;
		$this->load->view('layouts/main', $data);
	}

	public function bulk_index()
	{
		$data['_view'] = 'producto/bulk/index';
		$data['user'] = $this->user;
		$this->load->view('layouts/main', $data);
	}

	public function get_bulk_list_dt(){

		//Paginado
		$start = $this->input->post('start');

		//Ver
		$length = $this->input->post("length");

		//Busqueda
		$search = $this->input->post("search")['value'];

		$this->load->model('BulkUpdateLogs_model');
		$result = $this->BulkUpdateLogs_model->get_all_bulk_update_logs($start, $length, $search);

		$resultado = $result["datos"];

		$numrows   = $result["numRows"];
		$numrowstotal = $this->BulkUpdateLogs_model->getNumRowAll();

		$data = array();

		foreach ($resultado->result_array() as $result_row) {

			$row_of_data = array();

			$row_of_data[] = $result_row["filename"];
			$row_of_data[] = $result_row["updated"];
			$row_of_data[] = $result_row["fecha"];
			$row_of_data[] = "<a href='". base_url() . 'producto/bulk_show/'  . $result_row["idBulkUpdateLogs"] . "' class='btn btn-info btn-xs'><span class='fa fa-pencil'></span> Ver</a>";

			$data[] = $row_of_data;

		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($numrowstotal),
			"recordsFiltered" => intval($numrows),
			"data"            => $data
		);


		echo json_encode($json_data);

    }
    public function get_children(){

        $input = $this->input->post('data');
        foreach ($input as  $value) {
            if ($value['name'] == 'idProducto'){
                $idProducto = $value['value'];
            }
        }

        $childs = $this->Producto_model->get_child_by_parent($idProducto);

        $item = array();
        $anterior = "";

        foreach ($childs as $child) {
        
            if ($anterior != $child['idProducto'] && !empty($item)){
                
                $list[] = $item;
                $item = array();
                
            } 
            $item['idProducto']   = $child['idProducto'];
            $item['prod_nombre']  = $child['prod_nombre'];
            $item['sku']          = $child['sku'];
            $item['precio']       = $child["precio"];
            $item['show_price']   = show_price($child["precio"]);
            $item[$child['slug']] = $child["valores"];
            $anterior = $child['idProducto'];           

        }
        $list[] = $item;

       
        // Recorro la lista de Childrens
        foreach ($list as $key => $item) {
            
            //Recorro los atributos seleccionados en pantalla
            $save = true;
            foreach ($input as $value){         
            
                
                if ($value['name'] != 'idProducto'){
            
                    if ($item[$value['name']] != $value['value']){
                        $save = false;
                    }
                }   
            }

            if ($save == true){
                $result[] = $item;
            }

        }

           if (!empty($result)){        
                echo json_encode($result);
            }else{
                header('Content-Type: application/json');
                echo "string whithout json_encode";
                exit();
            } 

        

    }


    function check_sku_exist($sku = null){

        $sku = ( isset( $_REQUEST['sku'] ) ) ? $_REQUEST['sku'] : $sku ;

        if ( $sku !== null) {

            $count = $this->Producto_model->get_producto_by_sku( $sku, 'num_rows' );
            $varCount = $this->db->like('valores', '[{"sku":"'. $sku .'"')
                                 ->get('producto_atributo')
                                 ->num_rows();

            if ( $count == 0  && $varCount == 0 ) {
                $return = ['code' => 1, 'message' => null];
            }else{
                $return = ['code' => 0, 'message' => 'El SKU ya esta registrado'];
            }

        }else{
                $return = ['code' => 0, 'message' => 'No hay ningun SKU'];
        }

            $this->output
                    ->set_content_type('application/json')
                    ->set_output( json_encode($return) );

    }

    function atributos_familia_json( $idFamilia = null, $nombre = null ){

        $idFamilia = ( isset( $_REQUEST['idFamilia'] ) ) ? $_REQUEST['idFamilia'] : $idFamilia ;
        $nombre = ( isset( $_REQUEST['nombre'] ) ) ? $_REQUEST['nombre'] : $nombre ;
 
        $atributos = $this->db->where(  [   
                                            'idFamilia' => $idFamilia,
                                            'nombre' => $nombre
                                        ] )
                              ->get('atributo')
                              ->result_array(); 

        $a = array();

        foreach ( $atributos as $atributo ) { 

            if ( is_array( json_decode( $atributo['valor'], true ) ) ) {

                $json = json_decode( $atributo['valor'] );

                foreach ( $json as $value ) {
                    $a[] = $value; 
                }

            }

        }

        $this->output
                ->set_content_type( 'application/json' )
                ->set_output( json_encode($a) );

    }

    function producto_variaciones_json( $idProducto = null, $idAtributo = null ){

        $this->load->model('Producto_model', 'Producto', true); 

        $idProducto = ( isset( $_REQUEST['idProducto'] ) ) ? $_REQUEST['idProducto'] : $idProducto ;
        $idAtributo = ( isset( $_REQUEST['idAtributo'] ) ) ? $_REQUEST['idAtributo'] : $idAtributo ;

        $options = $this->Producto->get_variations_options( $idProducto, $idAtributo );
        
        $optionsSelected = $this->Producto->get_variations_options_selected( $idProducto, $idAtributo );

        $response = [ 'options' => $options, 'optionsSelected' => $optionsSelected ];

        $this->output
                ->set_content_type( 'application/json' )
                ->set_output( json_encode( $response ) );

    }

    function ko_db_atributos( $idProducto = null ){

        $this->load->model('Atributo_model', 'Atributo', true); 

        $idProducto = ( isset( $_REQUEST['idProducto'] ) ) ? $_REQUEST['idProducto'] : $idProducto ;  

        $atributos = $this->db->where(  [   
                                            'idProducto' => $idProducto, 
                                        ] )
                              ->get('producto_atributo')
                              ->result_array(); 

        $idAtributosArray = $a = array();
        $isVariacionArray = array();
        $atributosDisponibles = array();

        // Construye array de los atributos registrados
        foreach ( $atributos as $atributo ) {  

            if ( ! in_array( $atributo['idAtributo'], $idAtributosArray ) ) {
                // Array de id's
                $idAtributosArray[] = $atributo['idAtributo'];
                // Array de is_variacion
                $is_variacion = ( $atributo['is_variacion'] == null ) ? 0 : $atributo['is_variacion'] ;
                $isVariacionArray[] = $is_variacion;
                // valores para select en Knockout
                $atributosDisponibles[] = $this->Atributo->get_valores_por_atributo( $atributo['idAtributo'] );
            } 

        }   

        $json = array();
        // Recorre el array y construye el json para knockout  
        for ($i=0; $i < count($idAtributosArray); $i++) {  
                    
            $atributoData = $this->Atributo->get_atributo( $idAtributosArray[$i] );

            $json[$i] = array(  
                                'idAtributo' => $idAtributosArray[$i], 
                                'nombre' => $atributoData['nombre'],
                                'atributos' => $atributosDisponibles[$i],
                                'valores' => [],
                                'is_variacion' => $isVariacionArray[$i],
                            );

            foreach ( $atributos as $atributo ) { 

                // Si es el atributo almacenarlo en array
                if ( $atributo['idAtributo'] == $idAtributosArray[$i] ) {
                    $json[$i]['valores'][] = $atributo['valores'];
                }

            } 
 
        } 

        $this->output
                ->set_content_type( 'application/json' )
                ->set_output( json_encode( $json ) );

    }

    function ko_db_variaciones( $idProducto = null ){

        $this->load->model('Producto_model', 'Producto', true);
        $this->load->model('ProductoAtributo_model', 'PA', true); 

        $idProducto = ( isset( $_REQUEST['idProducto'] ) ) ? $_REQUEST['idProducto'] : $idProducto ;  
        
        $return = array();

        $variaciones = $this->db->where(  [   
                                            'idParent' => $idProducto, 
                                            'idTipo' => 2
                                        ] )
                              ->get('producto')
                              ->result_array();

        foreach ( $variaciones as $variacion ) {

            $atributo = $this->db->get_where( 'producto_atributo', [ 'idProducto' => $variacion['idProducto'] ] )->result_array();

            $return[] = array(
                                'producto' => $variacion,
                                'atributos' => $atributo
                        );
        }

        $this->output
                ->set_content_type( 'application/json' )
                ->set_output( json_encode( $return ) );

    }

    function guardar_producto_atributos(){

        $idProducto = ( isset( $_REQUEST['idProducto'] ) ) ? $_REQUEST['idProducto'] : $idProducto ;
        $idAtributo = ( isset( $_REQUEST['idAtributo'] ) ) ? $_REQUEST['idAtributo'] : $idAtributo ;
        $valores = ( isset( $_REQUEST['valores'] ) ) ? $_REQUEST['valores'] : $valores ;
        $is_variacion = ( isset( $_REQUEST['is_variacion'] ) ) ? $_REQUEST['is_variacion'] : $is_variacion ;

        // Si no hay ninguno vacio
        if ( $idProducto == '' && $idAtributo == '' && $valores == '' ) {
            $return = [ 'code' => 0, 'message' => 'No hay valores' ];
            $this->output
                    ->set_content_type( 'application/json' )
                    ->set_output( json_encode($return) );
            return false;
        } 

        $batch = array();

        for ($i=0; $i < count($idAtributo); $i++) {  

            // Delete
            $delete =  $this->db->where( ['idProducto' => $idProducto, 'idAtributo' => $idAtributo[$i] ] )
                                ->delete( 'producto_atributo' );

            // Si no lo borra, salir
            if ( ! $delete ) {
                $return = [ 'code' => 0, 'message' => 'No se pudo eliminar los registros', 'sql' => $this->db->last_query() ];
                $this->output
                        ->set_content_type( 'application/json' )
                        ->set_output( json_encode($return) );
                exit;
            } 
 
            // Recorre los valores
            // foreach ( $valores[$i] as $valor ) {

                $batch[] = array(
                            'idAtributo' => $idAtributo[$i],
                            'idProducto' => $idProducto,
                            'valores' => json_encode( $valores[$i] ),
                            'is_variacion' => $is_variacion[$i],
                ); 

            // }   

        }

        $insert = $this->db->insert_batch('producto_atributo', $batch);

        if ( $insert == true ) {
            $return = ['code' => 1, 'message' => null, 'batch' => json_encode( $batch ) ];
        }else{
            $return = ['code' => 0, 'message' => 'No se pudo actualizar la base de datos'];
        }

        $this->output
                ->set_content_type( 'application/json' )
                ->set_output( json_encode($return) );

    }

    // public function atributo_es_variable( $idAtributo ){

    //     $query = $this->db->get_where( 'atributo', [ 
    //                                                 'idAtributo' => $idAtributo,
    //                                                 'variable' => 1
    //                                                ] );
    //     $count = $query->num_rows();

    //     echo $count;

    // }

    public function guardar_producto_variaciones(){


        $this->load->model('Producto_model', 'Producto', true); 
        $this->load->model('ProductoAtributo_model', 'PA', true); 

        $idProducto = ( isset( $_REQUEST['idProducto'] ) ) ? $_REQUEST['idProducto'] : null ;  
        $slug = ( isset( $_REQUEST['variacion_slug'] ) ) ? $_REQUEST['variacion_slug'] : null ;
        $sku = ( isset( $_REQUEST['variacion_sku'] ) ) ? $_REQUEST['variacion_sku'] : null ;  
        $precio = ( isset( $_REQUEST['variacion_precio'] ) ) ? $_REQUEST['variacion_precio'] : null ;  
        $stock = ( isset( $_REQUEST['variacion_stock'] ) ) ? $_REQUEST['variacion_stock'] : null ;  
        $atributo = ( isset( $_REQUEST['variacion_atributo'] ) ) ? $_REQUEST['variacion_atributo'] : null ;  

        $currentProduct = $this->Producto->get_producto( $idProducto ); 

        // Valida ID Producto
        if ( empty( $idProducto ) ){

            $response = [ 'code' => 0, 'message' => 'No hay un id de producto' ];
            $this->output
                    ->set_content_type( 'application/json' )
                    ->set_output( json_encode( $response ) );
            return false;

        }

        if( $sku == null || $precio == null || $stock == null || $atributo == null ){

            $response = [ 'code' => 0, 'message' => 'No hay valores' ];
            $this->output
                    ->set_content_type( 'application/json' )
                    ->set_output( json_encode( $response ) );

            return false;

        }
 
        $product_inserts = array();

        // Borrar registros de productos variables
        $delete =  $this->db->where( [ 'idParent' => $idProducto ] )
                            ->delete( 'producto' );

        // Si no lo borra, salir
        if ( ! $delete ) {
            $return = [ 'code' => 0, 'message' => 'No se pudo eliminar los productos variables', 'sql' => $this->db->last_query() ];
            $this->output
                    ->set_content_type( 'application/json' )
                    ->set_output( json_encode($return) );
            exit;
        }

        for ($i=0; $i < count( $sku ); $i++) { 

            // add 
            $data = array(
                                'nombre'      => $currentProduct['nombre'],
                                'slug'        => $currentProduct['slug'],
                                'idFamilia'   => $currentProduct['idFamilia'],
                                'idMarca'     => $currentProduct['idMarca'],
                                'modelo'      => $currentProduct['modelo'],
                                'descripcion' => $currentProduct['descripcion'],
                                'visibilidad' => 'publicar',
                                'sku'         => $sku[$i],
                                'precio'      => $precio[$i],
                                'peso'        => $currentProduct['peso'],
                                'stock'       => $stock[$i],
                                'dimensiones' => $currentProduct['dimensiones'],
                                'imagen'      => $currentProduct['imagen'], 
                                'idParent' => $idProducto,
                                'idTipo' => 2
                            ); 

            $product_inserts[] = $this->Producto->add_producto( $data );  

        } 

        // Borrar atributos de variaciones registrados 
        foreach ($product_inserts as $id) {

            $delete =  $this->db->where( [ 'idProducto' => $id ] )
                                ->delete( 'producto_atributo' );

            // Si no lo borra, salir
            if ( ! $delete ) {
                $return = [ 'code' => 0, 'message' => 'No se pudo eliminar las variaciones', 'sql' => $this->db->last_query() ];
                $this->output
                        ->set_content_type( 'application/json' )
                        ->set_output( json_encode($return) );
                exit;
            }

        }

        // Insertar atributos de variaciones 
        for ($i=0; $i < count($product_inserts); $i++) { 

            foreach ( $atributo[$i] as $a ) {

                $attribute_insert = array(
                        'idProducto' => $product_inserts[$i],
                        'idAtributo' => $a['idAtributo'],
                        'valores' => json_encode( $a['valores'] ),
                        'is_variacion' => true,
                );

                // Insertar
                $ai = $this->PA->add_producto_atributo( $attribute_insert );

                // Si no inserto
                if ( $ai == false ) {
                    $response = [ 'code' => 0, 'message' => 'No se inserto el atributo para el producto: ' . $product_inserts[$i] ];
                    $this->output
                            ->set_content_type( 'application/json' )
                            ->set_output( json_encode( $response ) );

                    return false;
                }

            }

        } 
 
        $response = [ 'code' => 1, 'message' => null ];
 
        $this->output
                ->set_content_type( 'application/json' )
                ->set_output( json_encode( $response ) );


    }

}
