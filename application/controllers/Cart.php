<?php

include_once(FCPATH."/application/controllers/BaseController.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Cart extends BaseController{
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {

            $this->load->library("cart");   
            $this->load->library('email');
            $this->load->helper("price");    
            $this->load->model('Order_hdr_model');
            $this->load->model('Order_itm_model');

        }else{
            redirect('/', 'refresh');
        }
    }

    public function add_to_cart(){

        $data = array(
            "id"    => $this->input->post("product_id"),
            "name"  => $this->input->post("product_name"),
            "qty"   => $this->input->post("quantity"),
            "price" => $this->input->post("product_price")
        );          

        $r = $this->cart->insert($data);

        if ( $r ) {

            echo json_encode( ['code' => 1, 'message' => 'Producto Agregado Correctamente!', 'console' => null ] );

        }else{

            echo json_encode( ['code' => 0, 'message' => 'No se pudo agregar el producto al pedido. Contacte al administrador', 'console' => $this->db->_error_message() ] );
        }

    }

    public function get_cart_content(){

        $count = 0;
        $output = '<tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Total</th>
                        <th>Acciones</th>
                    </tr>';

        foreach($this->cart->contents() as $items)
        {
            
            $count++;
            $output .= '<tr> 
                            <td>'.$items["name"].'</td>
                            <td><input class="qty" type="number" value="'.$items["qty"].'" id="'.$items["rowid"].'" ></td>
                            <td>'. show_price($items["price"]).'</td>
                            <td>'. show_price($items["subtotal"]).'</td>
                            <td><button type="button" name="remove" class="btn btn-outline-danger btn-sm remove_item" id="'.$items["rowid"].'" data-toggle="tooltip" data-placement="bottom" title="Borrar Item" ><i class="far fa-trash-alt"></i></button>
                            </td>
                        </tr>';
        }

        $output .= '<tr>
                        <td colspan="4" align="right">Total</td>
                        <td>'.show_price($this->cart->total()).'</td>
                    </tr>';

        if($count == 0)
        {
            $output = '<h3 align="center">Cart is Empty</h3>';
        }
        
        echo $output;

    }
        
    public function remove_from_cart(){

        $row_id = $_POST["row_id"];
        
        $data = array(
                  'rowid'  => $row_id,
                   'qty'  => 0
                );

        $r = $this->cart->update($data);

        if ( $r ) {

            echo json_encode( ['code' => 1, 'message' => 'Producto eliminado', 'console' => null ] );

        }else{

            echo json_encode( ['code' => 0, 'message' => 'No se pudo remover el producto del pedido. Contacte al administrador', 'console' => $this->db->_error_message() ] );
        }

    }

    public function update_cart(){

        $row_id = $_POST["row_id"];
        $qty    = $_POST["qty"];
        
        $data = array(
                  'rowid' => $row_id,
                   'qty'  => $qty
                );

        return $this->cart->update($data);
       
    }    

    public function process_order(){

        $this->load->model('Base_model', 'Base', true );
        $this->load->model('User_model', 'User', true );
        $this->load->model('Producto_model', 'Producto', true );

        $userData = $this->User->get_user( $this->session->userdata('id') );

        $order_hdr = array(
                'idUser'             => $this->session->userdata('id'),
                'total'                => $this->cart->total(),
                'idOrderStatus'  => '2', //ENVIADO
                'createdBy'        => $this->session->userdata('id'),
                'created'            => date("Y-m-d"), 
                'updatedBy'       => $this->session->userdata('id') 
                );
        
        $order_r = $this->Order_hdr_model->addOrder($order_hdr);

        $data = array();
        $excel = array();
        $i = 0;
        $total = 0;
        foreach ($this->cart->contents() as $items){
            $i++;    
            $total = $total + $items["subtotal"];
            $item = array(
                        'idOrderHdr '  => $order_r,
                        'posnr'            => $i,
                        'idProducto'   => $items["id"],
                        'precio'           => $items["price"],
                        'qty'                => $items["qty"],
                        'total'             => $items["subtotal"],
                        'options'         => '',
                        'idItemStatus' => 1, //ACTIVO
                        'createdBy'     => $this->session->userdata('id'),
                        'updatedBy'    => $this->session->userdata('id')
                    );    

            // Producto para excel
            $producto = $this->Producto->get_producto( $items["id"] );

            $excel[] = array(
                            'posnr'            => $i,
                            'idOrderHdr '  => $order_r,
                            'desc_producto'   => $producto['nombre'],
                            'precio'           => $items["price"],
                            'qty'                => $items["qty"],
                            'total'             => $items["subtotal"]
                        );    

            $data[] = $item;
        }

        $items_r = $this->Order_itm_model->addItems($data);

        // Excel
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet(); 
        $spreadsheet->getProperties()
                    ->setCreator( 'Monsa Web' )
                    ->setLastModifiedBy( 'Monsa Web' )
                    ->setTitle("Orden Monsa ".date('Y-m-d').".xlsx")
                    ->setSubject("Orden Monsa") 
                    ->setKeywords("office 2007 monsa openxml php");

        $spreadsheet->setActiveSheetIndex(0);

        $spreadsheet->getActiveSheet()->setCellValue('A1', 'ID Item')
                                      ->setCellValue('B1', 'ID Orden')
                                      ->setCellValue('C1', 'Desc. Producto')
                                      ->setCellValue('D1', 'Precio')
                                      ->setCellValue('E1', 'Cantidad') 
                                      ->setCellValue('F1', 'Total Renglon');
     
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 

        $spreadsheet->getActiveSheet()
            ->fromArray(
                $excel,  // The data to set
                NULL,        // Array values with this value will not be set
                'A2'         // Top left coordinate of the worksheet range where
                             //    we want to set these values (default is A1)
            ); 

        // Guarda en uploads
        $filenameExcel = PATH_UPLOAD . "/orders/Orden #" . $order_r . " Monsa " . date('Y-m-d') . ".xlsx";
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save( $filenameExcel );

        // Si se agrega el pedido
        if ( $items_r !== false ) {

            // Enviar Correo
            ob_start(); 
                include_once( APPPATH . 'views/layouts/email/email-header.php' );
                    include_once( APPPATH . 'views/layouts/email/new-order.php' );
                include_once( APPPATH . 'views/layouts/email/email-footer.php');
            $body = ob_get_clean(); 

            // Correo usuario
            $send = $this->Base->send_email_template( 'Nueva Orden #' . $order_r, $userData['email'], $body, $filenameExcel );
            // Correo Admin
            $sendAdmin = $this->Base->send_email_template( 'Tienes una nueva orden #' . $order_r, $this->Base->get_sysopt_value('admin_mail'), $body, $filenameExcel );

            if ( $send ) {

                $this->cart->destroy();

                $this->session->set_flashdata( 'success_message', 'Pedido #' . $order_r . ' generado correctamente.' );

                echo json_encode( [ 'code' => 1, 'url' => base_url( 'customer?pedido=' . $order_r ) ] ); 

            }else{

                $this->session->set_flashdata('alert_message', 'Pedido procesado pero no se pudo enviar el correo.');

                echo json_encode( [ 'code' => 0, 'message' => 'Pedido procesado pero no se pudo enviar el correo' ] );

            } 

        }

    }

    public function send_order_by_mail(){

    }

    public function get_user_cart($id_user, $state=''){
        // return $this->Order_model->get_order_by_user($id_user, $state);
    }

    public function save_cart($params){
        // return $this->Order_model->add_order($params);
    }

    // public function update_cart($idOrder, $params){
    //     // return $this->Order_model->update_order($idOrder, $params);
    // }
}