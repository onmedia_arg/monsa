<?php
// 13525449
include_once(FCPATH."/application/controllers/BaseController.php");
class Producto extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Producto_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of producto
     */
    function index()
    {
        $data['producto'] = $this->Producto_model->get_all_producto();
        $data['_view'] = 'producto/index';
        $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new producto
     */

    function get_product_by_id($idProducto){
        $producto = $this->Producto_model->get_producto($idProducto);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($producto));
    }

    function add()
    {  
        if(isset($_POST) && count($_POST) > 0)     
        {  
            $uploadsImages = $this->uploadsImages($_FILES, $this->input->post('idFamilia'));

            if (count($uploadsImages['error']) != 0) {
                // TODO: manejo de error al subir imagenes
                    echo '<ul class ="error error-upload">';
                    foreach ($uploadsImages['error'] as $key => $error) {
                        printf('<li>%s -- %s</li>', $key, $error);
                    }
                    echo '</ul>';
            }else{
                // add
                $dimensiones = array(
                    'alto'        => $this->input->post('alto'),
                    'largo'       => $this->input->post('largo'),
                    'ancho'       => $this->input->post('ancho')
                );
                $params = array(
                    'nombre'      => $this->input->post('nombre'),
                    'slug'        => $this->input->post('slug'),
                    'idFamilia'   => $this->input->post('idFamilia'),
                    'idMarca'     => $this->input->post('marcaProducto'),
                    'modelo'      => $this->input->post('modeloProducto'),
                    'descripcion' => $this->input->post('descripcion'),
                    'visibilidad' => $this->input->post('visibilidad'),
                    'sku'         => $this->input->post('sku'),
                    'precio'      => $this->input->post('precio'),
                    'peso'        => $this->input->post('peso'),
                    'stock'       => $this->input->post('stock'),
                    'dimensiones' => json_encode($dimensiones),
                    'imagen'      => json_encode($uploadsImages['img_url_arr'])
                    // 'state' => $this->input->post('state'),
                );
                $producto_id = $this->Producto_model->add_producto($params);
                
                // categorias
                if ($this->input->post('cats')) {
                    if (is_array($this->input->post('cats')) && count($this->input->post('cats') > 0)) {
                        $this->load->model('ProductoCategoria_model');
                        foreach ($this->input->post('cats') as $index => $cat) {
                            $params = [
                                'idProducto'  => $producto_id,
                                'idCategoria' => $cat
                            ];
                            $this->ProductoCategoria_model->add_productCat($params);
                        }
                    }
                }

                // atributos
                $this->load->model('ProductoAtributo_model');
                foreach ($this->input->post('atributo') as $familia => $atributo) {
                    foreach ($atributo as $idAtributo => $atributoValue) {
                        $valores = [];
                        foreach ($atributoValue as $key => $value) {
                            array_push($valores, $value);
                        }
                        $params = [
                            'idProducto' => $producto_id,
                            'idAtributo' => $idAtributo,
                            'valores'    => json_encode($valores)
                        ];
                        $this->ProductoAtributo_model->add_producto_atributo($params);
                    }
                }
                
                // aplicacion
                $aplicacion = $_POST['aplicacion'];
                unset($aplicacion[0]);
                $aplicacion = array_unique($aplicacion);
                sort($aplicacion);
                $this->load->model('ProductoAplicacion_model');
                $params = [
                    'idProducto' => $producto_id,
                    'arrMoto'    => []
                ];
                foreach ($aplicacion as $key => $idMoto) {
                    array_push($params['arrMoto'], $idMoto);
                    // aplicacion_producto
                    $appProd = $this->ProductoAplicacion_model->get_prodApp_by_idMoto($idMoto);
                    if (is_array($appProd)) {
                        $arrAppProd = json_decode($appProd['arrProd']);
                        $producto_id = (string)$producto_id;
                        array_push($arrAppProd, $producto_id);
                        $params2 = [
                            'idMoto'  => $idMoto, 
                            'arrProd' => json_encode($arrAppProd)
                        ];
                        $this->ProductoAplicacion_model->update_aplicacion_producto($appProd['idAppProd'], $params2);
                    }else{
                        $params2 = [
                            'idMoto'  => $idMoto, 
                            'arrProd' => [$producto_id]
                        ];
                        $params2['arrProd'] = json_encode($params2['arrProd']);
                        $this->ProductoAplicacion_model->add_aplicacion_producto($params2);
                    }
                }
                // producto_aplicacion
                $params['arrMoto'] = json_encode($params['arrMoto']);
                $this->ProductoAplicacion_model->add_producto_aplicacion($params);

                redirect('producto/index');
            }

        }
        else
        {     
            $this->load->model('Familium_model');
            $data['all_familia'] = $this->Familium_model->get_all_familia();
            $this->load->model('Marca_model');
            $data['all_marca'] = $this->Marca_model->get_all_marca();
            $this->load->model('Atributo_model');
            $data['all_atribito'] = $this->Atributo_model->get_all_atributo_join();

            $data['_view'] = 'producto/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a producto
     */
    function edit($idProducto)
    {   
        // check if the producto exists before trying to edit it
        $data['producto'] = $this->Producto_model->get_producto($idProducto);
        
        if(isset($data['producto']['idProducto']))
        {
            
            if(isset($_POST) && count($_POST) > 0)     
            {  
                // echo '<pre>';
                // var_dump($this->input->post());
                // die('here'); 
                // producto
                $this->updateProductTable($idProducto); 
                
                // categorias
                $this->updateProductCat($idProducto);
                
                // atributos
                $this->updateProductAttr($idProducto);
                
                // aplicacion
                $this->updateAplicacion($idProducto);

                // redirect('producto/index');
            }
            else
            {
                $this->load->model('ProductoAplicacion_model');
                $data['aplicacion'] = $this->ProductoAplicacion_model->get_prodApp_by_idProducto($idProducto);
                $this->load->model('Familium_model');
                $data['all_familia'] = $this->Familium_model->get_all_familia();
                $this->load->model('Marca_model');
                $data['all_marca'] = $this->Marca_model->get_all_marca();
                $this->load->model('ProductoCategoria_model');
                $data['cats']= $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
                $this->load->model('ProductoAtributo_model');
                $data['atributos'] = $this->ProductoAtributo_model->get_all_atributo_by_product($idProducto);
                $this->load->model('Moto_model');
                $data['aplicacion']['motos'] = [];
                if (isset($data['aplicacion']['arrMoto']) && $data['aplicacion']['arrMoto'] != '') {
                    $motos  = json_decode($data['aplicacion']['arrMoto']);
                    foreach ($motos as $key => $moto) {
                        array_push($data['aplicacion']['motos'], $this->Moto_model->get_moto($moto));
                    }
                }
                $data['_view'] = 'producto/edit';
                $data['user'] = $this->user;
                // echo '<pre>';
                // var_dump($data);
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The producto you are trying to edit does not exist.');
    } 
    
    public function updateProductTable($idProducto){
        if ($_FILES['imagen']['name'][0] != '') {
            $uploadsImages = $this->uploadsImages($_FILES, $this->input->post('idFamilia'));
            if (count($uploadsImages['error']) != 0) {
                // TODO: manejo de error al subir imagenes
                echo '<ul class ="error error-upload">';
                foreach ($uploadsImages['error'] as $key => $error) {
                    printf('<li>%s -- %s</li>', $key, $error);
                }
                echo '</ul>';
            }
        }else{
            $uploadsImages['img_url_arr'] = $this->input->post('upload');
        }
        
        // add
        $dimensiones = array(
            'alto'        => $this->input->post('alto'),
            'largo'       => $this->input->post('largo'),
            'ancho'       => $this->input->post('ancho')
        );
        $params = array(
            'nombre'      => $this->input->post('nombre'),
            'slug'        => $this->input->post('slug'),
            'idFamilia'   => $this->input->post('idFamilia'),
            'idMarca'     => $this->input->post('marcaProducto'),
            'modelo'      => $this->input->post('modeloProducto'),
            'descripcion' => $this->input->post('descripcion'),
            'visibilidad' => $this->input->post('visibilidad'),
            'sku'         => $this->input->post('sku'),
            'precio'      => $this->input->post('precio'),
            'peso'        => $this->input->post('peso'),
            'stock'       => $this->input->post('stock'),
            'dimensiones' => json_encode($dimensiones),
            'imagen'      => json_encode($uploadsImages['img_url_arr'])
            // 'state' => $this->input->post('state'),
        );
        $this->Producto_model->update_producto($idProducto, $params);
    }

    public function updateProductCat($idProducto){
        $this->load->model('ProductoCategoria_model');
        $old_cats = $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
        $new_cats = $this->input->post('cats');
        var_dump($new_cats!=null);
        if($new_cats!=null){
            if (!$this->arrayCompare($old_cats, $new_cats)) {
                foreach ($old_cats as $key => $cat_saved) {
                    $this->ProductoCategoria_model->delete_productCat($cat_saved['idProdCat']);
                }
                foreach ($this->input->post('cats') as $index => $cat) {
                    $params = [
                        'idProducto'  => $idProducto,
                        'idCategoria' => $cat
                    ];
                    $this->ProductoCategoria_model->add_productCat($params);
                }
            }
        }
    }

    public function updateProductAttr($idProducto){
        $this->load->model('ProductoAtributo_model');
        $old_attr = $this->ProductoAtributo_model->get_all_atributo_by_product($idProducto);
        $new_attr = $this->input->post('atributo');
        echo '<pre>';
        var_dump($old_attr);
        var_dump($new_attr);
        foreach ($old_attr as $key => $attr_saved) {
            $this->ProductoAtributo_model->delete_producto_atributo($attr_saved['idProdAtrib']);
        }
        foreach ($new_attr as $familia => $atributo) {
            foreach ($atributo as $idAtributo => $atributoValue) {
                $valores = [];
                foreach ($atributoValue as $key => $value) {
                    array_push($valores, $value);
                }
                $params = [
                    'idProducto' => $idProducto,
                    'idAtributo' => $idAtributo,
                    'valores'    => json_encode($valores)
                ];
                $this->ProductoAtributo_model->add_producto_atributo($params);
            }
        }
    }

    public function updateAplicacion($idProducto){
        if (isset($_POST['aplicacion']) && $_POST['aplicacion'] != '') {
            $this->load->model('ProductoAplicacion_model');
            $old_aplicacion = $this->ProductoAplicacion_model->get_prodApp_by_idProducto($idProducto);
            $idProdApp = $old_aplicacion['idProdApp'];
            $old_aplicacion = (is_array(json_decode($old_aplicacion['arrMoto']))) ? json_decode($old_aplicacion['arrMoto']) : [];
            $new_aplicacion = $_POST['aplicacion'];
            unset($new_aplicacion[0]);
            $new_aplicacion = array_unique($new_aplicacion);
            sort($new_aplicacion);

            if (!$this->arrayCompare($old_aplicacion, $new_aplicacion)) {
                $this->updateProductoAplicacionTable($idProdApp, $idProducto, $new_aplicacion);
                $this->updateAplicacionProductoTable($idProducto, $old_aplicacion, $new_aplicacion);
            }
        }
    }

    public function updateProductoAplicacionTable($idProdApp, $idProducto, $arrMoto){
        $params = [
            'idProducto' => $idProducto,
            'arrMoto'    => json_encode($arrMoto)
        ];
        $this->ProductoAplicacion_model->update_producto_aplicacion($idProdApp, $params);
    }

    public function updateAplicacionProductoTable($idProducto, $old_aplicacion, $new_aplicacion){
        echo '<pre>';
        $removeApp = array_diff($old_aplicacion, $new_aplicacion);
        $addApp    = array_diff($new_aplicacion, $old_aplicacion);
        
        if (count($removeApp) > 0) {
            foreach ($removeApp as $key => $oldMoto) {
               $strProdOld = $this->ProductoAplicacion_model->get_prodApp_by_idMoto($oldMoto);
                echo 'quitar in <br>';
                var_dump($strProdOld);
               if ($strProdOld) {
                   $arrProdOld = json_decode($strProdOld['arrProd']);
                    echo 'json decode arrProdOld <br>';
                    var_dump($arrProdOld);
                   $arrProdNew = $this->quitar_item($idProducto, $arrProdOld);
                   $paramsForRemove = [
                        'idMoto'  => $oldMoto, 
                        'arrProd' => json_encode($arrProdNew)
                   ];
                    echo 'quitar out <br>';
                    var_dump($paramsForRemov);
                   $this->ProductoAplicacion_model->update_aplicacion_producto($strProdOld['idAppProd'],$paramsForRemove);
               }
            }
            echo '<br>Entro en quitar<br>';
        }

        if (count($addApp) > 0) {
            foreach ($new_aplicacion as $key => $newVsOld) {
                $strProdOld = $this->ProductoAplicacion_model->get_prodApp_by_idMoto($newVsOld);
                var_dump($strProdOld);
                if (!is_array($strProdOld)) {
                    $strProdOld['arrProd'] = [];
                    echo '<br>query vacio ----------';
                }else{
                    if (!is_array($strProdOld['arrProd']) || $strProdOld['arrProd'] == null  || $strProdOld['arrProd'] == false  || $strProdOld['arrProd'] == '') {
                        $strProdOld['arrProd'] = [];
                        echo '<br>no hay data ----------';
                    }else{
                        echo '<br>bien!, hay data ----------';
                    }
                }
                $strProdOld['arrProd'] = json_encode($strProdOld['arrProd']);

                var_dump($strProdOld);
                echo '<hr>';
                

               //  echo 'from model------';
               //  var_dump($strProdOld);
               // if ($strProdOld) {
               //      $arrProdOld = json_decode($strProdOld['arrProd']);
               //      if (!is_array($arrProdOld || $arrProdOld == 'null')) {
               //          $arrProdOld = [];
               //      }
               //      array_push($arrProdOld, $idProducto);
               //      $paramsForAdd = [
               //          'idMoto'  => $newVsOld, 
               //          'arrProd' => $arrProdNew
               //      ];
               //      echo 'params------';
               //      var_dump($paramsForAdd);
               //     $this->ProductoAplicacion_model->update_aplicacion_producto($strProdOld['idAppProd'],$paramsForAdd);
               // }else{
               //      $paramsNew = [
               //          'idMoto'  => $idMoto, 
               //          'arrProd' => array($idProducto)
               //      ];
               //      $paramsNew['arrProd'] = json_encode($paramsNew['arrProd']);
               //      $this->ProductoAplicacion_model->add_aplicacion_producto($paramsNew);
               // }
            }
            echo '<br>Entro en agregar<br>';
        }
        echo $this->db->last_query().'<br>';
        var_dump($old_aplicacion);
        var_dump($new_aplicacion);
        var_dump($removeApp);
        var_dump($addApp);
    }

    public function arrayCompare($a, $b){
        $equal = false;
        $d = array_diff($a, $b);
        $e = array_diff($b, $a);

        if (count($d) == 0 && count($e) == 0) {
            $equal = true;
        }
        return $equal;
    }

    /*
     * Deleting producto
     */
    function remove($idProducto)
    {
        $producto = $this->Producto_model->get_producto($idProducto);

        // check if the producto exists before trying to delete it
        if(isset($producto['idProducto']))
        {
            $this->Producto_model->delete_producto($idProducto);
            redirect('producto/index');
        }
        else
            show_error('The producto you are trying to delete does not exist.');
    }

    function uploadsImages($files, $idFamilia){
        $extensiones_permitidas = ["png", "bmp", "jpg", "jpeg", "gif"];

        $uploadsImages['img_url_arr'] = [];
        $uploadsImages['error'] = [];

        foreach ($files['imagen']['name'] as $index => $name) {
            $imgName     = $files['imagen']['name'][$index];
            $imgTempPath = $files['imagen']['tmp_name'][$index]; 
            $imgNameArr  = explode('.', $imgName);
            $last        = count($imgNameArr);
            $ext         = $imgNameArr[$last -1];

            if (in_array($ext, $extensiones_permitidas)) {

                if ($_FILES['imagen']['error'][$index] != 1) {

                    // $idFamilia = $this->input->post('idFamilia');
                    if ($idFamilia != '' && $idFamilia != 0) {

                        $this->load->model('Familium_model');
                        $familiaRow  = $this->Familium_model->get_familium($idFamilia);
                        $familiaSlug = $familiaRow['slug'];
                        $path        = FCPATH.'resources/img/uploads/'.$familiaSlug.'/';

                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                            chmod($path, 0777);
                        }

                        $destino = $path.$this->input->post('slug').$index.'.'.$ext;
                        $img_url = 'resources/img/uploads/'.$familiaSlug.'/'.$this->input->post('slug').$index.'.'.$ext;
                        copy($imgTempPath, $destino);

                        array_push($uploadsImages['img_url_arr'], $img_url);
                    }else{
                        // error se debe seleccionar una familia
                        $error = [
                            'index' => $index, 
                            'mesage' => 'Error, se debe seleccionar una familia para gusrdado de las imagenes '.$index
                        ];
                        array_push($uploadsImages['error'], $error);
                    }
                }else{
                    // error en upload de al menos un archivo
                    $error = [
                        'index' => $index, 
                        'mesage' => 'Error al subir el archivo  '.$index
                    ];
                    array_push($uploadsImages['error'], $error);
                }
            }else{
                // extensiones no permitidas
                $error = [
                    'index' => $index, 
                    'mesage' => 'Error de extension '.$ext
                ];
                array_push($uploadsImages['error'], $error);
            }
        }
        return $uploadsImages;
    }

    public function getProductCats($idProducto = '', $print = false){{
        if ($idProducto == '') {
            $idProducto = $this->uri->segment(3);
        }

        if ($idProducto != '') {

            $this->load->model('ProductoCategoria_model');
            $out = $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
            
            if ($print) {
                $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($out));
            }else{
                return $out;
            }
        }
    }}

    public function quitar_item($valor ,$arr){
        if (is_array($arr)) {
            foreach (array_keys($arr, $valor) as $key) {
                unset($arr[$key]);
            }
        }
        return $arr;
    }
    
}
