<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Modelo Moto Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('modelo_moto/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>IdModeloMoto</th>
						<th>IdMarcaMoto</th>
						<th>Nombre</th>
						<th>Descripcion</th>
						<th>Year</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($modelo_moto as $m){ ?>
                    <tr>
						<td><?php echo $m['idModeloMoto']; ?></td>
						<td><?php echo $m['idMarcaMoto']; ?></td>
						<td><?php echo $m['nombre']; ?></td>
						<td><?php echo $m['descripcion']; ?></td>
						<td><?php echo $m['year']; ?></td>
						<td>
                            <a href="<?php echo site_url('modelo_moto/edit/'.$m['idModeloMoto']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('modelo_moto/remove/'.$m['idModeloMoto']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
