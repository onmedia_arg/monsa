<style>
	.thumbs{
		text-align: center;
	}
	.list-thumb{
		width: auto;
		height: auto;
		max-height: 50px;
		max-width: 50px;
		border: solid 1px white;
		border-radius: 3px;
	}
	.box-body{
		overflow-x: scroll;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box list">
            <div class="box-header">
                <h3 class="box-title">Detalle de Pedidos</h3>
            	<div class="box-tools">
                    <!-- <a href="" class="btn btn-success btn-sm">Agregar</a>  -->
                </div>
            </div>
            <div class="box-body">
				<table class="table" id="table-order-detail">
					<thead>
						<tr>
							<th>idOrderHdr</th>
							<th>posnr</th>
							<th>idProducto</th>
							<th>precio</th>
							<th>qty</th>
							<th>total</th>
							<th>options</th>
							<th>idItemStatus</th>
							<th>createdBy</th>
							<th>createdDate</th>
							<th>updatedBy</th>
							<th>updatedDate</th>
			            </tr>
					</thead>
				</table>                
                                
            </div>
        </div>
    </div>
</div>


<script>
	var table = null;

    $(function () {
        
        fn_build_table();

    });	

	function fn_build_table(){

		table = $('#table-order-detail').DataTable({
			"serverSide": true,
			"processing": true,
			"responsive": true,
			"ajax":{
				url :"<?php echo base_url() . 'Order/getOrderDetail/' . $order ?>", // json datasource
				type: "post", 
			},
	        "columnDefs":[
	                    {
	                    "targets": [ 4 ],
	                    render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' )
	                     },
	            ],			
		})

	};

</script>


<?php 

echo 'detail';

var_dump($order);

?>