<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado de Usuarios</h3>
            	<div class="box-tools">
            		<?php if ($user['level'] == '9'): ?>
                    	<a href="<?php echo site_url('user/add'); ?>" class="btn btn-success btn-sm">Add</a> 
					<?php endif ?>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>User Id</th>
						<th>Username</th>
						<th>Email</th>
						<th>Auth Level</th>
						<th>Banned</th>
						<th>Created At</th>
						<th>Modified At</th>
						<?php if ($user['level'] == '9'): ?>
							<th>Acciones</th>
						<?php endif ?>
                    </tr>
                    <?php foreach($users as $u){ ?>
                    <tr>
						<td><?php echo $u['user_id']; ?></td>
						<td><?php echo $u['username']; ?></td>
						<td><?php echo $u['email']; ?></td>
						<td><?php echo $u['auth_level']; ?></td>
						<td><?php echo $u['banned']; ?></td>
						<td><?php echo $u['created_at']; ?></td>
						<td><?php echo $u['modified_at']; ?></td>
						<?php if ($user['level'] == 9): ?>
							<td>
	                            <a href="<?php echo site_url('user/edit/'.$u['user_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Editar</a> 
	                            <a href="<?php echo site_url('user/remove/'.$u['user_id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Borrar</a>
	                        </td>
						<?php endif ?>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
