	<body>
		<div class="container">
			<div class="row text-center">
				<form class="form-signin" action="<?php echo base_url('start/login')?>" method="post" accept-charset="utf-8">
					<img class="mb-4" src="<?php echo site_url('resources/img/monsa-srl-logo.png');?>" alt="" width="300" >
					
					<label for="login_string" class="sr-only" >Usuario</label>
					<input type="text" name="login_string" id="login_string" class="form-control mb-1" placeholder="Usuario" required autofocus>
					
					<label for="login_pass" class="sr-only">Password</label>
					<input type="password" id="login_pass" name="login_pass" class="form-control" placeholder="Password" 
					<?php 
						if( config_item('max_chars_for_password') > 0 )
					    	echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
					?>
					required>
					
					<button class="btn btn-lg btn-warning btn-block mb-1" name="submit"  type="submit" id="submit_button"  >Ingresar</button>
					<p class="mt-5 mb-3 text-muted">&copy; 2019 - Powered by onMedia</p>
				</form>
			</div>
		</div>
	</body>


<?php 
	// $link_protocol = USE_SSL ? 'https' : NULL;
	// echo site_url('examples/recover', $link_protocol);
	// echo base_url('start/registerUser'); 
?>
