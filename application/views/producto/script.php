<script>

	function limpiarSlug(slug){
		slug = slug.toLowerCase();
		slug = slug.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
		slug = slug.replace(/ /g,"-");
		slug = slug.replace((/[^A-Za-z0-9]+/g),"_");
		return slug;
	}

	function formOnSubmit(){
		var valid = validForm();
		if (valid.success) {
			return true;
		}else{
			showErrors(valid.error);
			return false;
		}
	}

	function showErrors(arr){
		$('#form-error').parent().slideUp();
		$('#form-error').html('');
		arr.forEach((item, index) => {
			console.log(item);
			var li = $('<li>', {
				class : 'list-group-item', 
				html : item
			})
			$('#form-error').append(li);
		})
		$('#form-error').parent().slideDown();
	}

	function validForm(){
		var success = {
			error : [], 
			success : true
		};
		
		if ($('#nombre').val() == '') {
			success.error.push('Nombre: Escribe un nombre'); 
			success.success = false; 
		}

		if ($('#slug').val() == '') {
			success.error.push('Slug: Slug no valido'); 
			success.success = false; 
		}

		if ($('#idFamilia').val() == 0) {
			success.error.push('Familia: Selecciona una Familia'); 
			success.success = false; 
		}

		// if ($('#cats').val() == 0 || !$('#cats').val()) {
		// 	success.error.push('Categorías: Selecciona al menos una categoría'); 
		// 	success.success = false; 
		// }

		if ($('#marcaProducto').val() == 0) {
			success.error.push('Marca: selecciona una marca de producto'); 
			success.success = false; 
		}

		if ($('#modeloProducto').val() == 0) {
			success.error.push('Modelo: selecciona un modelo de producto'); 
			success.success = false; 
		}

		// if ($('#descripcion').val() == 0) {
		// 	success.error.push('Descripción: escribe algo que sea caracteristico del producto'); 
		// 	success.success = false; 
		// }

		if ($('#visibilidad').val() == 0) {
			success.error.push('Visibilidad: hubo un error no esperado, recarga la página'); 
			success.success = false; 
		}

		if ($('#sku').val() == 0) {
			success.error.push('SKU: escribe un SKU'); 
			success.success = false; 
		}

		if ($('#precio').val() == 0) {
			success.error.push('Precio: debes ponerle un precio al producto'); 
			success.success = false; 
		}

		// if ($('#peso').val() == 0) {
		// 	success.error.push('Peso: debes fijar un peso al producto'); 
		// 	success.success = false; 
		// }

		if ($('#stock').val() == 0) {
			success.error.push('Stock: hubo un error no esperado, recarga la página'); 
			success.success = false; 
		}

		if ($('#alto').val() == 0) {
			success.error.push('Alto: Debes seleccionar una dimensión valida'); 
			success.success = false; 
		}

		if ($('#largo').val() == 0) {
			success.error.push('Largo: Debes seleccionar una dimensión valida'); 
			success.success = false; 
		}

		if ($('#ancho').val() == 0) {
			success.error.push('Ancho: Debes seleccionar una dimensión valida'); 
			success.success = false; 
		}

		if ($('#atributoFamilia').val() == 0) {
			success.error.push('Familia: Debes seleccionar una familia para asociarla a los atributos'); 
			success.success = false; 
		}

		return success;
	}

	function validNameProduct(){
		var familia_val = $('#idFamilia').val();
		var marca_val = $('#marcaProducto').val();
		var modelo = $('#modeloProducto').val();
		var marca = '';
		var familia = '';
		var str = {};
		if ((familia_val != 0 && familia_val) && (marca_val != 0 && marca_val)) {
			marca = $('#marcaProducto option:selected').text();
			familia = $('#idFamilia option:selected').text();
		}
		
		if (modelo != '' && marca != '' && familia != '') {
			str.str  = familia + ' ' + marca + ' ' + modelo;
			str.slug = limpiarSlug(familia) + '_' + limpiarSlug(marca) + '_' + limpiarSlug(modelo);
		}
		console.log(str);
		return str;
	}

	function exeValidName(){
		var name = validNameProduct();
		if (name.slug !='') {
			$('#nombre').val(name.str);
			$('#slug').val(name.slug);
			$('#sku').val(name.slug);
			$('#nombre-label').text(name.str);
		}
	}

	function string_to_slug (str) {
	    str = str.replace(/^\s+|\s+$/g, ''); // trim
	    str = str.toLowerCase();
	  
	    // remove accents, swap ñ for n, etc
	    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
	    var to   = "aaaaeeeeiiiioooouuuunc------";
	    for (var i=0, l=from.length ; i<l ; i++) {
	        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	    }

	    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
	        .replace(/\s+/g, '-') // collapse whitespace and replace by -
	        .replace(/-+/g, '-'); // collapse dashes

	    return str;
	}

	function create_slug(){
		var inputName     = $('#nombre');
		var familiaSelect = $('#idFamilia').val();
		var familia       = $('#idFamilia option:selected').text();
		var ProdSelect    = $('#marcaProducto').val();
		var marcaProducto = $('#marcaProducto option:selected').text();
		var modelo        = $('#modeloProducto');

		console.log('\n familia' , familia);
		console.log('\n marcaProducto' , marcaProducto);
		console.log('\n modelo' , familia);
	}

	function filePreview(input) {
		// TODO: validar extensiones
		// TODO: validar tamaños
	    $('#images-preview img').remove();
	    if (input.files && input.files.length > 0) {
	    	for (var i = 0; i < input.files.length; i++) {
				var reader = new FileReader();
		        reader.onload = function (e) {
		            $('#images-preview').append('<div class="img-wrapper"><img src="'+e.target.result+'" width="450" height="300"/><button type="button" class="btn btn-danger btn-xs" data-id ="%s" onclick="borrar(this)"><i class="fa fa-remove"></i></button></div>');
		        }
		        reader.readAsDataURL(input.files[i]);
	    	}
	    }
	}

	function fillSelect (attr, target, url){
        $.get( url)
         .done( data => {
            target.html('');
            var option = $('<option>',  {
                value : 0, 
                text  : 'Seleccione ' + attr 
            });
            target.append(option);
            for (var i = 0; i < data[attr].length; i++) {
                option = $('<option>',  {
                    value : data[attr][i].idFamilia, 
                    text  : data[attr][i].nombre 
                });
                target.append(option);
                target.select2({placeholder: 'Seleccione ...', allowClear: true});
            }
        }).error( err => {
            let option = $('<option>',  {
                value : 0, 
                text  : 'Error al cargar ' + attr
            });
            target.append(option);
            target.select2({placeholder: 'Error al cargar',allowClear: true});
        })
    }

    var getAtributesByFamily = (e) => { 
			// $('#attr-wrapper').html('');
			getAtributesForAllFamily();
			var attr = $('#atributoFamilia').val();
			if (attr) {
				var ul = $('<ul>', {"class" : "family-list-ajax"});
				for (var i = 0; i < attr.length; i++) {
					$.get('<?php echo base_url() ?>/atributo/get_attr_by_family/' + attr[i])
					.done(data => {
						console.log(data);
						if (data.atributo[0]) {
							for (var j = 0; j < data.atributo.length; j++) {
								if (data.atributo[j].valor) {
									var li = $('<li>');
									var label = $('<label>', {
										class : 'control-label', 
										text  : data.atributo[j].familia.toUpperCase() + '/' + data.atributo[j].nombre.toUpperCase()
									})
									var form_group = $('<div>', {
										class : 'form-group'
									});
									var select = $('<select>', {
										name : 'atributo['+ data.atributo[j].familiaSlug + '][' + data.atributo[j].idAtributo + '][]', 
										class: 'form-control attr-select', 
										multiple : 'multiple'
									})
									for (var i = 0; i < data.atributo[j].valor.length; i++) {
										let option = $('<option>',  {
						                    value : data.atributo[j].valor[i], 
						                    text  : data.atributo[j].valor[i] 
						                });
						                select.append(option);
									}
									form_group.append(select);
									li.append(label);
									li.append(form_group);
									ul.append(li);
								}
							}
							$('.attr-select').select2(select2Params);
						}else{
							$('#attr-error').slideDown(()=>{
								$('#attr-wrapper').fadeOut(()=>{
									$('#attr-wrapper').fadeIn().html('');
								});
								$('#atributoFamilia').val(null).trigger('change')
								$('#attr-error').text('No hay aun atributos para esta familia').fadeOut(2000);
							})
						}
					})
					.error(err => {
						console.log(err);
					});
				}
				$('#attr-wrapper').append(ul);
			}else{
				console.log('cleared');
			}
		}

		var getAtributesForAllFamily = (e) => { 
			$('#attr-wrapper').html('');
			var attr = 'todas';
			var ul = $('<ul>', {"class" : "family-list-ajax"});
			$.get('<?php echo base_url() ?>atributo/get_attr_for_family_all/' + attr)
			.done(data => {
				if (data.atributo[0]) {
					for (var j = 0; j < data.atributo.length; j++) {
						if (data.atributo[j].valor) {
							var li = $('<li>');
							var label = $('<label>', {
								class : 'control-label', 
								text  : data.atributo[j].familia.toUpperCase() + '/' + data.atributo[j].nombre.toUpperCase()
							})
							var form_group = $('<div>', {
								class : 'form-group'
							});
							var select = $('<select>', {
								name : 'atributo['+ data.atributo[j].familiaSlug + '][' + data.atributo[j].idAtributo + '][]', 
								class: 'form-control attr-select', 
								multiple : 'multiple'
							})
							for (var i = 0; i < data.atributo[j].valor.length; i++) {
								let option = $('<option>',  {
				                    value : data.atributo[j].valor[i], 
				                    text  : data.atributo[j].valor[i] 
				                });
				                select.append(option);
							}
							form_group.append(select);
							li.append(label);
							li.append(form_group);
							ul.append(li);
						}
					}
					$('.attr-select').select2(select2Params);
				}else{
					$('#attr-error').slideDown(()=>{
						$('#attr-wrapper').fadeOut(()=>{
							$('#attr-wrapper').fadeIn().html('');
						});
						$('#atributoFamilia').val(null).trigger('change')
						$('#attr-error').text('No hay aun atributos para esta familia').fadeOut(2000);
					})
				}
			})
			.error(err => {
				console.log(err);
			});
			$('#attr-wrapper').append(ul);
			
		}

	function borrar(e){
		console.log('borrando', e);
		var li = e.parentNode;
		var ul = li.parentNode.removeChild(li);
		
	}

	function setAtributeFamily(){
		var familiaSelected = $('#idFamilia').val();
		$('#atributoFamilia').val(familiaSelected).trigger('change');
	}

	var base_url = '<?php echo base_url() ?>';
	var select2Params = {placeholder: 'Seleccione ...', allowClear: true};

</script>