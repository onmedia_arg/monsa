<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Order Itemmetum Add</h3>
            </div>
            <?php echo form_open('order_itemmetum/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="idOrderItem" class="control-label">IdOrderItem</label>
						<div class="form-group">
							<input type="text" name="idOrderItem" value="<?php echo $this->input->post('idOrderItem'); ?>" class="form-control" id="idOrderItem" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="metaKey" class="control-label">MetaKey</label>
						<div class="form-group">
							<input type="text" name="metaKey" value="<?php echo $this->input->post('metaKey'); ?>" class="form-control" id="metaKey" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="metaValue" class="control-label">MetaValue</label>
						<div class="form-group">
							<textarea name="metaValue" class="form-control" id="metaValue"><?php echo $this->input->post('metaValue'); ?></textarea>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>