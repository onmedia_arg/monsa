<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Mostrar Marcas</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('marca/add'); ?>" class="btn btn-success btn-sm">Nueva Marca</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdMarca</th>
    						<th>Nombre</th>
    						<th>Slug</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($marca as $m){ ?>
                        <tr>
    						<td><?php echo $m['idMarca']; ?></td>
    						<td><?php echo $m['nombre']; ?></td>
    						<td><?php echo $m['slug']; ?></td>
    						<td>
                                <a href="<?php echo site_url('marca/edit/'.$m['idMarca']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                <a href="<?php echo site_url('marca/remove/'.$m['idMarca']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>
