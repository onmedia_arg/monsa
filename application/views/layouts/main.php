<?php 
    $avatar     = 'resources/img/ninja.jpg';
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Monsa</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/font-awesome.min.css');?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Datetimepicker -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap-datetimepicker.min.css');?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/AdminLTE.min.css');?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/_all-skins.min.css');?>">
        <!-- DataTable -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">       


         <!-- jQuery 2.2.3 -->
        <script src="<?php echo site_url('resources/js/jquery-2.2.3.min.js');?>"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

        <!-- Knockout -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.5.0/knockout-min.js" integrity="sha256-Tjl7WVgF1hgGMgUKZZfzmxOrtoSf8qltZ9wMujjGNQk=" crossorigin="anonymous"></script>
        <!--  -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" integrity="sha256-LOnFraxKlOhESwdU/dX+K0GArwymUDups0czPWLEg4E=" crossorigin="anonymous"></script>
        <!-- BlockUI -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" integrity="sha256-9wRM03dUw6ABCs+AU69WbK33oktrlXamEXMvxUaF+KU=" crossorigin="anonymous"></script>
        <!-- iziToast -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" integrity="sha256-f6fW47QDm1m01HIep+UjpCpNwLVkBYKd+fhpb4VQ+gE=" crossorigin="anonymous" /> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha256-321PxS+POvbvWcIVoRZeRmf32q7fTFQJ21bXwTNWREY=" crossorigin="anonymous"></script>   
        
        <!-- <script src="//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"></script> -->
        <style>
            .navbar-nav > .user-menu > .dropdown-menu{
                min-width: 320px !important;
            }
            /*Alerts fix*/
            .callout i {
                margin-right: 15px;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini" data-url="<?php echo base_url(); ?>">
        <div class="wrapper" id="wrapper-app" data-url="<?php echo base_url() ?>">
            <header class="main-header">
                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">Monsa</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">Monsa</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo site_url($avatar);?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $userstring; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo site_url($avatar);?>" class="img-circle" alt="User Image">
                                        <p><?php echo $userstring; ?> </p>
                                        <small><?php echo $role; ?></small>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo base_url(); ?>" class="btn btn-info btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <!-- <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                                            <?php
                                                $login = $this->session->userdata('logged_in');
                                                echo (isset( $login )  && $login == true )
                                                    ? logout_anchor('start/logout', 'Logout', 'class="btn btn-warning btn-flat"')
                                                    : login_anchor('start', 'Login', 'id="login-link" class="btn btn-info btn-flat"' );
                                            ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?php echo site_url('producto/index');?>">
                                <i class="fa fa-list-ul"></i> Productos</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('order/index');?>">
                                <i class="fa fa-shopping-cart"></i> Pedidos</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Datos Maestros</span>
                            </a>                            
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo site_url('familium/index');?>"><i class="fa fa-list-ul"></i> Familias</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('categorium/index');?>"><i class="fa fa-list-ul"></i> Categorías</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('atributo/index');?>"><i class="fa fa-list-ul"></i> Atributos</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('marca/index');?>"><i class="fa fa-list-ul"></i> Marcas</a>
                                </li>
                            </ul>   
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Configuración</span>
                            </a>                            
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo site_url('Sys_Config/index');?>"><i class="fa fa-circle-o"></i> Ajustes Generales</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('user/index');?>"><i class="fa fa-circle-o"></i> Usuarios</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo site_url('FrontController/shop')?>">
                                <i class="fa fa-th-large"></i><span>Ver la Tienda</span>
                            </a>
                        </li>
<!--                         <li>
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Front</span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo site_url();?>">
                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('frontController/inicio');?>"><i class="fa fa-plus"></i> Catalogo</a>
                                </li>
                            </ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Atributo</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('atributo/add');?>"><i class="fa fa-plus"></i> Agregar</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('atributo/index');?>"><i class="fa fa-list-ul"></i> Atributos</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Categoría</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('categorium/add');?>"><i class="fa fa-plus"></i> Agregar</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('categorium/index');?>"><i class="fa fa-list-ul"></i> Categorías</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Familia</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('familium/add');?>"><i class="fa fa-plus"></i> Agregar</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('familium/index');?>"><i class="fa fa-list-ul"></i> Familias</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-car"></i> <span>Marca</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('marca/add');?>"><i class="fa fa-plus"></i> Agregar</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('marca/index');?>"><i class="fa fa-list-ul"></i> Marcas</a>
                                </li>
							</ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-car"></i> <span>Modelo</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('modelo/add');?>"><i class="fa fa-plus"></i> Agregar</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('modelo/index');?>"><i class="fa fa-list-ul"></i> Modelos ??</a>
                                </li>
                            </ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Producto</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('producto/add');?>"><i class="fa fa-plus"></i> Agregar</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('producto/index');?>"><i class="fa fa-list-ul"></i> Productos</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Usuario</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('user/add');?>"><i class="fa fa-plus"></i> Agregar</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('user/index');?>"><i class="fa fa-list-ul"></i> Usuarios</a>
                                </li>
							</ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Marca Moto</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('marca_moto/add');?>"><i class="fa fa-plus"></i> Add</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('marca_moto/index');?>"><i class="fa fa-list-ul"></i> Motos - Marcas</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-heart"></i> <span>Moto / Modelos</span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="active">
                                    <a href="<?php echo site_url('moto/add');?>"><i class="fa fa-plus"></i> Add Moto</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('moto/index');?>"><i class="fa fa-list-ul"></i> Motos</a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo site_url('modelo_moto/add');?>"><i class="fa fa-plus"></i> Add Model Moto</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('modelo_moto/index');?>"><i class="fa fa-list-ul"></i> Motos - Modelos</a>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <?php                    
                    if(isset($_view) && $_view)
                        $this->load->view($_view);
                    ?>                    
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>onMedia</strong>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo site_url('resources/js/bootstrap.min.js');?>"></script>
        <!-- FastClick -->
        <script src="<?php echo site_url('resources/js/fastclick.js');?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo site_url('resources/js/app.min.js');?>"></script>
        <!-- AdminLTE for demo purposes -->
        <!-- <script src="<?php echo site_url('resources/js/demo.js');?>"></script> -->
        <!-- DatePicker -->
        <!-- <script src="<?php echo site_url('resources/js/moment.js');?>"></script>
        <script src="<?php echo site_url('resources/js/bootstrap-datetimepicker.min.js');?>"></script>
        <script src="<?php echo site_url('resources/js/global.js');?>"></script> -->
        <!-- <script src="<?php echo site_url('resources/js/monsa.js');?>"></script> -->
    </body>
</html>
