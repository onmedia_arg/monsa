<?php 

   $ci =&get_instance();
   $ci->load->model('Base_model');  

 ?>
            <div class="container-fluid">
 
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <section class="content">

                        <?php if ( $this->session->flashdata('error_message') !== null ): ?>
                            <div class="alert bg-danger text-white"><i class="fa fa-times mr-2"></i><?= $this->session->flashdata('error_message') ?></div>
                        <?php endif; ?>

                        <?php if ( $this->session->flashdata('alert_message') !== null ): ?>
                            <div class="alert bg-warning text-white"><i class="fa fa-exclamation mr-2"></i><?= $this->session->flashdata('alert_message') ?></div>
                        <?php endif; ?>

                        <?php if ( $this->session->flashdata('success_message') !== null ): ?>
                            <div class="alert bg-success text-white"><i class="fa fa-check mr-2"></i><?= $this->session->flashdata('success_message') ?></div>
                        <?php endif; ?>

                              <!-- Default box -->
                              <div class="box">
                                <div class="box-header with-border">
                                  <h3 class="box-title">Ajustes Generales</h3> 
                                </div>
                                <div class="box-body">
                                    <?php echo form_open('Sys_Config/save_sys_config', [ 'class' => "form-horizontal",  'role' => "form", 'data-toggle' => "validator" ] ); ?>   

                                        <div class="form-group">
                                            <label class="col-md-12">URL del Sistema</label>
                                            <div class="col-md-6">
                                                <input readonly="" type="text" name="sys-url" class="form-control" value="<?= base_url() ?>">
                                                  
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-12">Nombre del Sistema</label>
                                            <div class="col-md-6">
                                                <input type="text" name="sys-title" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_title'); ?>">
                                                  
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-12">Logo del Sistema</label>
                                            <div class="col-md-6">
                                                <input type="text" name="sys-logo" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_logo'); ?>">
                                                  
                                            </div>
                                        </div>

        <!--                                 <div class="form-group">
                                            <label class="col-md-12">Avatar por defecto</label>
                                            <div class="col-md-6">
                                                <input type="text" name="default-avatar" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_default_avatar'); ?>">
                                                  
                                            </div>
                                        </div> -->

        <!--                                 <div class="form-group">
                                            <label class="col-md-12">Favicon</label>
                                            <div class="col-md-6">
                                                <input type="text" name="sys-favicon" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_favicon'); ?>">
                                                  
                                            </div>
                                        </div> -->

                                        <div class="form-group">
                                            <label class="col-md-12">Logo del Correo</label>
                                            <div class="col-md-6">
                                                <input type="text" name="sys-mail-logo" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_mail_logo'); ?>">
                                                <span class="help-block">Logo de cabecera en notificaciones por correo</span>  
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-12">Correo Remitente</label>
                                            <div class="col-md-6">
                                                <input type="text" name="mail-sender" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_mailsender'); ?>">
                                                <span class="help-block">Correo que envia por defecto las alertas del sistema</span>  
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-12">Correo del Administrador</label>
                                            <div class="col-md-6">
                                                <input type="text" name="admin-mail" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('admin_mail'); ?>"> 
                                                <span class="help-block">Receptores de correos de notificacion de Pedidos <b>(Si es mas de un correo, separelos con una coma ",")</b></span> 
                                            </div>
                                        </div>

                        <hr>

                        <h3 class="box-title m-b-0">Configuracion del Correo</h3>
                        <br> 

                            <div class="form-group">
                                <label class="col-md-12">Host</label>
                                <div class="col-md-6">
                                    <input type="text" name="smtp_host" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_host'); ?>">
                                    <span class="help-block">Host del Servidor de correo </span> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Puerto</label>
                                <div class="col-md-6">
                                    <input type="text" name="smtp_port" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_port'); ?>">
                                    <span class="help-block">Puerto para la conexion </span> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Usuario</label>
                                <div class="col-md-6">
                                    <input type="text" name="smtp_user" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_user'); ?>">
                                    <span class="help-block">Correo remitente </span> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Contraseña</label>
                                <div class="col-md-6">
                                    <input type="text" name="smtp_pass" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_pass'); ?>">
                                    <span class="help-block">Contraseña del correo remitente </span> 
                                </div>
                            </div>

                            <button type="submit" name="save-config" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button>

                        <?php echo form_close(); ?>
                                </div>
                                <!-- /.box-body --> 
                              </div>
                              <!-- /.box -->

                            </section> 