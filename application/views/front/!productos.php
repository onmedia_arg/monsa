<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';

    $this->load->view('front/layouts/header',$nav);
    $this->load->view('front/layouts/seccion',$title);
    $this->load->view('front/productos/filtros');
    $this->load->view('front/productos/grilla');
    $this->load->view('front/productos/small', $featured_prod);
    $this->load->view('front/layouts/footer');

?>