<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';

    $this->load->view('front/layouts/header',$nav);
    $this->load->view('front/layouts/seccion',$title);
?>
<!--================Cart Area =================-->
    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Producto</th>
                                <th scope="col">Familia</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Total</th>
                                <th scope="col">Remover</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php   if (isset($cartitems)) {

       
foreach ($cartitems as $cartitem) {
    if(!empty($cartitem['product'][0])){
                   ?>
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="d-flex">
                                            <?php  
                                $quitar                  = ['\\'];
                                $remplazar               = '';
                                $imagenes = str_replace($quitar, $remplazar, $cartitem['product'][0]['imagen']);
                                $imagenes = json_decode($imagenes);
                                $max_images = 1;
                                if($imagenes != NULL){
                                    if (count($imagenes) > 0) {
                                        for ($i=0; $i < count($imagenes) ; $i++) {
                                            if ($i < $max_images) {
                                                printf('<img src="%s" alt="%s" class="img-fluid">',
                                                    base_url($imagenes[$i]), 
                                                    $cartitem['product'][0]['slug']
                                                );
                                            }
                                        }
                                    }
                                }
                                else
                                    echo '<img src="'.site_url('').'resources/img/dumy/default.jpg" alt="trabas_antirrobo_kryptonite_ke" class="img-fluid">';
                                     ?>
                                        </div>
                                        <div class="media-body">
                                            <p><?php echo $cartitem['product'][0]['nombre'] ?></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5><?php echo $cartitem['product'][0]['familiaSlug'] ?></h5>
                                </td>
                                <td>
                                    <h5><?php echo $cartitem['product'][0]['marca'] ?></h5>
                                </td>
                                <td>
                                    <h5><?php echo $cartitem['product'][0]['nombre'] ?></h5>
                                </td>                                                                
                                <td>
                                    <div class="product_count">
                                        <input type="text" name="qty" id="sst" maxlength="4" value="<?php echo $cartitem['quantity'] ?>" title="Quantity:" class="input-text qty">
                                    </div>
                                </td>
                                <td>
                                    <h5><?php echo $cartitem['product'][0]['precio']* $cartitem['quantity']; ?></h5>
                                </td>
                                <td>
                                    <a href="<?php echo site_url('/Cart/remove_from_cart/'.$cartitem['product'][0]['idProducto'].'/1'); ?>" class="remove remove-from-cart" data-id-producto="<?php echo $cartitem['product'][0]['idProducto']; ?>"><i class="fas fa-minus-circle"></i></a>
                                </td>
                            </tr>
                            <?php } }
}
?>	
                            <tr class="bottom_button">
                                <td>
                                    <a class="gray_btn" href="#">Actualizar</a>
                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>

                                <td>

                                </td>
                                <td>
                                  
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <h5>Subtotal</h5>
                                </td>
                                <td>
                                    <h5>$2160.00</h5>
                                </td>
                            </tr>
                            
                            <tr class="out_button_area">
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <div class="checkout_btn_inner d-flex align-items-center">
                                        <a class="gray_btn" href="#">AGREGAR MAS PRODUCTOS</a>
                                        <a class="primary-btn" href="#">ENVIAR PEDIDO</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--================End Cart Area =================-->
<?php
    $this->load->view('front/layouts/footer');
?>