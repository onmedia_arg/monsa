<style>
	.product-list{
		list-style: none;
		padding: 0;
		margin: 0;
		display: flex;
		justify-content: space-between;
		align-items: center;
		flex-wrap: wrap;
	}

	.product-list >li{
		flex: 1;
		max-width: 600px;
		min-width: 300px;
		min-height: 350px;
		margin: 10px;
	}

	.product-list .btn{
		min-width: 40px;
	}

	.product-list .box-body{
		min-height: 250px;
		position: relative;
		overflow: hidden;
	}

	.product-list .box-body .thumb{
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background-size: cover;
		background-repeat: no-repeat;
		background-position: center;
		z-index: 1;

	}

	.product-list .box-body .filter{
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		z-index: 10;
		background: black;
		opacity:0.5;
		-ms-transition:all 1s ease-in-out ;
		-webkit-transition:all 1s ease-in-out;
		-moz-transition:all 1s ease-in-out;
		transition:all 1s ease-in-out;
	}

	.product-list .box-body:hover .filter{
		background: transparent;
		opacity:0;
	}

	.product-list .price,
	.product-list .text-info{
		font-size: 24px;
	}


</style>
<section class="main-section home-view">
	<fieldset class="catalog">
		<legend>Catalogo de Productos</legend>

		<ul class="product-list">
			<?php
				foreach ($products as $key => $product) {
					$imagenes = str_replace('\\', '', $product['imagen']);
					$imagenes = json_decode($imagenes);
					$thumb = (isset($imagenes[0]))?base_url($imagenes[0]): base_url('resources/img/dumy/default.jpg');
					$btn_cart_html =   '<button data-url="'.base_url('cart/add_to_cart/') . $product['idProducto'].'" class="btn btn-info pull-left add-to-cart">
	        								<i class="fa fa-shopping-cart"></i>
	        							</button>';
					$btn_cart = ($this->session->userdata('logged_in')) ? $btn_cart_html : '';
					$precio = ($this->session->userdata('logged_in')) ? $product['precio'] : '';
					printf('<li>
								<div class="box box-info">
            						<div class="box-header with-border">
										<h2>%s</h2>
            						</div>
	            					<div  class="box-body">
	            						<div class="thumb" style="background-image: url(%s)"></div>
	            						<div class="filter"></div>
	        							%s
	        							%s
	        							%s
	        							%s
	        							%s
	        							%s
	        							%s
	            					</div>	
	            					<div  class="box-footer text-center">
	        							<span class="price text-info">%s</span>
	        							%s
	        							<a href="%s" class="btn btn-success pull-right">
	        								<i class="fa fa-plus"></i>
	        							</a>
	            					</div>	
            					</div>
							</li>', 
							$product['nombre'],
							$thumb, 
							$product['idProducto'],
							$product['familia'],
							$product['marca'],
							$product['slug'],
							$product['modelo'],
							$product['descripcion'],
							$product['sku'],
							$precio,
							$btn_cart,
							base_url('frontController/detalle/').$product['slug']
						);
				}
			?>
		</ul>
	</fieldset>
</section>
<?php $this->load->view('front/scripts') ?>