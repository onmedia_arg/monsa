<?php

/* TODO la marca del producto no trae el slug */

foreach ($nav['nav_family'] as $key => $familia) {
    $familias[$key]['id'] = $familia['idFamilia'];
    $familias[$key]['nombre'] = $familia['nombre'];
    $familias[$key]['slug'] = $familia['slug'];
}

$filtro_marcas = array();

foreach ($marcas as $producto) {
    $filtro_marcas[] = $producto['marca'];
}

$filtro_marcas = array_unique($filtro_marcas);

$filtro_familias = array();

foreach ($products as $producto) {
    $filtro_familias[] = $producto['familia'];
}

$filtro_familias = array_unique($filtro_familias);

function slugger ($string){
    return strtolower(str_replace('_',' ',$string));
}

?>    <div class="container section_gap">
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="sidebar-categories">
                    <div class="head">Buscar según marcas</div>
                    <ul class="main-categories">
                        <?php
                          $fam = $fam == NULL ? 'todas' : $fam; foreach ($filtro_marcas as $marca) {
                           echo '<li class="main-nav-list"><a class="nav-link" href="'.site_url("/frontController/productos/".$fam."/".slugger($marca)).'">'.$marca.'</a></li>';
                        } ?>
                    </ul>
                </div>
                <div class="sidebar-categories sidebar-filter mt-50">
                    <div class="top-filter-head">Buscar según categoría</div>
                    <ul class="main-categories">
                        <?php
                          $fam = $fam == NULL ? 'todas' : $fam; foreach ($filtro_familias as $familia) {
                           echo '<li class="main-nav-list"><a class="nav-link" href="'.site_url("/frontController/marcas/".$marca."/".slugger($familia)).'">'.$familia.'</a></li>';
                        } ?>
                    </ul>
                </div>
                <div class="sidebar-filter mt-50">
                    <div class="top-filter-head">Filtros de productos</div>
                    <div class="common-filter">
                        <div class="head">Precio</div>
                        <div class="price-range-area">
                            <div id="price-range"></div>
                            <div class="value-wrapper d-flex">
                                <div class="price">Precio:</div>
                                <span>$</span>
                                <div id="lower-value"></div>
                                <div class="to">a</div>
                                <span>$</span>
                                <div id="upper-value"></div>
                            </div>
                        </div>
                    </div>
                    <?php if($this->session->userdata('preciomax')) { ?>
                            <span class="d-none pricesession priceminsession"><?php echo intval($this->session->userdata('preciomin')); ?></span>
                            <span class="d-none pricesession pricemaxsession"><?php 
                            if(null !== ($this->session->userdata('preciomax'))){
                                echo intval($this->session->userdata('preciomax')); 
                            }
                            else {
                                echo "10000"; 
                            } 
                        } ?></span>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap justify-content-end"><?php echo $total_rows.' productos según el filtro';  ?></div>             
                <div class="filter-bar d-flex flex-wrap align-items-center">
                    <div class="sorting">
                        <select class="filter_ord">
                            <option value="ordasc" <?php if($this->session->userdata('ord')=="asc") echo 'selected="selected"'; ?>>Orden ascendente</option>
                            <option value="orddesc" <?php if($this->session->userdata('ord')=="desc") echo 'selected="selected"'; ?>>Orden descendente</option>
                        </select>
                    </div>
                    <div class="sorting ">
                        <select class="filter_q">
                            <option value="mostrar18" <?php if($this->session->userdata('mostrar')=="18") echo 'selected="selected"'; ?>>Mostrar 18</option>
                            <option value="mostrar9" <?php if($this->session->userdata('mostrar')=="9") echo 'selected="selected"'; ?>>Mostrar 9</option>
                            <option value="mostrar6" <?php if($this->session->userdata('mostrar')=="6") echo 'selected="selected"'; ?>>Mostrar 6</option>
                        </select>
                    </div>
                    <div class="sorting">
                        <a href="<?php echo site_url("/frontController/productoslista/".$fam.'/'.$marca); ?>"><i class="fas fa-list"></i></a>
                        <a href="<?php echo site_url("/frontController/productos/".$fam.'/'.$marca); ?>"><i class="fas fa-th-large"></i></a>
                    </div>
                    <?php echo $pagination;  ?>
                </div>
                <!-- End Filter Bar -->            