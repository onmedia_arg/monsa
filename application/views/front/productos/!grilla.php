                <!-- Start Best Seller -->
                <section class="lattest-product-area pb-40 category-list">
                    <div class="row">
                        <?php  foreach ($products as $producto) {
  
                            ?>
                            <!-- single product -->
                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <a href="<?php echo site_url('frontController/detalle/'). $producto['slug']; ?>"><?php  
                                $quitar                  = ['\\'];
                                $remplazar               = '';
                                $imagenes = str_replace($quitar, $remplazar, $producto['imagen']);
                                $imagenes = json_decode($imagenes);
                                $max_images = 1;
                                if($imagenes != NULL){
                                    if (count($imagenes) > 0) {
                                        for ($i=0; $i < count($imagenes) ; $i++) {
                                            if ($i < $max_images) {
                                                printf('<img src="%s" alt="%s" class="img-fluid">',
                                                    base_url($imagenes[$i]), 
                                                    $producto['slug']
                                                );
                                            }
                                        }
                                    }
                                }
                                else
                                    echo '<img src="'.site_url('').'resources/img/dumy/default.jpg" alt="trabas_antirrobo_kryptonite_ke" class="img-fluid">';
                                     ?></a>
                                    <div class="product-details">
                                        <h6> <a href="<?php echo site_url('frontController/detalle/'). $producto['slug']; ?>"><?php echo $producto['nombre']; ?></a></h6>
                                        <div class="price">
                                            <h6>$<?php echo $producto['precio']; ?></h6>
                                        </div>
                                        <div class="add-bag d-flex align-items-center">
                                            <a href="<?php echo site_url('/Cart/add_to_cart/'.$producto['idProducto'].'/1'); ?>" class="add-to-cart" data-id-producto="<?php echo $producto['idProducto']; ?>" >
                                               <span class="add-btn" ><i class="fas fa-plus"></i> </span>
                                               <span class="text"> Agregar al Pedido</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single product -->
                            <?php 
                        } ?>
                    </div>
                </section>
                <!-- End Best Seller -->
                <?php echo $pagination;  ?>
            </div>
        </div>
    </div>