<?php 

    if($product['dimensiones']!=""){
        $dimen  = json_decode($product['dimensiones']);
        $alto   = $dimen->alto;
        $largo  = $dimen->largo;
        $ancho  = $dimen->ancho;
    }
    else{
        $alto   = 0;
        $largo  = 0;
        $ancho  = 0;
    }
    $images = json_decode($product['imagen']);

    

?>
    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-6">
                    <?php 
                    if (isset($images)&& !empty($images)) {
                    // if ($images!==NULL) {
                       ?>
                        <ul class="product-slider">
                            <?php 
                                for ($i=0; $i < 1; $i++) {
                                    echo '<li><img class="img-fluid" src="'.base_url() .$images[$i].'" alt="'.$title.'" /></li>';
                                }                   
                            ?></ul>
                       <?php
                    } 
                        else
                            echo '<img src="'.site_url('').'resources/img/dumy/default.jpg" alt="img" class="img-fluid" style="width:100%">'; 
                    ?>
                   
                </div>

                <?php if ($product['idTipo'] == 0 ){ ?>    
                <!-- Detalle Producto Simple -->                
                <div class="col-lg-5 offset-lg-1">
                    <div class="s_product_text">
                        <h3><?php echo $product['nombre']; ?></h3>
                        <h2>$<?php echo $product['precio']; ?></h2>
                        <ul class="list">
                            <li><span>Categoría:</span> <?php if(count($product['cats'])){ ?>
                                <?php
                                    printf('
                                                <a href="%s" class="btn btn-info btn-xs">%s</a>
                                             ',
                                            base_url('categoria/buscar/' . $product['cats'][0]['familiaSlug']), 
                                            strtolower($product['cats'][0]['familia'])
                                        );
                                    foreach ($product['cats'] as $key => $cat) {
                                        printf('
                                                <a href="%s" class="btn btn-info btn-xs">%s</a>
                                            ',
                                            base_url('categoria/buscar/'.$product['cats'][0]['familiaSlug'].'/'.$cat['slug']), 
                                            strtolower($cat['categoria'])
                                        );
                                    }
                                }
                                ?>
                            </li>
                            <li><a href="#"><span>Modelo:</span> <?php echo $modelo = $product['modelo']; ?></a></li>
                            <li><a href="#"><span>Stock:</span> <?php echo $product['stock']; ?></a></li>
                        </ul>
                        <p><?php echo $product['descripcion']; ?></p>
                        <div class="product_count">
                            <label for="qty">Cantidad:</label>
                            <input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id="<?php echo $product['idProducto'] ?>">
                        </div>
                        <div class="card_area d-flex align-items-center">
                            <button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" 
                                    data-productname = "<?php echo $product['nombre'] ?>"
                                    data-price       = "<?php echo $product['precio'] ?>"
                                    data-productid   = "<?php echo $product['idProducto'] ?>"> Agregar al Pedido</button>
                        </div>
                    </div>
                </div>
                <!-- Fin Detalle Producto Simple -->
                <?php 
                } elseif ($product['idTipo'] == 1 ) { ?>                
                <!-- Detalle Producto Variable -->                
                <div class="col-lg-5 offset-lg-1">
                    <div class="s_product_text">
                        <h3><?php echo $product['nombre']; ?></h3>
                        <ul class="list">
                            <li><span>Categoría:</span> <?php if(count($product['cats'])){ ?>
                                <?php
                                    printf('<a href="%s" class="btn btn-info btn-xs">%s</a>',
                                            base_url('categoria/buscar/' . $product['cats'][0]['familiaSlug']), 
                                            strtolower($product['cats'][0]['familia'])
                                        );
                                    foreach ($product['cats'] as $key => $cat) {
                                        printf('
                                                <a href="%s" class="btn btn-info btn-xs">%s</a>
                                            ',
                                            base_url('categoria/buscar/'.$product['cats'][0]['familiaSlug'].'/'.$cat['slug']), 
                                            strtolower($cat['categoria'])
                                        );
                                    }
                                }
                                ?>
                            </li>
                            <!-- <li><a href="#"><span>Modelo:</span> <?php echo $modelo = $product['modelo']; ?></a></li> -->
                            <?php  
                                if($product['atributos']){
                                    foreach ($product['atributos'] as $key => $attr) { 
                                        if(!$attr['is_variacion'])
                                        {
                            ?>
                                            <li><a href="#"><span><?php echo $attr['nombre']; ?>: </span> <?php echo $attr['valores']; ?></a></li>
                            <?php
                                        }
                                    }
                                }
                            ?>
                        </ul>
                        <hr>
<!--                         <p><?php echo $product['descripcion']; ?></p> -->
                    </div>
                    <h2 class="pb-3">Seleccione su producto:</h2>
                    <div class="row">
                        <?php 
                            // Se consulta si existen atributos para el producto
                            if($product['atributos']){
                        ?>
                                <form id="attr-producto" class="col-md-12" method="post">
                                    <input name="idProducto" value="<?php echo $product['idProducto']?>" hidden>
                        <?php   
                                    // Se recorre la lista de atributos
                                    foreach ($product['atributos'] as $key => $atributo) {
                                        
                                        //Se consulta por aquellos que esten marcados como VARIACION, para armar el filtro
                                        if($atributo['is_variacion']){
                                            // Se armar el filtro de los atributos de VARIACION
                                            printf('<div class="form-group"><label>%s</label><select name="%s" class="form-control"><option selected disabled>Seleccione...</option>',
                                                    $atributo['nombre'], 
                                                    $atributo['slug']);

                                            //Se recorre la tabla ATR_VALUE_LIST para cargar los posibles valores en el filtro
                                            foreach ($product['atr_value_list'] as $key => $atrib_valores) {
                                                if ($atrib_valores['idAtributo'] == $atributo['idAtributo']){
                                                    $valores_list = json_decode($atrib_valores['valor']);
                                                    
                                                    foreach ($valores_list as $key => $valor) {
                                                        printf('<option value="%s">%s</option>',$valor,$valor);    
                                                    }
                                                    printf('</select></div>'); 
                                                }
                                            }    
                                        }
                                    }
                        ?>
                            <style>
                                .row.var-filter {
                                    margin: auto;
                                }
                                .row.var-filter button {
                                    margin-right: 20px;
                                    margin-top: 20px;
                                }    
                            </style>

                            <div class="row var-filter">
                                    <button class="btn btn-sm btn-primary" type="action">Ver Todo</button>
                                    <button class="btn btn-sm btn-primary" type="action">Buscar</button>
                                    <button class="btn btn-sm btn-primary" id="clear-filter" type="button"><i class="glyphicon glyphicon-remove"></i>Limpiar Filtro</button>
                            </div>
                        </form>
                        <?php 
                            } 
                        ?>

                    </div>                 
                </div>
                <!-- Fin Detalle Producto Variable -->                

                <?php } ?>                    
            </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->

    <?php if ($product['idTipo'] == 0 ){ ?>

    <!--================Product Simple Description Area =================-->
    <section class="product_description_area">
        <div class="container">
            <div class="row">
                <?php  
                    // foreach ($product['atributos'] as $key => $attr) {
                    //     $valores = json_decode($attr['valores']);
                    //     $badge = ''; 
                    // }

                    // if(isset($valores)){
                    // if(count($valores)){ 
                    if($product['atributos']){
                ?>
                    <div class="table-responsive col-md-6">
                        <h2 class="pb-3">Atributos</h2>
                        <table class="table">
                            <tbody>
                        <?php
                            
                            foreach ($product['atributos'] as $key => $attr) {
                                // $valores = json_decode($attr['valores']);
                                // $badge = '';

                                // if(count($valores)){
                                //     foreach ($valores as $valor) { 
                                //         $badge .= $valor;
                                //     }
                                //     printf('<tr class="list-group-item">
                                //                 <td><h5>%s</h5></td>
                                //                 <td>%s</td>
                                //             </tr>',
                                //             $attr['nombre'], 
                                //             $badge
                                //         );
                                // }
                                    printf('<tr class="list-group-item">
                                                <td><h5>%s</h5></td>
                                                <td>%s</td>
                                            </tr>',
                                            $attr['nombre'], 
                                            $attr['valores']
                                        );                                
                            }
                        ?>    </tbody></table>                
                        
                    </div>
                    <?php } if(count($product['aplicacion']['motos'] )) { ?>
                        <div class="table-responsive col-md-6">
                            <h2 class="pb-3">Aplicaciones</h2>
                            <table class="table">
                                <tbody>
                                    <?php
                                        foreach ($product['aplicacion']['motos'] as $key => $moto) {
                                            printf('<tr>
                                                        <td>%s</td>
                                                    </tr>',
                                                    $moto['name']
                                                );
                                        } 
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
            </div>
        </div>
    </section>
    
    <div class="container"><hr></div>
    
    <!--================End Product Description Area =================-->

    <?php 
    
    } elseif ($product['idTipo'] == 1 ) { ?>
    
    <!--================Product Variable Description Area =================-->
    <section class="product_description_area">
        <div class="container">
<!--             <h2 class="pb-3">Seleccione su producto:</h2>
            <div class="row">
                <?php 
                    foreach ($product['atributos'] as $key => $attr) {
                         $valores = json_decode($attr['valor']);
                         $badge = ''; 
                    }
                    if(isset($valores)){
                ?>
                    <form id="attr-producto" class="col-md-12" method="post">
                        <input name="idProducto" value="<?php echo $product['idProducto']?>" hidden>
                <?php   
                        foreach ($product['atributos'] as $key => $attr) {
                            $valores = json_decode($attr['valor']);
                            $badge = '';
                            printf('<div class="form-group"><label>%s</label><select name="%s" class="form-control"><option selected disabled>Seleccione...</option>',$attr['nombre'],$attr['slug']);
                           
                            if(count($valores)){
                                foreach ($valores as $valor) { 
                                    printf('<option value="%s">%s</option>',$valor,$valor);
                                    
                                };
                                printf('</select></div>'); 
                            }
                        }
                ?>
                    <div class="col-md-2"><button class="btn btn-primary" type="action">Buscar</button></div>
                    </form>
                <?php 
                    } 
                ?>

            </div> -->
            <div class="row">
                <table style="margin-top:30px" class="table child-products"></table>
            </div>
        </div>

    </section>                
    <div class="container"><hr></div>
    
    <!--================End Product Description Area =================-->

    <?php } ?>
