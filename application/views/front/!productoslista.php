<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';

    $this->load->view('front/layouts/header',$nav);
    $this->load->view('front/layouts/seccion',$title);
    $this->load->view('front/productos/filtros');
    $this->load->view('front/productos/lista');
    $this->load->view('front/productos/small');
    $this->load->view('front/layouts/footer');

?>