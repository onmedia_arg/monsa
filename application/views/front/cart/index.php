<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';

    $this->load->view('front/layouts/header', $nav);
    $this->load->view('front/layouts/seccion',$title);
    $this->load->view('front/cart/list');
    $this->load->view('front/layouts/destacados');
    $this->load->view('front/layouts/footer');

?>