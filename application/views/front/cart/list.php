
<section class="cart-list-area pb-40">
    <div class="container section_gap">
        <div class="section-title col-md-6">
            <h1>Nuevo Pedido</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box-shadow">
                    <div class="spinner"><i class="spinner-grow text-warning"></i></div>
                    <form class="form-cart">
                        <div class="col-md-12">
                            <table class="table cart-list-table">
                            </table>
                        </div>  
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nota-cart">Notas</label>
								<small class="float-right pr-5">Los precios no incluyen IVA</small>
                                <textarea class="form-control" id="nota-cart" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <!-- <div class="checkout_btn_inner d-flex align-items-center"> -->
                            <div class="checkout_btn_inner d-flex">                            
                                <a class="cust-btn gray_btn" href="<?php echo site_url('frontController/Shop')?>"><i class="fas fa-chevron-left"></i> AGREGAR MAS PRODUCTOS</a>
                                <a class="cust-btn primary-btn enviar_pedido" href="#">ENVIAR PEDIDO  <i class="fas fa-angle-right"></i></a>
                            </div>            
                        </div>
                    </form>
                </div>    
            </div>
        </div>
    </div>
</section>
<div class="container"><hr></div>


<!-- End Best Seller -->
