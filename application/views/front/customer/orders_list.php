<div class="container section_gap">
	<div class="row">
		<div class="col-md-3">
			<div class="head">PEDIDOS</div>
			<div class="box-shadow">
				<table class="table" id="orders_list">
					<thead>
						<th>PEDIDO Nº</th>
						<th>CREADO</th>
					</thead>		
				</table>				
			</div>

		</div>

		<div class="col-md-9">
			<h2>Detalle Pedido<span id="nro-pedido"></span></h2>
			<ul class="pedido-detail">
				<li id="detail-created" class="detail-item">Fecha de Creación: <span></span></li>
				<li id="detail-nota"    class="detail-item">Notas:  <span></span></li>
				<li id="detail-total"   class="detail-item">Total:  <span></span></li>
				<li id="detail-estado"  class="detail-item">Estado: <span></span></li>
			</ul>
			<table class="table" id="order_items">
				<thead>
					<th>POSICIÓN</th>
					<th>ID PROD</th>
					<th>PRECIO</th>
					<th>CANTIDAD</th>
					<th>TOTAL</th>
				</thead>		
			</table>
		</div>	
	</div>	
</div>			