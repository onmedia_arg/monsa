<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';

    $this->load->view('front/layouts/header',$nav);
    $this->load->view('front/home/banner');
    $this->load->view('front/home/badges');
    $this->load->view('front/home/families', $nav);    
    $this->load->view('front/home/marcas');
    $this->load->view('front/home/productos');
    $this->load->view('front/layouts/destacados');
    $this->load->view('front/layouts/footer');

?>