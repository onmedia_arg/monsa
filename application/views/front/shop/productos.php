        <div class="col-xl-9 col-lg-8 col-md-7">
            <ul id="active-filters">
                <!-- Filter -->
            </ul>
			<section class="shop-buttons">
				<button id="btn-grid-layout" class="cust-btn gray_btn"><i class="fas fa-th"></i>VER CUADRICULA</button>
				<button id="btn-list-layout"  class="cust-btn gray_btn"><i class="fas fa-list"></i>VER LISTA</button>
			</section>
            <div class="spinner"><i class="spinner-grow text-warning"></i></div>
            <section class="lattest-product-area pb-40 category-list">
                <div class="row filter_data" id="catalogo"></div>                    
                <div align="center" id="pagination_link"></div>
            </section>
        </div>
    </div>
</div>