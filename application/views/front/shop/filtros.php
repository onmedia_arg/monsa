<style>
    .sidebar-categories .head{
        MARGIN-BOTTOM: 5PX;
    }

    .list-group {
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
        border-radius: 4px;
    }
    
    .mt-50{
        margin-top: 50px;
    }
    
</style>
<div class="container section_gap">
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="sidebar-categories">
                    <div class="head">FAMILIA</div>
                    <!-- <ul class="main-categories"></ul> -->
                    <div class="list-group">
                        <?php foreach($family_data as $familia) { 

                                    // si hay un parametro en el url
                                    if ( isset($_GET['familia']) ) {

                                        // Revisa si son varios parametros
                                        $getArray = ( strpos($_GET['familia'], ',') !== false ) ? explode(',', $_GET['familia']) : $_GET['familia'] ;

                                        // Si son varios revisa si esta en el array
                                        if ( is_array($getArray) ) {
                                            $checked = ( in_array( $familia['idFamilia'], $getArray ) )  ? 'checked' : null ;
                                        }else{
                                            $checked = ( $getArray == $familia['idFamilia'] ) ? 'checked' : null ;
                                        }   

                                    }else{
                                        $checked = null;
                                    }

                            ?>
                            <div class="list-group-item checkbox">
                                <label>
                                    <input type="checkbox" class="common_selector familia" name="familia" data-name="<?= $familia["nombre"] ?>" data-familia="<?= $familia['idFamilia'] ?>" value="<?php echo $familia['idFamilia']; ?>" <?= $checked ?> > 
                                    <?php echo $familia["nombre"]; ?>
                                </label>
                            </div>                            
                        <?php } ?>                          
                    </div>
                </div>
                <!-- <div class="sidebar-categories sidebar-filter mt-50"> -->
                <!--    <div class="top-filter-head">MARCA</div> -->
                <div class="sidebar-categories mt-50">   
                    <div class="head">MARCA</div> 
                    
                    <div class="list-group">
                        <?php foreach($marca_data as $marca) { 

                                // si hay un parametro en el url
                                if ( isset($_GET['marca']) ) {

                                    // Revisa si son varios parametros
                                    $getArray = ( strpos($_GET['marca'], ',') !== false ) ? explode(',', $_GET['marca']) : $_GET['marca'] ;

                                    // Si son varios revisa si esta en el array
                                    if ( is_array($getArray) ) {
                                        $checked = ( in_array( $marca['idMarca'], $getArray ) )  ? 'checked' : null ;
                                    }else{
                                        $checked = ( $getArray == $marca['idMarca'] ) ? 'checked' : null ;
                                    }   

                                }else{
                                    $checked = null;
                                }

                            ?>
                            <div class="list-group-item checkbox">
                                <label>
                                    <input type="checkbox" class="common_selector marca" name="marca" data-name="<?= $marca["nombre"] ?>" data-marca="<?= $marca['idMarca'] ?>" value="<?php echo $marca['idMarca']; ?>" 
                                    <?= $checked ?> > <?php echo $marca["nombre"]; ?>
                                </label>
                            </div>                            
                        <?php } ?>                          
                    </div>
                </div>
            </div>
            
          