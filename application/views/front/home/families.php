<?php  
    foreach ($nav['nav_family'] as $key => $familia) {
        $familias[$key]['id'] = $familia['idFamilia'];
        $familias[$key]['nombre'] = $familia['nombre'];
        $familias[$key]['slug'] = $familia['slug'];
    }
?>

<!-- start features Area -->
<section class="family-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title">
                    <h1 style="width:70%;">Búsqueda por Categoría</h1>
                </div>
            </div>
        </div> 
    </div>

    <div class="container family-container">
        <div class="row equal">
            <?php   
                foreach ($familias as $familia) {
                    if ($familia['slug'] != 'todas'){
                        echo '<div class="col-md-2 col-sm-6 single-family">
                                <div class="family-widget">
                                    <a class="family-link" href="'.site_url("/frontController/shop?familia=".$familia['id']).'"><p>'.$familia['nombre'].'</p></a></div></div>';
                    }
                }
            ?>
        </div>
    </div>
</section>
<!-- end features Area -->