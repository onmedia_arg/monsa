            <!-- start features Area -->
            <section class="features-area section_gap">
                <div class="container">
                    <div class="row features-inner">
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-truck fa-2x"></i>
                                </div>
                                <h6>Envios a domicilio</h6>
                                
                            </div>
                        </div>
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-undo fa-2x"></i>
                                </div>
                                <h6>Políticas de reembolso</h6>
                                
                            </div>
                        </div>
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-headset fa-2x"></i>
                                </div>
                                <h6>Atención al cliente</h6>
                                
                            </div>
                        </div>
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-database fa-2x"></i>
                                </div>
                                <h6>Formas de pago  online</h6>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end features Area -->