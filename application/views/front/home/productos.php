<!-- start product Area -->
<section class=" section_gap">
    <!-- single product slide -->
    <div class="single-product-slider">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h1>Últimos productos</h1>
                       </div>
                </div>
            </div>
            <div class="row">
                <section class="lattest-product-area pb-40 category-list">
                    <div class="row" id="productos-home"></div>                    
                </section>
            </div>
        </div>
    </div>
</section>
<!-- end product Area -->