            <!-- start banner Area -->
            <section class="banner-area">
                <div class="container">
                    <div class="row fullscreen align-items-center justify-content-start banner-fix">
                        <div class="col-lg-12">
                            <div class="active-banner-slider owl-carousel">
                                <!-- single-slide -->
                                <div class="row single-slide align-items-center d-flex">
                                    <div class="col-lg-5 col-md-6">
                                        <div class="banner-content">
                                            <h1>Nueva colección de cascos!</h1>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                            <div class="add-bag d-flex align-items-center">
                                                <a class="add-btn" href=""><i class="fas fa-plus"></i></a>
                                                <span class="add-text text-uppercase">ver más</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="banner-img">
                                            <img class="img-fluid" src="<?php echo site_url('resources/img/banner.png');?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <!-- single-slide -->
                                <div class="row single-slide">
                                    <div class="col-lg-5">
                                        <div class="banner-content">
                                            <h1>Otra imagen de banner!</h1>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                            <div class="add-bag d-flex align-items-center">
                                                <a class="add-btn" href=""><i class="fas fa-plus"></i></a>
                                                <span class="add-text text-uppercase">ver más</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7">
                                        <div class="banner-img">
                                            <img class="img-fluid" src="<?php echo site_url('resources/img/banner2.png');?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End banner Area -->