<style>
    /**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */
.owl-theme .owl-dots,.owl-theme .owl-nav{text-align:center;-webkit-tap-highlight-color:transparent}.owl-theme .owl-nav{margin-top:10px}.owl-theme .owl-nav [class*=owl-]{color:#FFF;font-size:14px;margin:5px;padding:4px 7px;background:#D6D6D6;display:inline-block;cursor:pointer;border-radius:3px}.owl-theme .owl-nav [class*=owl-]:hover{background:#869791;color:#FFF;text-decoration:none}.owl-theme .owl-nav .disabled{opacity:.5;cursor:default}.owl-theme .owl-nav.disabled+.owl-dots{margin-top:10px}.owl-theme .owl-dots .owl-dot{display:inline-block;zoom:1}.owl-theme .owl-dots .owl-dot span{width:10px;height:10px;margin:5px 7px;background:#D6D6D6;display:block;-webkit-backface-visibility:visible;transition:opacity .2s ease;border-radius:30px}.owl-theme .owl-dots .owl-dot.active span,.owl-theme .owl-dots .owl-dot:hover span{background:#869791}

</style>            

            <!-- Start brand Area -->
            <section class="brand-area section_gap">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-6">
                            <div class="section-title">
                                <h1>Nuestras marcas</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'1') ?>">
                                    <img src="<?php echo site_url('resources/img/marcas/agv.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/cometa.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'12');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/circuit.png');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'9');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/delta.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'3');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/did.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/duro.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'10');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/hfk.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'4');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/horng.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'7');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/kryptonite.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'2');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/metzeler.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'8');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/motul.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'11');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/ngk.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'6');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/piton.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'13');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/pro-tork.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'5');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/riffel.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/stlu.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/wester.jpg');?>">
                                </a>
                            </div>
                            <div class="item">
                                <a href="<?php echo site_url('/frontController/shop?marca='.'0');?>">
                                    <img src="<?php echo site_url('resources/img/marcas/yuasa.png');?>"></div>                      
                                </a>

                        </div>
                    </div>      
                </div>
            </section>
            <!-- End brand Area -->

