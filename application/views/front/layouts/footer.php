            <!-- start footer Area -->
            <footer class="footer-area section_gap">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3  col-md-6 col-sm-6">
                            <div class="single-footer-widget">
                                <img class="logo" src="<?php echo site_url('resources/img/monsa-srl-logo.png');?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-6">
                            <div class="single-footer-widget">
                                <h6>Menu</h6>
                                <div class="footer-social d-flex align-items-center">
                                    <ul class="">
                                        <li class="nav-item active"><a class="nav-link" href="<?php echo site_url('/');?>">HOME</a></li>
                                        <li class="nav-item submenu dropdown">
                                            <a href="#" class="nav-link " >CATEGORÍAS</a>
                                        </li>
                                        <li class="nav-item submenu dropdown">
                                            <a href="#" class="nav-link " >PRODUCTOS</a>
                                        </li>
                                        <li class="nav-item submenu dropdown">
                                            <a href="#" class="nav-link " >MARCAS</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="contact.html">CONTACTO</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4  col-md-6 col-sm-6">
                            <div class="single-footer-widget">
                                <h6>Newsletter</h6>
                                <p>Subscribite para recibir novedades y descuentos</p>
                                <div class="" id="mc_embed_signup">

                                    <form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                     method="get" class="form-inline">

                                        <div class="d-flex flex-row">

                                            <input class="form-control" name="EMAIL" placeholder="Ingresá tu email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ingresá tu email '"
                                             required="" type="email">


                                            <button class="click-btn btn btn-default"><i class="fas fa-angle-right"></i></button>
                                            <div style="position: absolute; left: -5000px;">
                                                <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                            </div>
                                        </div>
                                        <div class="info"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3  col-md-6 col-sm-6">
                            <div class="single-footer-widget mail-chimp">
                                <h6 class="mb-20">Seguínos en Instagram</h6>
                                <ul class="instafeed d-flex flex-wrap">
                                    <li><img src="<?php echo site_url('resources/img/ig.jpg');?>" alt=""></li>
                                    <li><img src="<?php echo site_url('resources/img/ig.jpg');?>" alt=""></li>
                                    <li><img src="<?php echo site_url('resources/img/ig.jpg');?>" alt=""></li>
                                    <li><img src="<?php echo site_url('resources/img/ig.jpg');?>" alt=""></li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
                        <p class="footer-text m-0">
        &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado por <a href="https://onmedia.com.ar" target="_blank">onMedia</a>
        </p>
                    </div>
                </div>
            </footer>
            <!-- End footer Area -->
  
        </div>
        <!-- ./wrapper -->

        <script defer src="<?= site_url('resources/js/front/jquery-3.4.0.min.js');?>"></script>
        <script defer src="<?= site_url('resources/js/front/bootstrap.bundle.min.js');?>"></script>
        <script defer src="<?= site_url('resources/js/front/owl.carousel.min.js');?>"></script>
        <script defer src="<?= site_url('resources/js/front/jquery.nice-select.min.js');?>"></script>
        <script defer src="<?= site_url('resources/js/front/jquery.sticky.js');?>"></script>
        <script defer src="<?= site_url('resources/js/front/nouislider.min.js');?>"></script>
        <script defer src="<?= site_url('resources/js/front/bootstrap-notify.min.js');?>"></script>
        <script defer src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <!-- iziToast -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" integrity="sha256-f6fW47QDm1m01HIep+UjpCpNwLVkBYKd+fhpb4VQ+gE=" crossorigin="anonymous" /> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha256-321PxS+POvbvWcIVoRZeRmf32q7fTFQJ21bXwTNWREY=" crossorigin="anonymous"></script>        
        <script defer src="<?php echo site_url('resources/js/front/scripts.js');?>"></script>

    </body>
                <!-- Modal -->
                <div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel2">Right Sidebar</h4>
                            </div>

                            <div class="modal-body">
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </p>
                            </div>

                        </div><!-- modal-content -->
                    </div><!-- modal-dialog -->
                </div><!-- modal -->     
</html>