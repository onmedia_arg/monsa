<?php 
    $avatar     = 'resources/img/ninja.jpg';
    $userstring = (isset($user['email'])) ? $user['email'] : 'Invitado';
    $role       = (isset($user['role'])) ? $user['role'] : '';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Monsa</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/font-awesome.min.css');?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/AdminLTE.min.css');?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/_all-skins.min.css');?>">
         <!-- jQuery 2.2.3 -->
        <script src="<?php echo site_url('resources/js/jquery-2.2.3.min.js');?>"></script> 
        <style>
            .navbar-nav > .user-menu > .dropdown-menu{
                min-width: 320px !important;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini" data-url="<?php echo base_url(); ?>">
        <div class="wrapper" id="wrapper-app" data-url="<?php echo base_url() ?>">
            <header class="main-header">
                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">Monsa</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">Monsa</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu " id="cart">
                                <?php $this->load->view('front/cart') ?>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo site_url($avatar);?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $userstring; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo site_url($avatar);?>" class="img-circle" alt="User Image">
                                        <p><?php echo $userstring; ?> </p>
                                        <small><?php echo $role; ?></small>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <!-- <div class="pull-left">
                                            <a href="<?php //echo base_url(); ?>" class="btn btn-info btn-flat">Profile</a>
                                        </div> -->
                                        <div class="pull-right">
                                            <!-- <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                                            <?php 
                                                printf('<a href="%s" id="login-link" class="btn btn-info btn-flat">
                                                            <i class="fa fa-user"></i> %s
                                                        </a>',
                                                        base_url(),
                                                        'Login'
                                                     );
                                            ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url($avatar);?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $userstring; ?></p>
                            <small><?php echo $role; ?></small>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li>
                            <a href="<?php echo base_url();?>">
                                <i class="fa fa-dashboard"></i> <span>Inicio</span>
                            </a>
                        </li>

                        <li class="header">Categorías</li>

                        <?php

                            $famInCats  = [];
                            $allCats = [];
                            foreach ($nav['nav_cats'] as $cat) {
                                if (!in_array($cat['idFamilia'], $famInCats)) {
                                    $item[$cat['idFamilia']] = $cat['familia'];
                                    array_push($famInCats, $cat['idFamilia']);
                                }
                            }
                            foreach ($famInCats as $idFamilia) {
                                $allCats[$idFamilia] = [];
                                foreach ($nav['nav_cats'] as $cat) {
                                    if ($cat['idFamilia'] == $idFamilia) {
                                        array_push($allCats[$idFamilia], $cat);
                                    }
                                }
                            }

                            foreach ($allCats as $familiaItem) {
                                $list = '';
                                foreach ($familiaItem as $catItem) {
                                    $list .= '<li>';
                                    $list .=    '<a href="'. base_url('categoria/buscar/').$catItem['familia_slug'].'/'.$catItem['slug'].'">';
                                    $list .=        '<i class="fa fa-arrow-right"></i> '.$catItem['nombre'];
                                    $list .=    '</a>';
                                    $list .='</li>';
                                }
                                
                                // if ($familiaItem[0]['familia_slug'] != 'todas') {
                                    printf('
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-star"></i> <span>%s</span>
                                                </a>
                                                <ul class="treeview-menu">%s</ul>
                                            </li>
                                        ',  
                                        $familiaItem[0]['familia'],
                                        $list 
                                    );
                                // }
                            }
                        ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <?php
                        if(isset($_view) && $_view)
                        $this->load->view($_view);
                    ?>                    
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>onMedia</strong>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo site_url('resources/js/bootstrap.min.js');?>"></script>
        <!-- FastClick -->
        <script src="<?php echo site_url('resources/js/fastclick.js');?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo site_url('resources/js/app.min.js');?>"></script>
        <!-- AdminLTE for demo purposes -->
        <!-- <script src="<?php echo site_url('resources/js/demo.js');?>"></script> -->
        <!-- DatePicker -->
        <script src="<?php echo site_url('resources/js/global.js');?>"></script> -->
        <script src="<?php echo site_url('resources/js/monsa.js');?>"></script>
    </body>
</html>
