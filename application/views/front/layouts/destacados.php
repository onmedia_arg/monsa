<!-- Start related-product Area -->
<section class="related-product-area productos-small section_gap">
    <div class="container">
        <div class="row ">
            <div class="col-lg-6">
                <div class="section-title">
                    <h1>Destacados</h1>
                </div>
            </div>
        </div>
        <div class="row" id="prod-destacados">
        </div>
    </div>
</section>
<!-- End related-product Area -->