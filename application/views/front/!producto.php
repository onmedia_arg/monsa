<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';
	
	$data = array('title' => $product['nombre']);

    $this->load->view('front/layouts/header',$nav);
    $this->load->view('front/layouts/seccion',$data);
    $this->load->view('front/productos/detalle');
    $this->load->view('front/layouts/destacados');
    $this->load->view('front/layouts/footer');

?>