<?php
 
class Producto_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_producto($idProducto)
    {
        return $this->db->get_where('producto',array('idProducto'=>$idProducto))->row_array();
    }

	function get_producto_by_sku( $sku='', $return = 'row_array' ){

        switch ($return) {
            case 'row_array':
                return $this->db->get_where( 'producto', array( 'sku' => $sku ) )->row_array(); 
                break;
            
            case 'row_array': 
                return $this->db->get_where( 'producto', array( 'sku' => $sku ) )->num_rows(); 
                break;

            default:
                return $this->db->get_where( 'producto', array( 'sku' => $sku ) )->row_array(); 
                break;
        }

	}

    function get_product_by_str($str){
        $this->db->select('
                            pr.idProducto,
                            pr.idTipo,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones,
                            pr.idParent
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        $this->db->where('pr.slug',$str);
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 

    }

    function get_all_producto($page=0,$perpage=20,$fam='',$marca=''){
        $page = $page-1;
        if ($page<0) { 
            $page = 0;
        }
        $from = $page*$perpage;

        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            ma.slug as marcaSlug,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->limit($perpage, $from);        
        if($this->session->userdata('preciomin') && $this->session->userdata('preciomax'))
            $this->db->where('pr.precio BETWEEN "'.$this->session->userdata('preciomin').'" AND "'.$this->session->userdata('preciomax').'"');
        if($this->session->userdata('ord'))
           $this->db->order_by('pr.idProducto', $this->session->userdata('ord'));
        else
            $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '' && $fam != "todas") {
            $this->db->where('fa.slug',$fam);
        }
        if ($marca != '' && !is_numeric($marca)) {
            $this->db->where('ma.slug',$marca);
        }

        $query = $this->db->get('producto as pr');
        return $query->result_array(); 

    }

    public function getAllProducto($start, $length, $search){
        
        $where = "";
        
        if($search != null && trim($search) != ""){

            $search_like = $this->db->escape("%".$search."%");

            $where = " where (fa.nombre  LIKE " .$search_like . " OR
                              ma.nombre  LIKE " .$search_like . " OR
                              pr.modelo  LIKE " .$search_like . " OR
                              pr.sku     LIKE " .$search_like . " OR
                              pr.stock   LIKE " .$search_like . " )";
         }
      
// //      Query para levantar los datos
        $r = $this->db->query("SELECT pr.idProducto,
                                fa.idFamilia,
                                fa.nombre AS familia,
                                fa.slug AS familiaSlug,
                                ma.idMarca,
                                ma.nombre AS marca,
                                ma.slug AS marcaSlug,
                                pr.nombre,
                                pr.slug,
                                pr.modelo,
                                pr.descripcion,
                                pr.sku,
                                pr.precio,
                                pr.stock,
                                pr.peso,
                                pr.visibilidad,
                                pr.state,
                                pr.imagen,
                                pr.dimensiones
                            FROM producto AS pr
                            LEFT OUTER JOIN familia AS fa ON fa.idFamilia = pr.idFamilia 
                            LEFT OUTER JOIN marca   AS ma ON ma.idMarca = pr.idMarca
                            ". $where
                            ." ORDER BY pr.idProducto ASC LIMIT $start, $length");

// //      Misma query sin LIMIT para contar los registros
        $r2 = $this->db->query("SELECT pr.idProducto,
                                fa.idFamilia,
                                fa.nombre AS familia,
                                fa.slug AS familiaSlug,
                                ma.idMarca,
                                ma.nombre AS marca,
                                ma.slug AS marcaSlug,
                                pr.nombre,
                                pr.slug,
                                pr.modelo,
                                pr.descripcion,
                                pr.sku,
                                pr.precio,
                                pr.stock,
                                pr.peso,
                                pr.visibilidad,
                                pr.state,
                                pr.imagen,
                                pr.dimensiones
                            FROM producto AS pr
                            LEFT OUTER JOIN familia AS fa ON fa.idFamilia = pr.idFamilia 
                            LEFT OUTER JOIN marca   AS ma ON ma.idMarca = pr.idMarca
                            ". $where );

        $response = array(
                    "numRows" => $r2->num_rows(), 
                    "datos"   => $r
                    );

        return $response;

    }    

    public function getNumRowAll(){

        $r = $this->db->get('producto');

        return $r->num_rows();

    }

    function get_all_producto_count($fam='',$marca=''){


        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            ma.slug as marcaSlug,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');

        if($this->session->userdata('ord'))
           $this->db->order_by('pr.idProducto', $this->session->userdata('ord'));
        else
            $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '' && $fam != "todas") {
            $this->db->where('fa.slug',$fam);
        }
        if($this->session->userdata('preciomin') && $this->session->userdata('preciomax'))
           $this->db->where('pr.precio BETWEEN "'.$this->session->userdata('preciomin').'" AND "'.$this->session->userdata('preciomax').'"');
        if ($marca != '' && !is_numeric($marca)) {
            $this->db->where('ma.slug',$marca);
        }        
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    }

    function get_all_producto_con_img($page=0,$perpage=20,$fam='') {
        $page = $page-1;
        if ($page<0) { 
            $page = 0;
        }
        $from = $page*$perpage;

        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->where('pr.imagen != ""');
        $this->db->where('pr.imagen != "[]"');
        $this->db->limit($perpage, $from);
        $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '') {
            $this->db->where('fa.slug',$fam);
        }
        $query = $this->db->get('producto as pr');

        return $query->result_array(); 
    }

    function get_all_producto_con_img_count($fam='') {
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->where('pr.imagen != ""');
        $this->db->where('pr.imagen != "[]"');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '') {
            $this->db->where('fa.slug',$fam);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    }    

    function get_producto_by_cat($cat=''){
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones, 
                            ca.idCategoria, 
                            ca.nombre as categoria,
                            ca.slug as categoriaSlug,
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->join('producto_categoria as pc', 'pc.idProducto = pr.idProducto', 'OUTER LEFT');
        $this->db->join('categoria as ca', 'ca.idCategoria = pc.idCategoria', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($cat != '') {
            $this->db->where('ca.slug',$cat);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    }

    function get_producto_by_id($id=''){
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones, 
                            ca.idCategoria, 
                            ca.nombre as categoria,
                            ca.slug as categoriaSlug,
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->join('producto_categoria as pc', 'pc.idProducto = pr.idProducto', 'OUTER LEFT');
        $this->db->join('categoria as ca', 'ca.idCategoria = pc.idCategoria', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($id != '') {
            $this->db->where('pr.idProducto',$id);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    }
        
    /*
     * function to add new producto
     */
    function add_producto($params)
    {
        $this->db->insert('producto',$params);
        $sql = $this->db->last_query();
        return $this->db->insert_id();
    }
    
    /*
     * function to update producto
     */
    function update_producto($idProducto,$params)
    {
        $this->db->where('idProducto',$idProducto);
        return $this->db->update('producto',$params);
    }
    
    /*
     * function to delete producto
     */
    function delete_producto($idProducto)
    {
        return $this->db->delete('producto',array('idProducto'=>$idProducto));
    }

    function make_query($marca, $familia)
    {
        $query = "
        SELECT * FROM producto 
        WHERE visibilidad = 'publicar'
          AND idTipo != 2  
        ";

        if(isset($marca))
        {
            $marca_filter = implode("','", $marca);
            $query .= "
             AND idMarca IN('".$marca_filter."')
            ";
        }

        if(isset($familia))
        {
            $familia_filter = implode("','", $familia);
            $query .= "
             AND idFamilia IN('".$familia_filter."')
            ";
        }

        return $query;
    }

    function countAll($marca, $familia)
    {
        $query = $this->make_query($marca, $familia);
        $data  = $this->db->query($query);
        return $data->num_rows();
    }

    function fetch_data($limit, $start, $marca, $familia){

        $query  = $this->make_query($marca, $familia);
        $query .= ' LIMIT '.$start.', ' . $limit;

        $data = $this->db->query($query);
        $output = '';
        
        return $data->result_array();
    } 

    function get_products_home(){

        $this->db->where('show_home', 1);
        $this->db->where('visibilidad', 'publicar');
        
        $r = $this->db->get('producto',8);
        return $r->result_array();    
    }

    function get_products_destacado(){
        $this->db->select('
                            pr.idProducto,
                            pr.nombre,
                            pr.modelo,
                            pr.slug,
                            pr.precio,
                            pr.imagen,
                        ');
        $this->db->where('destacado',1); 
        $this->db->limit(8);
        $query = $this->db->get('producto as pr');
        return $query->result_array(); 
    } 

    function get_child_by_parent($idParent){

        $query = " SELECT pr.idProducto, 
                          pr.nombre as prod_nombre,
                          pr.sku,
                          pr.precio, 
                          at.idAtributo, 
                          at.valores, attr.nombre, attr.slug 
                     FROM producto as pr
                    inner join producto_atributo as at on pr.idProducto = at.idProducto
                    inner join atributo as attr on at.idAtributo = attr.idAtributo
                    where pr.idParent = " . $idParent .
                    " order by at.idProducto asc";

        $r = $this->db->query($query);
        
        // $r  = $this->db->get_where('producto',array('idParent'=>$idParent));

        return $r->result_array(); 
        

    }

    /*
     * function to add new producto
     * @return Insert ID
     */
    public function add_variacion( $data )
    {
        
        $query = $this->db->insert( 'producto_atributo', $data );

        if ( $query !== true ) {
            return false;
        }else{
            $sql = $this->db->last_query();
            return $this->db->insert_id();
        }

    } 

    /*
     * function to edit variacion
     * @return Insert ID
     */
    public function update_variacion( $idProducto, $idAtributo, $data )
    {
        $this->db->where( 'idProducto', $idProducto )
                 ->where( 'idAtributo', $idAtributo );
        $delete = $this->db->delete('producto_atributo');

        if ( $delete ) {

            // Guardar
            $insert = $this->db->insert( 'producto_atributo', $data );

            if ($insert !== true) {
                // No se pudo guardar
                return false;
            }else{
                $sql = $this->db->last_query();
                return $this->db->insert_id();
            }

        }else{
            // No se pudo borrar
            return false;
        }
        
    } 


    public function get_variations_options( $idProducto = null, $idAtributo = null ){

        $atributos = $this->db->where(  [   
                                            'idProducto' => $idProducto,
                                            'idAtributo' => $idAtributo,
                                            'is_variacion' => 1
                                        ] )
                              ->get('producto_atributo')
                              ->result_array(); 

        $options = array();

        foreach ( $atributos as $atributo ) { 

            if ( is_array( json_decode( $atributo['valor'], true ) ) ) {

                $json = json_decode( $atributo['valor'] );

                foreach ( $json as $value ) {
                    $options[] = $value; 
                }

            }

        }

    }

}
