<?php
 
class ProductoAtributo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get producto_atributo by idProdAtrib
     */
    function get_producto_atributo($idProdAtrib)
    {
        return $this->db->get_where('producto_atributo',array('idProdAtrib'=>$idProdAtrib))->row_array();
    }
    
    /*
     * Get all producto_atributo count
     */
    function get_all_producto_atributo_count()
    {
        $this->db->from('producto_atributo');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all producto_atributo
     */
    function get_all_producto_atributo($params = array())
    {

        $this->db->order_by('idProdAtrib', 'desc');

        if(isset($params['limit']) && !empty($params['offset']))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }

        if ( isset( $params['idProducto'] ) && ! empty( $params['idProducto'] ) ) {
            $this->db->where('idProducto', $params['idProducto']);
        }

        if ( isset( $params['idAtributo'] ) && ! empty( $params['idAtributo'] ) ) {
            $this->db->where('idAtributo', $params['idAtributo']);
        }

        return $this->db->get('producto_atributo')->result_array();

    }

    function get_all_atributo_by_product($idProducto)
    {

        $this->db->select(' pa.idProdAtrib,
                            pa.idProducto, 
                            pa.idAtributo, 
                            at.slug, 
                            at.nombre,
                            pa.valores,
                            pa.is_variacion
                        ');
        $this->db->join('atributo as at', 'at.idAtributo = pa.idAtributo', 'OUTER LEFT');
        $this->db->where('pa.idProducto', $idProducto);
        $this->db->order_by('pa.idProducto', 'desc');
        $query = $this->db->get('producto_atributo as pa');

        return $query->result_array(); 
    }

    function atributos_por_familia($idFamilia)
    {

        $this->db->where('idFamilia', $idFamilia)
                 ->order_by('idAtributo', 'desc');
 
        $query = $this->db->get('atributo');

        return $query->result_array(); 

    } 

    /*
     * function to add new producto_atributo
     */
    function get_all_variaciones_by_product($idProducto)
    {

        $query = $this->db->where( 'idProducto', $idProducto )
                          ->where( 'is_variacion', 1 )
                          ->get('producto_atributo');

        if ( $query->num_rows() >= 1 ) {
            return json_decode( $query->result_array()[0]['valores'] );
        }

    }

    /*
     * function to add new producto_atributo
     */
    function add_producto_atributo($params)
    {
        $this->db->insert('producto_atributo',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update producto_atributo
     */
    function update_producto_atributo($idProdAtrib,$params)
    {
        $this->db->where('idProdAtrib',$idProdAtrib);
        return $this->db->update('producto_atributo',$params);
    }
    
    /*
     * function to delete producto_atributo
     */
    function delete_producto_atributo($idProdAtrib)
    {
        return $this->db->delete('producto_atributo',array('idProdAtrib'=>$idProdAtrib));
    }

    function is_variable($idProducto){

        $count = $this->db->where( 'idProducto', $idProducto )
                         ->where( 'idTipo', 2 )
                         ->get( 'producto' )
                         ->num_rows();
        
        if ( $count >= 1) {
            return true;
        }else{
            return false;
        }

    }

    function is_variacion($idProdAtrib){

        $count = $this->db->where( 'idProdAtrib', $idProdAtrib )
                         ->where( 'is_variacion', 1 )
                         ->get( 'producto' )
                         ->num_rows();
        
        if ( $count >= 1) {
            return true;
        }else{
            return false;
        }

    }

}
