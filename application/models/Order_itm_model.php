<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Order_itm_model extends CI_Model{

	function __construct(){
		parent::__construct();


	}

    function addItems($data){
        $this->db->insert_batch('order_itm',$data);
        return $this->db->insert_id();
    }

	function getItemsByOrder($orderid){

		$r = $this->db->get_where('order_itm', array('idOrderHdr'=>$orderid));
		return $r->result();
	}


}