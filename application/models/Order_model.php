<?php
 
class Order_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get order by idOrder
     */
    function get_order($idOrder)
    {
        return $this->db->get_where('order',array('idOrder'=>$idOrder))->row_array();
    }

    function get_order_by_user($idUser, $state = "")
    {

        // $this->db   ->select()
        //             ->order_by('or.idOrder', 'desc');
        // if ($idUser != '') {
        //     $this->db->where('or.idUser',$idUser);
        // }
        // if ($state != '') {
        //     $this->db->where('or.state',$state);
        // }
        // $query = $this->db->get('order as or');
        // return $query->result_array();
        
            $where = "";
            if ($state != "" && trim($state) != "" ){
                $where = " AND  hdr.state = " . $state ;
            }

            $r = $this->db->query("SELECT hdr.idOrder, 
                                          hdr.idUser, 
                                          hdr.state, 
                                          hdr.created, 
                                          pos.quantity, 
                                          pos.price 
                                     FROM `order` "."  AS hdr
                                    LEFT OUTER JOIN order_item AS pos ON hdr.idOrder = pos.idOrder
                                    WHERE hdr.idUser = " . $idUser . $where );
                               
            
            return $r->result_array();
    }

    function get_current_order_by_user($idUser, $state = "")
    {

        $this->db   ->select()
                    ->order_by('or.idOrder', 'desc');
        if ($idUser != '') {
            $this->db->where('or.idUser',$idUser);
        }
        if ($state != '') {
            $this->db->where('or.state',$state);
        }
        $query = $this->db->get('order as or');
        return $query->result_array();

    }

    

    /*
     * Get all order
     */
    function get_all_order()
    {
        $this->db->order_by('idOrder', 'desc');
        return $this->db->get('order')->result_array();
    }
        
    /*
     * function to add new order
     */
    function add_order($params)
    {
        $this->db->insert('order',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update order
     */
    function update_order($idOrder,$params)
    {
        $this->db->where('idOrder',$idOrder);
        return $this->db->update('order',$params);
    }
    
    /*
     * function to delete order
     */
    function delete_order($idOrder)
    {
        return $this->db->delete('order',array('idOrder'=>$idOrder));
    }
}
