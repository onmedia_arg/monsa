<?php
 
class Denied_acces_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get denied_acces by ai
     */
    function get_denied_acces($ai)
    {
        return $this->db->get_where('denied_access',array('ai'=>$ai))->row_array();
    }
        
    /*
     * Get all denied_access
     */
    function get_all_denied_access()
    {
        $this->db->order_by('ai', 'desc');
        return $this->db->get('denied_access')->result_array();
    }
        
    /*
     * function to add new denied_acces
     */
    function add_denied_acces($params)
    {
        $this->db->insert('denied_access',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update denied_acces
     */
    function update_denied_acces($ai,$params)
    {
        $this->db->where('ai',$ai);
        return $this->db->update('denied_access',$params);
    }
    
    /*
     * function to delete denied_acces
     */
    function delete_denied_acces($ai)
    {
        return $this->db->delete('denied_access',array('ai'=>$ai));
    }
}
