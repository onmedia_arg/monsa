<?php
 
class Familium_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get familium by idFamilia
     */
    function get_familium($idFamilia)
    {
        return $this->db->get_where('familia',array('idFamilia'=>$idFamilia))->row_array();
    }
        
    /*
     * Get all familia
     */
    function get_all_familia()
    {
        $this->db->order_by('idFamilia', 'desc');
        return $this->db->get('familia')->result_array();
    }
        
    /*
     * function to add new familium
     */
    function add_familium($params)
    {
        $this->db->insert('familia',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update familium
     */
    function update_familium($idFamilia,$params)
    {
        $this->db->where('idFamilia',$idFamilia);
        return $this->db->update('familia',$params);
    }
    
    /*
     * function to delete familium
     */
    function delete_familium($idFamilia)
    {
        return $this->db->delete('familia',array('idFamilia'=>$idFamilia));
    } 
    
}
