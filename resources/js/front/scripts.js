var cart_list  = "";

var shop_layout = readCookie("shop_layout");
if (shop_layout == null) {
    // do cookie doesn't exist stuff;
    document.cookie = "shop_layout=default";
}

// Unblock Config
var unblockConfig = {
      message: '<div class="lds-ripple"><div></div><div></div></div>',
        css:{
            backgroundColor: 'transparent',
            border: 'none', 
        },
        overlayCSS: {
            backgroundColor: '#FFF',
        }
  }; 

// iziToast - settings
iziToast.settings({
  timeout: 3000, // default timeout
  resetOnHover: true,
  // icon: '', // icon class
  transitionIn: 'fadeInUp',
  transitionOut: 'fadeOut',
});

$(document).ready(function(){

  var parts = document.location.pathname.split('/');
  console.log(parts);

  var lastSegment = parts.pop() || parts.pop();

  // "use strict";

	var window_width 	 = $(window).width(),
	    window_height  = window.innerHeight,
	    header_height  = $(".default-header").height(),
	    header_height_static = $(".site-header.static").outerHeight(),
	    fitscreen 		 = window_height - header_height;
	
  $(".fullscreen").css("height", window_height)
  $(".fitscreen").css("height", fitscreen);

  //------- Active Nice Select --------//

    // $('select').niceSelect();

    $('.navbar-nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    }); 

    /*=================================
    Funciones para el HOME
    ==================================*/

      /*==========================
  		javaScript for sticky header
  		============================*/
  		$(".sticky-header").sticky();  

      /*=================================
      Javascript for banner area carousel
      ==================================*/
      $(".active-banner-slider").owlCarousel({
          items:1,
          autoplay:true,
          autoplayTimeout: 5000,
          loop:true,
          dots:false
      });

      $('.owl-carousel').owlCarousel({
          loop:true,
          margin:10,
          // nav:true,
          dots:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:3
              },
              1000:{
                  items:6
              }
          }
      })

      /*=================================
      Javascript for product area carousel
      ==================================*/
      $(".active-product-area").owlCarousel({
          items:1,
          autoplay:true,
          autoplayTimeout: 5000,
          loop:true,
          dots:false
      });

      $('.product_carousel').owlCarousel();

      /*=================================
      Javascript for single product area carousel
      ==================================*/
      $(".s_Product_carousel").owlCarousel({
        items:1,
        autoplay:false,
        autoplayTimeout: 5000,
        loop:true,
        nav:false,
        dots:true
      });
      
      /*=================================
      Javascript for exclusive area carousel
      ==================================*/
      $(".active-exclusive-product-slider").owlCarousel({
          items:1,
          autoplay:false,
          autoplayTimeout: 5000,
          loop:true,
          nav:true,
          navText:["<img src='img/product/prev.png'>","<img src='img/product/next.png'>"],
          dots:false
      });

      $('.quick-view-carousel-details').owlCarousel({
          loop: true,
          dots: true,
          items: 1,
      })

    /*=================================
    Fn para Productos del HOME
    ==================================*/
    render_productos_home();

    render_destacados();

     

    /*=================================
    Fn para Pagina SHOP
    ==================================*/

    filter_data(1); 

    filter_init();

    $(document).on('click', '.pagination li a', function(event){
        event.preventDefault();
        var page = $(this).data('ci-pagination-page');
        filter_data(page);
    });

    $(document).on('change', '.common_selector', function(){ 

        if ( $(this).is(':checked') ) {
            var filter = '<li class="single-filter float-left">';
                filter += $(this).attr('data-name')
                filter += '<button remove-filter data-name="' + $(this).attr('name') + '" data-id="' + $(this).val() + '"><i class="fa fa-times"></i></button>'
                filter += '</li>'        
        }

        $('#active-filters').append( filter )

        // Actualizar URL
        var url = new URL(window.location.href);

        var brandFilters = $('input[name=marca]').serializeArray(),
            familyFilters = $('input[name=familia]').serializeArray()

        // Limpiar parametros
        url.searchParams.delete( 'marca' )
        url.searchParams.delete( 'familia' ) 

        $.each(brandFilters, function(i, field){ 
          updateParamURL( url, field.name, field.value )  
        });

        $.each(familyFilters, function(i, field){ 
          updateParamURL( url, field.name, field.value )  
        }); 

        // Actualiza url
        window.history.pushState( null, null, url.href);

        filter_data(1);

    });

    
    $("#btn-grid-layout").on('click',function(){
      document.cookie = "shop_layout=default";
      $('#catalogo').empty();
      $('#pagination_link').empty();
      filter_data(1);
    })
    
    $("#btn-list-layout").on('click',function(){
      document.cookie = "shop_layout=list";
      $('#catalogo').empty();
      $('#pagination_link').empty();      
      filter_data(1);

    })

    $(document).on('mouseenter', '.category-list .row.single-product-list', function(){
      $(this).find('.quantity').css('display','block');
      $(this).find('.quantity').focus()
    });

    $(document).on('mouseleave', '.category-list .row.single-product-list', function(){
      $(this).find('.quantity').css('display','none')
    });


    /*==================================
    Fn Agregar productos al para Carrito
    ===================================*/

    $(document).on('click','#add_cart' ,function(){

        var product_id    = $(this).data("productid");
        var product_name  = $(this).data("productname").replace(/[^a-z0-9\s]/gi, ' ');

        var product_price = $(this).data("price");
        var quantity      = $('#' + product_id).val();

        if (quantity != '' && quantity > 0){
           $.ajax({
              url:  window.base + 'cart/add_to_cart',
              method:"POST",
              data:{
                      product_id   :product_id, 
                      product_name :product_name, 
                      product_price:product_price, 
                      quantity:quantity
                    },
              success:function(data)
              {

                  var response = $.parseJSON(data)

                  if ( response.code == 1 || response.code == '1' ) {

                    iziToast.success({
                        title: 'OK',
                        message: response.message,
                    });
                    // alert("Producto Agregado al Pedido"); 
                    $('#' + product_id).val(1);

                  }else{

                    iziToast.error({
                        title: 'Error!',
                        message: response.message,
                    }); 

                    console.log(response.console)

                  }

              }
           });

        }
    });

    /*==================================
    Fn Borrar productos de Carrito
    ===================================*/

    $(document).on('click', '.remove_item', function(){
    
      var row_id = $(this).attr("id");

      iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          backgroundColor: '#FFB200',
          title: '¿Estás Seguro?',
          message: 'Quieres eliminar el producto?',
          position: 'center',
          buttons: [
              ['<button><b>YES</b></button>', function (instance, toast) {
       
                  instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
       
                  $.ajax({
                    url    : window.base + "cart/remove_from_cart",
                    method : "POST",
                    data   : {row_id:row_id},
                    success:function(data)
                    {

                        var response = $.parseJSON(data)

                        if ( response.code == 1 || response.code == '1' ) {

                          iziToast.success({
                              title: 'OK',
                              message: response.message,
                          });
                          // alert("Producto Agregado al Pedido"); 
                          $('#' + product_id).val(1);

                        }else{

                          iziToast.error({
                              title: 'Error!',
                              message: response.message,
                          }); 

                          console.log(response.console)

                        }

                        render_side_cart();
                        render_cart();
                    }
                  });

              }, true],
              ['<button>NO</button>', function (instance, toast) {
       
                  instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
       
              }],
          ],
      });
 
    });

    /*===================================
    Fn Update cantidad en item de Carrito
    ====================================*/

    $(document).on('change', '.qty', function(){
    
      var row_id = $(this).attr("id");
      var qty    = $(this).val();
      
      $.ajax({
          url    : window.base + "cart/update_cart",
          method : "POST",
          data   :{
                    row_id : row_id,
                    qty    : qty 
                  },
          success:function(data)
          {
              render_side_cart();
              render_cart();
          }
        });
    });    

    var product_id    = $(this).data("productid");

    /*======================================
    Fn para armar el contenido de Side Cart
    =======================================*/
    $('#show-side-cart').click(function(){
      render_side_cart();
    });

    /*======================================
    Fn para armar el contenido del Carrito
    =======================================*/    
    set_cart_list();

    /*======================================
    Fn para Procesar el Carrito
    =======================================*/      

    $('.enviar_pedido').on('click', function(e){
        e.preventDefault();

        var nota = $('#nota-cart').val();

        $.ajax({
            url    : window.base + "cart/process_order",
            method : 'POST',
            data   : {nota : nota},
            success: function(data){

                var response = $.parseJSON(data)

                if ( response.code == 1 || response.code == '1' ) {

                    window.location.replace( response.url );

                }else{

                    iziToast.error({
                        title: 'Error!',
                        message: response.message,
                    }); 

                    console.log(response.console)

                }

            } 
        });
    })

    /*======================================
    Fn para Mostrar pagina Customer
    =======================================*/      
    

    if (lastSegment == 'customer'){
      render_orders_list();
      
      var table = $('#orders_list').DataTable();    
      
      var pedido = GetURLParameter('pedido');
      if (pedido != undefined && pedido.length > 0){
        render_order_details(pedido);
        render_order_items(pedido);

      }
    }
    

    $('#orders_list tbody').on('click', 'tr', function () {
      
      $(this).toggleClass('tr-active');

      var data = table.row( this ).data();
      
      // Se buscan los Items en la BD
      idOrderHdr = data[0]; //idOrderHdr
      render_order_items(idOrderHdr);
      
      //Se completan los detalles del pedido
      $('#detail-created span').html(data[1]);
      $('#detail-nota span').html(data[1]);
      $('#detail-total span').html(data[2]);
      $('#detail-estado span').html(data[3]);
    });


    $( "#attr-producto" ).submit(function( event ) {
      event.preventDefault();
      console.log( $( this ).serializeArray() );

      data = $( this ).serializeArray();

      get_children(data);
      
    }); 

    $('#clear-filter').on('click', function(){
      $('#attr-producto').trigger("reset");
    });

    $('#show-all').on('click', function(){
      $('#attr-producto').trigger("reset");
      data="";
      get_children(data);

    });   

});

  function get_children(data){

      $.ajax({
        url: window.base + "producto/get_children",
        type: "POST",
        data: {data : data},
        success: function(data){
                  result = JSON.parse(data);
                  output = "";
                  output = '<tr><th>SKU</th><th>Atributo</th><th>Precio</th><th>Cantidad</th><th>Agregar</th></tr>';

                  for( var i = 0; i < result.length ; i++ ) {
                    output += '<tr> \
                                <td>' + result[i]["sku"] + '</td>';
                                
                               // for (var j=5; j< result[i].length ; j++ ){
                               //    // output += '<td> Medida: ' + result[i][j] + '<br>Rodado: ' + result[i]["rodado"] + '<br>Valvula: ' + result[i]["valvula"] +  '</td>'; 
                               //    output += '<td> Attr1: ' + result[i][j] + '<br>'  
                               // }   
                               //  output += '</td>';
                              var j = 0;

                              output += '<td>';
                               for(attr in result[i]){
                                   j++;
                                  if (j>5){
                                    output += attr +': ' + result[i][attr] +  '<br>';
                                  }
                                }
                                output += '</td>';
                               

                    output+=  '<td>' + result[i]["show_price"]+ '</td> \
                              <td><input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id='+ result[i]["idProducto"]  +'></td> \
                              <td><button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" \
                                    data-productname = "' + result[i]["prod_nombre"]+ '" \
                                    data-price       = "' + result[i]["precio"]+ '" \
                                    data-productid   = "' + result[i]["idProducto"]+ '"> Agregar</button></td></tr>';
                  }
                  $('.child-products').empty(); 
                  $('.child-products').append(output);
                },
        error: function(){
                  output="";
                  output="<h3>No se encontraron productos</h3>";
                  $('.child-products').empty(); 
                  $('.child-products').append(output);
                }
        });

  }

  function readCookie(name) {

    var nameEQ = name + "="; 
    var ca = document.cookie.split(';');

    for(var i=0;i < ca.length;i++) {

      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) {
        return decodeURIComponent( c.substring(nameEQ.length,c.length) );
      }

    }

    return null;

  }

  function getCookie(name) {
      var dc = document.cookie;
      var prefix = name + "=";
      var begin = dc.indexOf("; " + prefix);
      if (begin == -1) {
          begin = dc.indexOf(prefix);
          if (begin != 0) return null;
      }
      else
      {
          begin += 2;
          var end = document.cookie.indexOf(";", begin);
          if (end == -1) {
          end = dc.length;
          }
      }
      // because unescape has been deprecated, replaced with decodeURI
      //return unescape(dc.substring(begin + prefix.length, end));
      return decodeURI(dc.substring(begin + prefix.length, end));
  } 


  function GetURLParameter(sParam){
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++)
    {
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == sParam)
      {
        return sParameterName[1];
      }
   }
  } 

  function filter_data(page){
    var action = 'fetch_data';
    var marca   = get_filter('marca');
    var familia = get_filter('familia');

    var shop_layout = readCookie("shop_layout"); 

    // Ajax de grilla
    $.ajax({
        url: window.base + 'Shop/fetch_data/' +page,
        method:"POST",
        dataType:"JSON",
        data:{
          action  : action, 
          familia : familia, 
          marca   : marca,
        },
        beforeSend: function(){
            $('#catalogo').fadeOut()
            setTimeout(function(){ 
              $('.spinner').fadeIn()
            },200);
        },
        success:function(data)
        {
          var output = "";

          if ( data.product_list.length !== 0 ) {

              for( var i = 0; i < data.product_list.length; i++ ) {
                 if (shop_layout == 'default'){
                    output = render_shop_default(output, data.product_list[i]);
                 }
                 else
                 {
                    output = render_shop_list(output, data.product_list[i]);
                 }
              }   

          }else{

              iziToast.warning({
                  title: 'Ups',
                  message: 'No hubo resultados para su búsqueda, intente nuevamente.',
              });

          }

          $('.filter_data').html(output);
          $('#pagination_link').html(data.pagination_link);
          $('.spinner').fadeOut()
          setTimeout(function(){
            $('#catalogo').fadeIn()
          }, 200)          
        }
    })    

  }

  function render_shop_default(output, data){
    var img_src = "";

    build_img_src(data);

    output += '<div class="col-lg-4 col-md-6">';
    output +=   '<div class="single-product">';
    output +=     '<a href="' + window.base + 'frontController/detalle/' + data['slug'] +'">';
    output +=       '<div class="img-prod" style="background:url(' + data['imagen'] + ')"></div>';
    output +=     '</a>';
    output +=     '<div class="product-details">';
    output +=       '<h6><a href="' + window.base + 'frontController/detalle/' + data['slug'] + '">' + data['nombre'] + '</a></h6>';
    output +=         '<div class="price">';
    output +=           '<p class="p-price">$ ' + data['precio'] + '</p>';
    output +=         '</div>';
    output +=       '<div class="add-bag d-flex align-items-center">';
    output +=         '<input type="number" name="quantity" class="form-control quantity" value="1" id="' + data["idProducto"] + '"/>';
    output +=         '<button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart"'; 
    output +=                   '  data-productname = "' + data["nombre"]; 
    output +=                   '" data-price       = "' + data["precio"]; 
    output +=                   '" data-productid   = "' + data["idProducto"]; 
    output +=                   '">Agregar al Pedido</button>'
    output +=       '</div>';
    output +=     '</div>';
    output +=   '</div>';    
    output += '</div>';  

    return output;

  }

  function render_shop_list(output, data){
    var img_src = "";

    build_img_src(data);
    output += '<div class="col-lg-12 col-md-12">';
    output +=  '<div class="row single-product-list">';
    output +=    '<div class="col-md-1">';
    output +=      '<a href="' + window.base + 'frontController/detalle/' + data['slug'] +'">';
    output +=        '<div class="img-prod" style="background:url(' + data['imagen'] + ')"></div>';
    output +=      '</a>';
    output +=    '</div>';
    output +=    '<div class="col-md-4">';
    output +=      '<div class="product-details">';
    output +=        '<h6><a href="' + window.base + 'frontController/detalle/' + data['slug'] + '">' + data['nombre'] + '</a></h6>';
    output +=      '</div>';
    output +=    '</div>';
    output +=    '<div class="col-md-4 row">';
    output +=      '<div class="price"><p class="p-price">$ ' + data['precio'] + '</p></div>';
    output +=      '<input type="number" name="quantity" class="form-control quantity" placeholder="Unidades" value="1" id="' + data["idProducto"] + '">';
    output +=    '</div>';
    output +=    '<div class="col-md-3">';
    output +=      '<div class="add-bag d-flex align-items-center">';
    output +=        '<button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" data-productname="' + data["nombre"] + '" data-price="' + data["precio"] + '" data-productid="' + data["idProducto"] + '">Agregar al Pedido</button>';
    output +=      '</div>';
    output +=    '</div>';
    output +=   '</div>';
    output += '</div>';

    return output;

  }

  function render_home_default(output, data){
    var img_src = "";

    build_img_src(data);

    output += '<div class="col-lg-3 col-md-6">';
    output +=   '<div class="single-product">';
    output +=     '<a href="' + window.base + 'frontController/detalle/' + data['slug'] +'">';
    output +=       '<div class="img-prod" style="background:url(' + data['imagen'] + ')"></div>';
    output +=     '</a>';
    output +=     '<div class="product-details">';
    output +=       '<h6><a href="' + window.base + 'frontController/detalle/' + data['slug'] + '">' + data['nombre'] + '</a></h6>';
    output +=         '<div class="price">';
    output +=           '<p class="p-price">$ ' + data['precio'] + '</p>';
    output +=         '</div>';
    output +=       '<div class="add-bag d-flex align-items-center">';
    output +=         '<input type="number" name="quantity" class="form-control quantity" value="1" id="' + data["idProducto"] + '"/>';
    output +=         '<button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart"'; 
    output +=                   '  data-productname = "' + data["nombre"]; 
    output +=                   '" data-price       = "' + data["precio"]; 
    output +=                   '" data-productid   = "' + data["idProducto"]; 
    output +=                   '">Agregar al Pedido</button>'
    output +=       '</div>';
    output +=     '</div>';
    output +=   '</div>';    
    output += '</div>';  

    return output;

  }

  function render_prod_dest(output, data){
    var img_src = "";

    build_img_src(data);

    output += '<div class="col-lg-3 col-md-4 col-sm-6 mb-20">'
    output +=     '<div class="single-related-product d-flex featured-box" id="' + data["idProducto"] + '">';
    output +=         '<a href="' + window.base + 'frontController/detalle/' + data['slug'] + '">';
    output +=             '<img class="img-fluid" src="' + data['imagen'] + '" alt="">';
    output +=         '</a>'
    output +=         '<div class="desc">';
    output +=             '<a href="' + window.base + 'frontController/detalle/' + data['slug'] + '" class="title">' + data["modelo"];
    output +=             '</a>';
    output +=             '<div class="price">';
    output +=                 '<h6>$' + data["precio"] + '</h6>';
    output +=             '</div>';
    output +=         '</div>';
    output +=     '</div>';
    output += '</div>';

    return output;

  }


  function build_img_src(data){

    var url = window.base + 'shop/build_src';
    
    $.ajax({
      async  : false,
      url    : url,
      method : 'POST',
      data: {images : data['imagen']},
      success : function(r){
          result = JSON.parse(r);
          data['imagen'] = result.img_src;
      }
    })



  }

  function get_filter(class_name){
    var filter = [];
    $('.'+class_name+':checked').each(function(){
        filter.push($(this).val());
    });
    return filter;
  }

  function render_side_cart(){
    build_cart_table();
    $('#side-cart-content table').empty();
    $('#side-cart-content table').append(cart_list);    
  }

  function build_cart_table(){
    $.ajax({
      async  : false,
      url    : window.base + 'cart/get_cart_content',
      method : 'POST',
      dataType: "html",          
      success: function(data){
        updateCartList(data);
      }
    })  
  }

  function updateCartList(data){
   cart_list = data; 
  }
  
  function set_cart_list(){
    build_cart_table();
    $('.spinner').css('display','none');
    render_cart();
    $('.form-cart').css('display','block');
  }

  function render_cart(){
    $('.cart-list-table').empty();
    $('.cart-list-table').append(cart_list);    
  }

  function render_orders_list(){
    $('#orders_list').DataTable( {
        "bPaginate" : false,
        "bLengthChange": false,
        "bFilter": false,
        "pageLength"   : 50,
        "info": false,
        "ajax": {
            url :  window.base + 'customer/listOrders',
            type : 'POST'
        }       
    });    
  }

  function render_order_items(idOrderHdr){
    $('#order_items').DataTable({
        "searching": false,
        "bPaginate" : false,
        "bLengthChange": false,
        "info": false,
        "destroy":true,
        "ajax": {
            url :  window.base + 'Customer/listItems',
            type : 'POST',
            data : {idOrderHdr:idOrderHdr}
        }    
    });


    $('#nro-pedido').html(' Nº ' + idOrderHdr);

    $('.pedido-detail').css('display','block');
  }

  function render_order_details(idOrderHdr){

    $.ajax({
      url    : window.base + "Customer/getOrder",
      method : 'POST',
      data   : {"idOrderHdr" : idOrderHdr},
      success: function(data){
          data = JSON.parse(data);
      //Se completan los detalles del pedido
        $('#detail-created span').html(data[0]);
        $('#detail-nota    span').html(data[1]);
        $('#detail-total   span').html(data[2]);
        $('#detail-estado  span').html(data[3]);
      }
    })
  }

  function render_productos_home(){

    var url = window.base + 'home/getProductosHome';

    $.ajax({
      url     : url,
      method  : 'POST',
      success : function(data){
          data = JSON.parse(data);
          var output = "";
          for( var i = 0; i < data.length ; i++ ) {
             output = render_home_default(output, data[i]);
          }
          $('#productos-home').html(output);
      }
    })
  }

  function render_destacados(){

    var url = window.base + 'frontController/getProdDestacados';

    $.ajax({
      url    : url,
      method : 'POST',
      success: function(data){
          data = JSON.parse(data);
          var output = "";
          for( var i = 0; i < data.length ; i++ ) {
            output = render_prod_dest(output, data[i]);
          }
          $('#prod-destacados').html(output);
      }


    })


  }

  function updateParamURL( url, name, value ){

    var currentParam = url.searchParams.get( name )

    //Si el parametro existe
    if ( currentParam ) {

      //Picar
      var check = currentParam.split(',')

      //Revisa si hay mas de un valor en el parametro
      if ( Array.isArray(check) == true ){
        //Revisa si el valor del loop existe en el array
        if( $.inArray( value, check ) !== -1){
          // No hacer nada si existe
        }else{
          var m = currentParam + ',' + value
          url.searchParams.set( name , m );
          console.log(m)
        }
      }

    }else{
      url.searchParams.append( name , value );
    }

  }

  function getAllUrlParams(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

      // stuff after # is not part of query string, so get rid of it
      queryString = queryString.split('#')[0];

      // split our query string into its component parts
      var arr = queryString.split('&');

      for (var i = 0; i < arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');

        // set parameter name and value (use 'true' if empty)
        var paramName = a[0];
        var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

        // (optional) keep case consistent
        paramName = paramName.toLowerCase();
        if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

        // if the paramName ends with square brackets, e.g. colors[] or colors[2]
        if (paramName.match(/\[(\d+)?\]$/)) {

          // create key if it doesn't exist
          var key = paramName.replace(/\[(\d+)?\]/, '');
          if (!obj[key]) obj[key] = [];

          // if it's an indexed array e.g. colors[2]
          if (paramName.match(/\[\d+\]$/)) {
            // get the index value and add the entry at the appropriate position
            var index = /\[(\d+)\]/.exec(paramName)[1];
            obj[key][index] = paramValue;
          } else {
            // otherwise add the value to the end of the array
            obj[key].push(paramValue);
          }
        } else {
          // we're dealing with a string
          if (!obj[paramName]) {
            // if it doesn't exist, create property
            obj[paramName] = paramValue;
          } else if (obj[paramName] && typeof obj[paramName] === 'string'){
            // if property does exist and it's a string, convert it to an array
            obj[paramName] = [obj[paramName]];
            obj[paramName].push(paramValue);
          } else {
            // otherwise add the property
            obj[paramName].push(paramValue);
          }
        }
      }
    }

    return obj;
  }

  function filter_init(){ 

      var filter = '';

      $('.common_selector:checked').each(function(i, field){

        filter += '<li class="single-filter float-left">';
        filter += $(this).attr('data-name')
        filter += '<button remove-filter data-name="' + $(this).attr('name') + '" data-id="' + $(this).val() + '"><i class="fa fa-times"></i></button>'
        filter += '</li>'

        console.log($(this))

      })

      $('#active-filters').append( filter )

  }

  $(document).on('click', '[remove-filter]', function(){

      var name = $(this).attr('data-name'),
          val = $(this).attr('data-id');
      console.log($('.common_selector[name="' + name + '"][value="' + val + '"'))

      $('.common_selector[name="' + name + '"][value="' + val + '"').click();
      $(this).parent().fadeOut()
  })
