
var unblockConfig = {
			message: '<i class="fa fa-cog fa-2x fa-spin"></i>',
        css:{
            backgroundColor: 'transparent',
            border: 'none', 
        },
        overlayCSS: {
            backgroundColor: '#FFF',
        }
	}; 

// iziToast - settings
	iziToast.settings({
	  timeout: 3000, // default timeout
	  resetOnHover: true,
	  // icon: '', // icon class
	  transitionIn: 'fadeInUp',
	  transitionOut: 'fadeOut',
	}); 

// Select Attr FIX
$(window).on('load', function(){
	$('#atributoFamilia option[data-placeholder]').prop('selected', true)
})

// Knockout 
$(document).ready(function(){ 

	// Atributos
	var AttributesModel = function(attributes) {

	    var self = this;
	    self.attributes = ko.observableArray(attributes); 

	    // Chequea si es variable y muestra el checkbox
	    self.activeVariable = function(attribute){  

	    	var varCheck = $('#' + attribute.attributesSection + ' [name="atributo_variacion[]"]');

	    	if ( varCheck.is(':checked') ) {
	    		$('select[name="variacionesFamilia"]').append('<option value="' + attribute.attributesSection + '">' + attribute.attributeName + '</option>')
	    	}else{
	    		$('select[name="variacionesFamilia"] option[value="' + attribute.attributesSection + '"]').remove()
	    	} 

	    }

	    // Agrega un nuevo row
	    self.addAttr = function() { 

	    	// Family ID
	    	var family = $('#atributoFamilia option:selected').attr('data-family'),
	    		title = $('#atributoFamilia option:selected').text(),
	    		attrID = $('#atributoFamilia option:selected').val(),
	    		attrOptions;

	    	if ( family == undefined || family == '' ) {
			    iziToast.error({
                    title: 'Error!',
                    message: 'Debe seleccionar una Familia',
                }); 
	    		return false;
	    	}

	    	// Carga las opciones del select nuevo
			$.ajax({
				    url: base_url + "Producto/atributos_familia_json/" + family + '/' + title,
				    // dataType: 'json',
				    async: false,
				    success: function (data) {
				    	attrOptions = data;
				    }
			})

	    	// Atributo es variable
	    	var isAttrVar;
			$.ajax({
				    url: base_url + "Producto/atributo_es_variable/" + attrID,
				    async: false,
				    success: function (response) {

				    	isAttrVar = ( response == 1 ) ? true : false;

				    }
			})

	        self.attributes.push({
	            attributeName: title,
	            attributeValue: "", 
				attributesSection: attrID, 
				attributesOptions:  attrOptions,
				attributesSelectedOptions: {}, 
				attrVar: false,
				isAttrVariable: isAttrVar, 
	        });

	        // Disable attribute option 
	        $('#atributoFamilia option:selected').prop('disabled', true)
	        setTimeout( function(){
	        	$('#atributoFamilia option[data-placeholder]').prop('selected', true)
	        } , 200 )
	        

	    };
	 	
	 	// Quitar row
	    self.removeAttr = function(attribute) {
	    	$('#atributoFamilia option[value="' + attribute.attributesSection + '"]').prop('disabled', false)
	        self.attributes.remove(attribute);
	    };

	    // Al terminar el render del row nuevo, ejecutar:
	    self.attrAfterRender = function(attribute){ 

	        // Si existe un row [FIX]   
	        // Select2
			$(".attr-row-data select[name='atributo_valores[]']:last-child").select2();
			// Cierra las pestañas
			$('.attr-row:last-child .attr-slide').click() 
	    }

	};  

	// Atributos registrados en la BD
	var bindAttr = Array();
	
	$.ajax({
		url: base_url + 'Producto/ko_db_atributos',
		type: 'post',
		data: { 'idProducto': idProducto },
		async: false,
		beforeSend: function(){
			$('#ko-attributes').block( unblockConfig )
		},
		success: function(data){

			$('#ko-attributes').unblock()
	
			var db = data;
			
			db.forEach(function(item, index){  

				var options = $.parseJSON( item.atributos.valor ),
					is_var = ( item.is_variacion == 1 ) ? true : false,
					isAttrVar = ( item.atributo_variable == 1 ) ? true : false,
					attrSelectedOpt = ( isJson( item.valores ) ) ? $.parseJSON( item.valores ) : [ item.valores ]; //Si no es JSON crea un array con el valor unico

				// Deshabilita la opcion en el select de atributo 
				$('#atributoFamilia option[value="'+item.idAtributo+'"]').prop('disabled', true)

				bindAttr.push({ 
		            attributeName: item.nombre,
		            attributeValue: '', 
					attributesSection: item.idAtributo,
					attributesOptions: options,
					attributesSelectedOptions: attrSelectedOpt,
					attrVar: is_var,
					isAttrVariable: isAttrVar, 
				}); 

			}) 
		}

	}) 

	var viewAttrModel = new AttributesModel( bindAttr );

	ko.applyBindings( viewAttrModel, document.getElementById("ko-attributes") ); 

	// Variaciones
	var VariationModel = function(variations) {

	    var self = this;
	    self.variations = ko.observableArray(variations);

	    // Agregar nueva variacion
	    self.addVar = function(variations) { 

	    	// Obtiene los valores seleccionados para la variacion
	    	var varValuesHTML = '',
	    		varAttr = '';

	    	// Recorre todos los valores 
			$('#ko-attributes .attr-row').each(function(index, item){  

				var varAttr = $(this).attr('id');

				var idAtributo = $(this).attr('id'),
					label = $(this).find('input[name="atributo_nombre[]"]').val(),
					checkbox = $(this).find(' input[name="atributo_variacion[]"]'),
					options = $(this).find(' select[name="atributo_valores[]"]').val()
 				
				//  revisa si esta tildado
				if ( checkbox.is(':checked') ) {

	 				// Si no hay opcion seleccionada
					if ( options == '' || options == undefined ) {

	                    iziToast.error({
	                        title: 'Error!',
	                        message: 'No hay ningun atributo seleccionado para variación.',
	                    });

					}

					varValuesHTML += '<div class="form-group col-sm-6"> '
					varValuesHTML += '<label>' + label + '</label>'
					varValuesHTML += '<select data-attribute="' + idAtributo + '" name="variacion_atributo[]" class="form-control">'

					options.forEach(function(item, index){
						varValuesHTML += '<option value="'+ item +'">'+ item +'</option>'
					})

					varValuesHTML += '</select>'
					varValuesHTML += '</div>'

				}  

			})

			if ( varValuesHTML !== '' ) {
	    		// Agrega una variacion
		        self.variations.push({
		            varSKU: "",
		            varPrice: "",
		            varAttr: varAttr,
		            varStock: "",
		            varValuesHTML: varValuesHTML,
		            // varRowTitle: title //Adicional para nombre de row
		        });
			}else{

                iziToast.error({
                    title: 'Error!',
                    message: 'No hay ningun atributo seleccionado como variacion, intente nuevamente.',
                }); 

			}

	    };
	 	
	 	// Quitar Variacion
	    self.removeVar = function(variation) {
	        self.variations.remove(variation);
	    }; 

	    // Al terminar el render del row nuevo, ejecutar:
	    self.varAfterRender = function(variation){ 

	        // Si existe un row [FIX]   
	        // Select2
			$(".var-row-data:last-child select").select2();
			// Cierra las pestañas 
			$('.var-row:last-child .var-slide').click() 

	    }

	};
	
	// Variaciones registradas en la BD
	var bindVar = Array();  
	
	$.ajax({
		url: base_url + 'Producto/ko_db_variaciones',
		type: 'post',
		data: { 'idProducto': idProducto },
		async: false,
		beforeSend: function(){
			$('#ko-variations').block( unblockConfig )
		},
		success: function(data){

			$('#ko-variations').unblock() 
			
			// Recorre los registrados
			data.forEach(function(item, index){    

				var varValuesHTML = '',
					atributos = item.atributos,
					producto = item.producto,
					varAttr = ''; //Atributo para HTML

				atributos.forEach( function(itemAtributo, indexAtributo){ 

			    	// Recorre todos las variaciones  
					varAttr = itemAtributo.idAtributo;

					var idAtributo = $('#'+ itemAtributo.idAtributo +'.attr-row').attr('id'),
						label = $('#'+ itemAtributo.idAtributo +'.attr-row').find('input[name="atributo_nombre[]"]').val(),
						checkbox = $('#'+ itemAtributo.idAtributo +'.attr-row').find('input[name="atributo_variacion[]"]'),
						options = $('#'+ itemAtributo.idAtributo +'.attr-row').find('select[name="atributo_valores[]"]').val() 

					//  agrega atributo  
					varValuesHTML += '<div class="form-group col-sm-6"> '
					varValuesHTML += '<label>' + label + '</label>'
					varValuesHTML += '<select data-attribute="' + idAtributo + '" name="variacion_atributo[]" class="form-control">'

					options.forEach(function(item, index){
						varValuesHTML += '<option value="'+ item +'">'+ item +'</option>'
					})

					varValuesHTML += '</select>'
					varValuesHTML += '</div>'  

				}) 

				console.log( producto )

				bindVar.push({ 
						      varSKU: producto.sku, 
						      varPrice: producto.precio, 
						      varAttr: varAttr,
						      varStock: producto.stock, 
						      varValuesHTML: varValuesHTML,
						      // varRowTitle: ''
						  	}); 

			}) 

		}

	})

	var viewModel = new VariationModel( bindVar );

	ko.applyBindings( viewModel, document.getElementById("ko-variations") ); 

})
 
// Nuevo valor de atributo para familia
$(document).on('click', '.new-attr-value', function(e){

	e.preventDefault()

  	var newAttrValue = prompt("Valor del atributo", ''),
  		attrID = $(this).attr('data-attribute'),
  		select = $(this).closest('.attr-row').find('select[name="atributo_valores[]"]');

  	if (newAttrValue != null) {
    	
	  	$.ajax({
	  		url: base_url + 'Atributo/nuevo_valor_atributo',
	  		type: 'post',
	  		data: { 'idAtributo': attrID, 'valor' : newAttrValue },
	  		beforeSend: function(){
	  			$('#ko-attributes').block( unblockConfig )
	  		},
	  		success: function(data){
	  			$('#ko-attributes').unblock() 

	  			if ( data.code !== 1 ) {
				    iziToast.error({
	                    title: 'Error!',
	                    message: data.message,
	                });
	  			}else{
	  				var newOption = new Option(newAttrValue, newAttrValue, false, false);
	  				select.append(newOption).trigger('change')
	  			}

	  		}
	  	})

  	}

	})

// Guardar Atributos 
$(document).on('click', '.save-attributes', function(e){

	e.preventDefault()

	var atributo_id = Array(),
		atributo_nombre = Array(),
		atributo_valores = Array(),
		atributo_variacion = Array();

	$('#ko-attributes input[name="atributo_id[]"]').each(function(){
		atributo_id.push( $(this).val() );
	})

	$('#ko-attributes input[name="atributo_nombre[]"]').each(function(){
		atributo_nombre.push( $(this).val() );
	})

	$('#ko-attributes select[name="atributo_valores[]"]').each(function(){
		atributo_valores.push( $(this).val() );
	})

	// Variaciones
	$('#ko-attributes .attr-row').each(function(){  

		var id = $(this).attr('id'),
			checkbox = $(this).find(' input[name="atributo_variacion[]"]');  

		// Si el atributo tiene el campo de variacion
		if ( checkbox.size() >= 1 ) {
			//  revisa si esta tildado
			if ( checkbox.is(':checked') ) {
				atributo_variacion.push( 1 ); 
			}else{
				atributo_variacion.push( 0 ); 
			}

		}else{
			atributo_variacion.push( 0 ); 
		} 

	})  

  	$.ajax({
	  	type: 'post',
  		url: base_url + 'Producto/guardar_producto_atributos/',
  		data: { 
  				'idProducto': idProducto, 
  				'idAtributo': atributo_id, 
  				'valores' : atributo_valores, 
  				'is_variacion': atributo_variacion 
  			},
  		beforeSend: function(){
  			$('#ko-attributes').block( unblockConfig )
  		},
  		success: function(data){
  			$('#ko-attributes').unblock()  

  			if ( data.code !== 1 ) {
			    iziToast.error({
                    title: 'Error!',
                    message: data.message,
                });
  			}else{
  				
  			}

  		}
  	})

})

// Guardar Atributos 
$(document).on('click', '.save-variations', function(e){

	e.preventDefault()

	var variacion_sku = Array(),
		variacion_precio = Array(),
		variacion_stock = Array(),
		variacion_atributo = Array(); 

    var formData = new FormData();
    	formData.append( 'idProducto', idProducto )


	$('#ko-variations .var-row input[name="variacion_sku[]"]').each(function(){
		formData.append( 'variacion_sku[]', $(this).val() )
	})

	$('#ko-variations input[name="variacion_precio[]"]').each(function(){
		formData.append( 'variacion_precio[]', $(this).val() )
	})

	$('#ko-variations select[name="variacion_stock[]"]').each(function(){
		formData.append( 'variacion_stock[]', $(this).val() )
	}) 

	// Atributos por fila
	$('#ko-variations .var-row').each(function(indexRow, item){

		$(this).find('select[name="variacion_atributo[]"]').each(function(indexVar, item){

			formData.append( 'variacion_atributo['+indexRow+']['+indexVar+'][idAtributo]', $(this).attr('data-attribute') )
			formData.append( 'variacion_atributo['+indexRow+']['+indexVar+'][valores]', $(this).val() )

		})  
		
	}) 

  	$.ajax({
	  	type: 'post',
  		url:  base_url + 'Producto/guardar_producto_variaciones/',
  		data: formData,
  		cache: false,
	    processData: false,
	    contentType: false,
  		beforeSend: function(){
  			$('#ko-variations').block( unblockConfig ) 
  		},
  		success: function(data){

  			$('#ko-variations').unblock() 

  			console.log(data)

  			if ( data.code !== 1 ) {
			    iziToast.error({
                    title: 'Error!',
                    message: data.message,
                });
  			}

  		}
  	})

})
// Toggles
$(document).on('click', '.var-slide', function(e){
	e.preventDefault()
	$(this).closest('.var-row').find('.var-row-data').slideToggle();

	if( $(this).find('i').hasClass('fa-sort-up') ){
		$(this).html('<i class="fa fa-sort-down"></i>')
	}else{
		$(this).html('<i class="fa fa-sort-up"></i>')
	}
	
})

$(document).on('click', '.attr-slide', function(e){
	e.preventDefault()
	$(this).closest('.attr-row').find('.attr-row-data').slideToggle();

	if( $(this).find('i').hasClass('fa-sort-up') ){
		$(this).html('<i class="fa fa-sort-down"></i>')
	}else{
		$(this).html('<i class="fa fa-sort-up"></i>')
	}
	
})

$(document).on('change', '[name="variacion_sku[]"]', function(){

	var line = $(this).closest('tr');

	if( $(this).val() !== '' ){
		line.find('[name="variacion_precio[]"]').prop('required', true)
		line.find('[name="variacion_talle[]"]').prop('required', true)
	}else{
		line.find('[name="variacion_precio[]"]').prop('required', false)
		line.find('[name="variacion_talle[]"]').prop('required', false)
	}

})

$(document).on('change', '[name="variacion_sku[]"]', function(){

	var sku = $(this).val(),
		input = $(this);

	if( sku == '' || sku == undefined){
		return;
	}

	$.ajax({
		'url': base_url + 'Producto/check_sku_exist',
		'data': {'sku': sku},
		beforeSend: function(){

		},
		success: function(response){ 

			if (response.code == 0) {
				input.parent().addClass('has-error')
				input.parent().find('.help-block').text( response.message )
				input.val('')
			}else{
				input.parent().removeClass('has-error')											
				input.parent().find('.help-block').text('SKU de la variación')
			}

		}
	})

})

// Detect JSON
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}